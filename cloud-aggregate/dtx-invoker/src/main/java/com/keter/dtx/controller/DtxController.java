package com.keter.dtx.controller;

import com.keter.dtx.service.BusinessService;
import com.keter.framework.core.exception.ValidateException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author jimin.jm@alibaba-inc.com
 * @date 2019/06/14
 */

@RestController
public class DtxController {

    private static final Logger logger = LoggerFactory.getLogger(DtxController.class);
    @Autowired
    private BusinessService businessService;

    /**
     * 购买下单，模拟全局事务提交
     *
     * @return
     */
    @RequestMapping(value = "/purchase/commit")
    public String purchaseCommit() {
        try {
             businessService.purchase("U100000", "C100000", 3);
        } catch (Exception exx) {
            logger.error("", exx);
            throw new ValidateException(exx.getMessage());
        }
        return "success";
    }

    /**
     * 购买下单，模拟全局事务回滚
     * 账户或库存不足
     *
     * @return
     */
    @RequestMapping("/purchase/rollback")
    public String purchaseRollback() {
        try {
            businessService.purchase("U100000", "C100000", 99999);
        } catch (Exception exx) {
            logger.error("", exx);
            throw new ValidateException(exx.getMessage());
        }
        return "success";
    }
}
