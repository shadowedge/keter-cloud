//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.keter.dtx.plugin;

import com.jfinal.plugin.druid.DruidPlugin;
import io.seata.rm.datasource.DataSourceProxy;

import javax.sql.DataSource;

public class SeataDruidPlugin extends DruidPlugin {

    private DataSourceProxy dsp;

    @Override
    public DataSource getDataSource() {
        return this.dsp;
    }

    public SeataDruidPlugin(String url, String username, String password) {
        super(url, username, password);
    }

    @Override
    public boolean start(){
        boolean result = super.start();
        dsp = new DataSourceProxy(super.getDataSource());
        return result;
    }
}
