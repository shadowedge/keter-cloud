package test.exp;

import com.keter.dtx.controller.DtxController;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.MockitoAnnotations;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import test.base.ControllerBaseTest;

public class DtxControllerTest extends ControllerBaseTest {
	/**
	* Logger for this class
	*/
	private static final Logger logger = LoggerFactory.getLogger(DtxControllerTest.class);

	@Autowired
	DtxController controller;

	@Before
	public  void mock(){
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void commit(){
		String result = controller.purchaseCommit();
		logger.info("res:{}",result);
		Assert.assertEquals("success",result);
	}

	@Test
	public void rollback(){
		String result = controller.purchaseRollback();
		logger.info("res:{}",result);
		Assert.assertEquals("success",result);
	}
}