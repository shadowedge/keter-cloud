#! /bin/sh
# 传统部署模式
# 不依赖于CI的构建脚本
SCRIPT_DIR=$(cd `dirname "$0"`; pwd)

# 从util 中统一取端口、名称和版本号等信息
. ${SCRIPT_DIR}/util.sh

cd "${SCRIPT_DIR}"/../
echo "打包maven应用..."
mvn -DskipTests=true package
echo "提取文件名称:" $(filename)
echo "生成最新版本(latest)的程序文件..."
cp "${SCRIPT_DIR}"/"$(filename)" "${SCRIPT_DIR}"/${ARTIFACT_ID}-latest.jar

