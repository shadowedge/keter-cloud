#! /bin/sh
# 不依赖于CI的构建脚本
SCRIPT_DIR=$(cd `dirname $0`; pwd)
# 从util 中统一取端口、名称和版本号等信息
. ${SCRIPT_DIR}/util.sh
# 执行打包操作
cd ${SCRIPT_DIR}/../
mvn -DskipTests=true package
# 通过当前版本生成最新版本(latest)，以便提供给cloud-deploy使用
cp ${SCRIPT_DIR}/$(name)-$(version).jar ${SCRIPT_DIR}/$(name)-latest.jar