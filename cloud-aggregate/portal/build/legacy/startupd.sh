#! /bin/sh
# 支持Jenkins的启动脚本
SCRIPT_DIR=$(cd `dirname $0`; pwd)
# 从util 中统一取端口、名称和版本号等信息
. ${SCRIPT_DIR}/util.sh
HOST_NAME =  $(ip addr | grep 'state UP' -A2 | tail -n1 | awk '{print $2}' | cut -f1 -d '/')
pid=`ps -ef |grep 'java -jar'|grep $(name)|awk '{print $2}'`
    if [ -n "$pid" ];then
        echo "----kill java process: "$pid
        kill -9 $pid
    fi
	echo "++++start java process: "$(name)-$(version)
    
	# Jenkins运行正常但会出现 “Process leaked file descriptors. ”警告
	nohup java -jar ${SCRIPT_DIR}/$(name)-$(version).jar  --spring.profiles.active=ci --config.enabled=true --config.hostname=${HOST_NAME} --config.uri=http://192.168.1.111:9100 >  ${SCRIPT_DIR}/startup.log&
