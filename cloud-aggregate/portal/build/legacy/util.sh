#!/bin/bash
SCRIPT_DIR=$(cd `dirname $0`; pwd)
# pom.xml中的项目ARTIFACT_ID标识
ARTIFACT_ID=portal
# docker镜像库标识，tag前缀
REGISTRY=keter

port(){ # 容器内服务运行端口
 echo 8888
}

version(){ # 从工程父目录的pom.xml文件中读取版本号信息
    # 去除Windows换行：避免文件直接copy情况下出现行长度取值+1问题
	  sed -i 's/\r//'  ${SCRIPT_DIR}/../pom.xml
    # 根据项目的<artifactId>定位<version>行，并提取版本号
    VERSION_LINE=$(cat ${SCRIPT_DIR}/../pom.xml|grep -A1 -m1 "<artifactId>${ARTIFACT_ID}"|grep version)
    # 需要用echo的方式读取变量，否则没法把字符串喂给后面的截取程序处理
    LENGTH=`echo ${VERSION_LINE}|wc -m`
    END=`echo ${LENGTH}-11|bc`
    echo ${VERSION_LINE}|cut -c10-${END}
 }

name(){ # 服务名
  echo ${ARTIFACT_ID}
}

tag(){ #镜像TAG
 echo ${REGISTRY}/$(name):$(version)
}

data(){ #镜像运行时产生数据的目录
 echo $(name)-data
}

echo "镜像："$(tag)
echo "文件："$(name)-$(version)
echo "数据："$(data)
