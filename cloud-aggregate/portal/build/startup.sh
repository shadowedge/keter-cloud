#! /bin/sh
# 以startupd启动应用并观察日志：不可用于开机启动，会导致后续脚本阻塞
SCRIPT_DIR=$(cd `dirname $0`; pwd)
bash "${SCRIPT_DIR}"/startupd.sh
tail -f "${SCRIPT_DIR}"/startup.log
