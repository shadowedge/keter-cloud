#! /bin/bash
SCRIPT_DIR=$(cd `dirname $0`; pwd)
# pom.xml中的项目ARTIFACT_ID标识
ARTIFACT_ID=keter-web

filename(){
  echo $(ls ${SCRIPT_DIR}|grep ${ARTIFACT_ID}|grep -v 'latest')
}

appname(){
  echo ${ARTIFACT_ID}'-latest.jar'
}

pkill(){
    pid=`ps -ef |grep 'java -jar'|grep $(filename)|awk '{print $2}'`
    if [ -n "$pid" ];then
        echo "----kill java process: "$pid
        kill -9 $pid
    fi
	echo "++++start java process: "$(filename)
}

echo "文件名称：" $(filename)
echo "App名称：" $(appname)
