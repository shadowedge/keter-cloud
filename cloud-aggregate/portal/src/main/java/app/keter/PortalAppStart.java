package app.keter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.server.WebServerFactoryCustomizer;
import org.springframework.boot.web.servlet.context.ServletWebServerInitializedEvent;
import org.springframework.boot.web.servlet.server.ConfigurableServletWebServerFactory;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.hystrix.EnableHystrix;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.ApplicationListener;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;

/**
 * Spring Boot 应用的入口文件
 * @author gu
 */
@SpringBootApplication
@EnableWebSecurity //启用安全功能
@EnableHystrix
@EnableFeignClients
@EnableDiscoveryClient
@ComponentScan("app.keter.portal")
public class PortalAppStart implements ApplicationListener<ServletWebServerInitializedEvent>, WebServerFactoryCustomizer<ConfigurableServletWebServerFactory> {
    private static final Logger logger = LoggerFactory.getLogger(PortalAppStart.class);

    public static void main(String[] args) {
        SpringApplication.run(PortalAppStart.class, args);
        logger.info("app started!");
    }

    @Value("${spring.cloud.consul.discovery.instanceId:portal-0}")
    String instId;
    @Value("${spring.cloud.config.enabled:false}")
    boolean remoteConfig;

    @Override
    public void onApplicationEvent(ServletWebServerInitializedEvent servletWebServerInitializedEvent) {
        if(remoteConfig) {
            System.out.println("启用远程配置： \nSERVER PORT: " + servletWebServerInitializedEvent.getWebServer().getPort() + " \nINSTANCE: " + instId + "");
        }
    }

    @Override
    public void customize(ConfigurableServletWebServerFactory factory) {

    }
}
