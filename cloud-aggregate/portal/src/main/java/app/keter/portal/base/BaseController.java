package app.keter.portal.base;

import app.keter.portal.config.Services;
import com.keter.framework.web.base.AbstractBaseController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.client.RestTemplate;

@Controller
public class BaseController extends AbstractBaseController {
    @Autowired
    protected RestTemplate restTemplate;

    @Autowired
    protected Services services;
}
