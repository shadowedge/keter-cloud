package app.keter.portal.config;

import com.keter.framework.web.component.http.HttpClienter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

/**
 * Created by gulixing@msn.com on 2019/4/2.
 */
@Configuration
public class AppConfig {

    @Autowired
    HttpClienter clienter;

    /**
     * 舱壁隔离模式：支持为每个服务分配专属的RestTemplate
     * COMMON服务客户端：所有掉用COMMON服务的请求均使用同一个客户端
     * @return
     */
    @LoadBalanced
    @Bean("commonTemplate")
    RestTemplate commonTemplate(){
        return clienter.restTemplate(5);
    }
}
