package app.keter.portal.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * 由消费端来统一配置服务地址
 */
@Configuration
@ConfigurationProperties(prefix="services")
public class Services {
    private final String PROTOCOL = "http://";

    private String common;

    public String getCommon() {
        return PROTOCOL + common;
    }

    public void setCommon(String common) {
        this.common = common;
    }

}