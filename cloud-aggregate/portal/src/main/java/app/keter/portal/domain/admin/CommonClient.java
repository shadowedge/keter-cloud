package app.keter.portal.domain.admin;

import com.alibaba.fastjson.JSONObject;
import com.keter.framework.web.result.JSONResult;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

/**
 * 处理Common服务中Org相关功能的请求
 */
@FeignClient(name ="${services.common}", path = "/api/v1", contextId = "common", fallbackFactory = CommonServiceFallback.class)
public interface CommonClient {

    @GetMapping("/orgs")
    JSONResult findAllOrg();

    @GetMapping("/orgs/{id}")
    JSONResult findOrgById(@PathVariable String id);

    @PostMapping("/orgs")
    JSONResult addOrg(@RequestBody JSONObject org);

    @PostMapping("/orgs/{orgId}/users")
    JSONResult addOrgUsers(@PathVariable String orgId, @RequestBody String userIds);

    @GetMapping("/orgs/{id}/users")
    JSONResult findOrgUsersById(@PathVariable String id);
}
