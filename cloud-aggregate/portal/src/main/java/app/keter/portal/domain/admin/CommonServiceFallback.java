package app.keter.portal.domain.admin;

import com.alibaba.fastjson.JSONObject;
import com.keter.framework.web.result.JSONResult;
import feign.hystrix.FallbackFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * 处理CommonService熔断的工厂方法
 */
@Component
public class CommonServiceFallback implements FallbackFactory<CommonClient> {
    private static final Logger logger = LoggerFactory.getLogger(CommonServiceFallback.class);

    @Override
    public CommonClient create(Throwable arg0) {
        logger.info("fallback reason was:{}", arg0.getMessage());
        return new CommonOrgClientFactory() {
            @Override
            public JSONResult findAllOrg() {
                return fallbackResult("findAllOrgs 熔断介入！");
            }

            @Override
            public JSONResult findOrgById(String id) {
                return fallbackResult("findOneOrg 熔断介入！");
            }

            @Override
            public JSONResult addOrg(JSONObject org) {
                return fallbackResult("addOrg 熔断介入！");
            }

            @Override
            public JSONResult addOrgUsers(String orgId, String userIds) {
                return fallbackResult("addOrgUsers 熔断介入！");
            }

            @Override
            public JSONResult findOrgUsersById(String orgId) {
                return fallbackResult("findUsersOfOrg 熔断介入！");
            }
        };
    }

    private JSONResult fallbackResult(String s) {
        JSONResult result = new JSONResult();
        result.put(JSONResult.KEY_STATUS, JSONResult.Status.ERROR.value());
        result.put(JSONResult.KEY_DATA, s);
        return result;
    }

    public interface CommonOrgClientFactory extends CommonClient {

    }

}