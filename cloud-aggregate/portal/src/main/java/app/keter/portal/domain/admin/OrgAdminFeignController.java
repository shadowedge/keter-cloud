package app.keter.portal.domain.admin;

import com.alibaba.fastjson.JSONObject;
import app.keter.portal.base.BaseController;
import com.keter.framework.web.result.JSONResult;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * 组织机构管理
 */
@RestController
@RequestMapping("/api/v1/admin/orgs")
public class OrgAdminFeignController extends BaseController {

	@Autowired
    CommonClient client;

	@ApiOperation(value = "获取全部组织机构")
	@GetMapping
	public JSONResult findAll() {
		return client.findAllOrg();
	}

	@ApiOperation(value = "通过ID查找组织机构")
	@GetMapping("/{id}")
	public JSONResult findById(
			@ApiParam(value = "组织机构ID", required = true)
			@PathVariable  String id) {
		return client.findOrgById(id);
	}

	@ApiOperation(value = "增加组织机构")
	@PostMapping
	public JSONResult add(
			@ApiParam(value = "组织机构对象", required = true)
			@RequestBody JSONObject org) {
		//默认状态
		org.put("status","0");
		return client.addOrg(org);
	}

	@ApiOperation(value = "向组织机构添加用户")
	@GetMapping("/{orgId}/users/{userIds}")
	public JSONResult addUsers(
			@ApiParam(value = "组织机构ID", required = true)
			@PathVariable String orgId,
			@ApiParam(value = "逗号分隔的用户ID列表",example = "1,2,3",required = true)
			@PathVariable String userIds) {
		return client.addOrgUsers(orgId,userIds);
	}

	@ApiOperation(value = "获取组织机构下的用户")
	@GetMapping("/{id}/users")
	public JSONResult findUsersById(
			@ApiParam(value = "组织机构ID", required = true)
			@PathVariable  String id) {
		return client.findOrgUsersById(id);
	}
}
