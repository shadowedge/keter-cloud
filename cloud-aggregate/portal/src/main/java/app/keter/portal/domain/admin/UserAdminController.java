package app.keter.portal.domain.admin;

import app.keter.portal.security.CurrentUser;
import app.keter.portal.security.core.UserDetailsImpl;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.keter.framework.core.exception.ValidateException;
import app.keter.portal.base.BaseController;
import app.keter.portal.mail.MailService;
import com.keter.framework.web.result.JSONResult;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.apache.commons.lang3.RandomStringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * 用户管理
 */
@RestController
@RequestMapping("/api/v1/admin/users")
public class UserAdminController extends BaseController {

    private static final Logger logger = LoggerFactory.getLogger(UserAdminController.class);

    @Autowired
    MailService mailSender;

    private String USER_API;

    @Autowired void init() {
        USER_API = services.getCommon() + "/api/v1/users";
    }

    @ApiOperation(value = "增加用户")
    @PostMapping
    @HystrixCommand(fallbackMethod = "addError"
            , ignoreExceptions = ValidateException.class //忽略业务异常
            , commandProperties = {
            //为了预留重试时间，需为Ribbon配置的超时时间需短于hystrix超时配置
            @HystrixProperty(name = "execution.isolation.thread.timeoutInMilliseconds", value = "4000")
    })
    public JSONResult add(
            @ApiParam(value = "用户对象", required = true)
            @RequestBody JSONObject user) {
        UserDetailsImpl userDetailsLocal = CurrentUser.getCurrentUser();
        // 后台生成6位默认随机密码
        user.put("password", RandomStringUtils.randomAlphabetic(6));
        user.put("status", 0);
        JSONResult result = restTemplate.postForObject(USER_API, user, JSONResult.class).ensure();
        mailSender.sendAsync(makeMail(userDetailsLocal, user));
        return result;
    }

    @ApiOperation(value = "获取全部用户")
    @GetMapping
    public JSONResult findAll() {
        return restTemplate.getForObject(USER_API, JSONResult.class);
    }

    @ApiOperation(value = "通过ID查找用户")
    @GetMapping("/{id}")
    public JSONResult findById(
            @ApiParam(value = "用户ID", required = true)
            @PathVariable String id) {
        return restTemplate.getForObject(USER_API + "/{1}", JSONResult.class, id);
    }

    @ApiOperation(value = "修改密码")
    @PostMapping("/passchange")
    public JSONResult passChange(
            @ApiParam(value = "密码对象:{oldpass:原密码,newpass:新密码}", required = true)
            @RequestBody JSONObject passObj) {
        return restTemplate.postForObject(USER_API + "/passchange/" + CurrentUser.getCurrentUser().getId(), passObj, JSONResult.class);
    }

    @ApiOperation(value = "修改用户")
    @PatchMapping
    public JSONResult update(
            @ApiParam(value = "用户对象", required = true)
            @RequestBody JSONObject user) {
        return restTemplate.patchForObject(USER_API + "/", user, JSONResult.class);
    }

    /**
     * 根据目标用户构建邮件对象
     * @param targgetUser
     * @return
     */
    private JSONObject makeMail(UserDetailsImpl currentUser, JSONObject targgetUser) {
        String content = String.format("尊敬的用户%s，您好：<br>你的账号已创建成功！用户名: %s，密码: %s，请尽快登录<a href=\"http://www.xxx.com/\">Keter Cloud平台</a>，修改默认密码！"
                , targgetUser.get("name")
                , targgetUser.get("username")
                , targgetUser.get("password"));
        JSONObject obj = new JSONObject();
        obj.put("title", "Keter Cloud平台用户账号创建成功！");
        obj.put("html", content);
        JSONArray to = new JSONArray();
        to.add(targgetUser.get("email"));
        obj.put("to", to);

        //暗送给管理员
        JSONArray bcc = new JSONArray();
        bcc.add(currentUser.getEmail());
        obj.put("bcc", bcc);
        return obj;
    }

    /**
     * 熔断机制的回调方法
     *
     * @param user
     * @param t
     * @return
     */
    public JSONResult addError(@RequestBody JSONObject user, Throwable t) {
        logger.error("触发熔断机制，原因:{}，数据:{}", t.toString(), user.toJSONString());
        return wrap(t.toString());
    }
}
