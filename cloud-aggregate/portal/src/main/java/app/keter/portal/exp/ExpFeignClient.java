package app.keter.portal.exp;

import com.keter.framework.web.component.sequence.SIDHolder;
import com.keter.framework.web.result.JSONResult;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;

@FeignClient(name= "${services.common}", path="/exp", contextId="exp")
public interface ExpFeignClient {

    /**
     * 对于需要幂等的请求，目前需主动在方法参数中提供SID并使用@RequestHeader添加消息头
     * @param sid
     * @return
     */
    @GetMapping(value = "/sid")
    JSONResult sid(@RequestHeader(SIDHolder.KEY_SID) Long sid);

    @GetMapping(value = "/data/object")
    JSONResult dataObject();

    @GetMapping(value = "/data/array")
    JSONResult dataArray();

    @GetMapping(value = "/data/string")
    JSONResult dataString();

    @GetMapping(value = "/timeout")
    JSONResult timeout();
}
