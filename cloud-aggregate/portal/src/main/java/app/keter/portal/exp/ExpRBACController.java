
package app.keter.portal.exp;

import app.keter.portal.base.BaseController;
import com.google.common.collect.Maps;
import com.jfinal.kit.JsonKit;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

/**
 * 用于验证框架RBAC功能
 */
@RestController
public class ExpRBACController extends BaseController {

    @PreAuthorize("hasRole('ADMIN')")
    @GetMapping("/admin/haha")
    public String admin() {
        return JsonKit.toJson("ok-admin-haha");
    }

    @GetMapping("/user/haha")
    public String user() {
        return JsonKit.toJson("ok-user-haha");
    }

    @GetMapping(value="/user/haha/{param}")
    @ResponseBody
    public String haha(@PathVariable String param) {
        Map map = Maps.newHashMap();
        map.put("param",param);
        return JsonKit.toJson(map);
    }

    @PostMapping(value="/user/haha")
    public @ResponseBody String haha() {
        Map map = Maps.newHashMap();
        map.put("success","true");
        return JsonKit.toJson(map);
    }
}