package app.keter.portal.mail;

import com.keter.framework.web.component.http.HttpClienter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

/**
 * Created by gulixing@msn.com on 2019/4/2.
 */
@Component
public class MainConfig {

    @Autowired
    HttpClienter clienter;

    /**
     * 邮件功能专用通信客户端：延长超时时间
     * 跟MailService放在一个类中声明会导致循环引用bean创建失败，不知道为啥！
     */
    @LoadBalanced
    @Bean("mailTemplate")
    RestTemplate mailTemplate(){
        /* 设置邮件读取超时时长 */
        return clienter.restTemplate(10);
    }
}
