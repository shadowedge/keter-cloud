package app.keter.portal.pages;

import app.keter.portal.base.BaseController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;

/**
 * 页面跳转控制器
 */
@Controller
public class PagesController extends BaseController {

    private static final Logger logger = LoggerFactory.getLogger(PagesController.class);

    // 获取环境变量信息
    @Autowired
    protected Environment env;

    @ModelAttribute
    public void appModel(Model model) {
        model.addAttribute("contextPath", env.getProperty("server_context_path"));
    }

    @GetMapping("/ajax/login")
    public String ajaxLogin() {
        return "ajax/login";
    }

    @GetMapping("/ajax/home")
    public String ajaxHome() {
        return "ajax/home";
    }

    @GetMapping("/console/organization")
    public String consoleOrganization(Model model) {
        return "console/organization";
    }

    @GetMapping("/console/user/index")
    public String userIndex() {
        return "user/index";
    }

    // 用户管理
    @GetMapping("/console/user")
    public String user() {
        return "console/user";
    }

}
