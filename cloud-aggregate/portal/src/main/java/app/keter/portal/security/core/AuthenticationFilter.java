package app.keter.portal.security.core;

import app.keter.portal.security.CurrentUser;
import app.keter.portal.security.SecurityConfig;
import app.keter.portal.security.SecurityService;
import com.google.common.base.Strings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.util.AntPathMatcher;
import org.springframework.util.PathMatcher;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Arrays;

@SuppressWarnings("SpringJavaAutowiringInspection")
@Component
public class AuthenticationFilter extends OncePerRequestFilter {
    private static final Logger logger = LoggerFactory.getLogger(AuthenticationFilter.class);

    @Autowired
    private SecurityService security;

    @Value("${jwt.header}")
    private String header;

    @Value("${jwt.tokenHead}")
    private String tokenHead;

    private PathMatcher pathMatcher = new AntPathMatcher();

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain) throws ServletException, IOException {
        // 开放访问的URL
        if (isAllowed(request.getRequestURI())) {
            chain.doFilter(request, response);
            return;
        }

        // 授权访问的URL
        String authHeader = request.getHeader(this.header);
        // 未携带token或token格式不正确
        if (Strings.isNullOrEmpty(authHeader) || !authHeader.startsWith(tokenHead)) {
            response401(response);
            return;
        }

        UserDetailsImpl userDetails;
        try {
            userDetails = security.fromToken(authHeader.substring(tokenHead.length()));
        } catch (Exception e) {
            logger.error("token认证失败！", e.getMessage());
            response401(response);
            return;
        }

        // token校验失败
        if (userDetails == null) {
            response401(response);
            return;
        }

        // token校验通过:装填当前用户
        setCurrentUser(request, userDetails);
        chain.doFilter(request, response);
    }

    private void setCurrentUser(HttpServletRequest request, UserDetailsImpl userDetails) {
        UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(
                userDetails, null, userDetails.getAuthorities());
        authentication.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
//        SecurityService.setCurrentUser(userDetails);
        CurrentUser.setCurrentUser(userDetails);
        SecurityContextHolder.getContext().setAuthentication(authentication);
    }

    private void response401(HttpServletResponse response) throws IOException {
        response.setStatus(401);
        response.getWriter().write("UnAuthenticated!!");
        response.getWriter().flush();
        response.getWriter().close();
    }

    private boolean isAllowed(String url) {
        return Arrays.stream(SecurityConfig.FREE_URL_PATTERNS).anyMatch(p -> pathMatcher.match(p, url));
    }
}