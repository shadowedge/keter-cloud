package app.keter.portal.swagger;

import app.keter.portal.base.BaseController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class SwaggerController extends BaseController {
	private static final Logger logger = LoggerFactory.getLogger(SwaggerController.class);

	@GetMapping("/")
	public String home() {
		return "redirect:/swagger-ui.html";
	}

}
