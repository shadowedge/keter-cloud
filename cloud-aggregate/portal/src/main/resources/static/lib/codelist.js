// 代码表
var codeList = {

    // 组织机构类型
    orgTypes : [
        { value: 'T', label: '技术会员' },
        { value: 'M', label: '医疗会员' },
        { value: 'I', label: '产业会员' },
    ],

    orgStatus : [
        { value: 0, label: '正常' },
        { value: 1, label: '其它' },
    ],

    userStatus : [
        { value: 0, label: '正常'},
        { value: 1, label: '待审批'},
        { value: 2, label: '被拒绝'},
        { value: 3, label: '注销'},
        { value: 4, label: '取消申请'},
        { value: 11, label: '锁定'},
        { value: 12, label: '禁用'},
    ],

    projectTypes : [
        { value: 'R', label: '科研项目' },
        { value: 'C', label: '合作项目' },
    ],

    projectStatus : [
        { value: 0, label: '进行中' },
        { value: 1, label: '审批中' },
        { value: 2, label: '被拒绝' },
        { value: 3, label: '暂停中' },
        { value: 4, label: '已终止' },
        { value: 5, label: '已完成' },
    ],

    deviceStatus : [
        { value: '0', label: '申请中' },
        { value: '1', label: '在用' },
        { value: '2', label: '停用' }
    ],

    cpuType : [
        { value: '0', label: '2' },
        { value: '1', label: '4' },
        { value: '2', label: '8' },
        { value: '3', label: '16' }
    ],

    gpuType : [
        { value: '0', label: '0' },
        { value: '1', label: '1' },
        { value: '2', label: '2' },
        { value: '3', label: '3' },
        { value: '4', label: '4' }
    ],

    memType : [
        { value: '0', label: '4' },
        { value: '1', label: '8' },
        { value: '2', label: '16' },
        { value: '3', label: '32' },
        { value: '4', label: '64' },
        { value: '5', label: '128' }
    ],

    diskType : [
        { value: '0', label: '100' },
        { value: '1', label: '200' },
        { value: '2', label: '500' },
        { value: '3', label: '1024' }
    ],

    osType : [
        { value: '0', label: 'Ubuntu16.04' }
    ],

    frameworkType : [
        { value: '0', label: 'Tensorflow' }
    ],

    associatorStatus : [
            { value: 0, label: '正常' },
            { value: 1, label: '退出' },
    ],

    memberType : [
            { value: 'R', label: '负责人' },
            { value: 'O', label: '普通成员' },
    ],

    assetTypes :[
            { value: 'O', label: '开放数据集'},
            { value: 'R', label: '科研数据集'},
            { value: 'L', label: '限权数据集'},
            { value: 'P', label: '私有数据集'},
    ],

    assetLifeCycles :[
            { value: 'O', label: '原始数据集'},
            { value: 'D', label: '衍生数据集'},
    ],

    assetStatus :[
            { value: 0, label: '正常'},
            { value: 1, label: '失效'},
    ],

    ChinaProvince: [
        { value: '01', label: '北京' },
        { value: '02', label: '天津' },
        { value: '03', label: '上海' },
        { value: '04', label: '重庆' },
        { value: '05', label: '河北' },
        { value: '06', label: '山西' },
        { value: '07', label: '辽宁' },
        { value: '08', label: '吉林' },
        { value: '09', label: '黑龙江' },
        { value: '10', label: '江苏' },
        { value: '11', label: '浙江' },
        { value: '12', label: '安徽' },
        { value: '13', label: '福建' },
        { value: '14', label: '江西' },
        { value: '15', label: '山东' },
        { value: '16', label: '河南' },
        { value: '17', label: '湖北' },
        { value: '18', label: '湖南' },
        { value: '19', label: '广东' },
        { value: '20', label: '海南' },
        { value: '21', label: '四川' },
        { value: '22', label: '贵州' },
        { value: '23', label: '云南' },
        { value: '24', label: '陕西' },
        { value: '25', label: '甘肃' },
        { value: '26', label: '青海' },
        { value: '27', label: '台湾' },
        { value: '28', label: '内蒙古'},
        { value: '29', label: '广西' },
        { value: '30', label: '西藏' },
        { value: '31', label: '宁夏' },
        { value: '32', label: '新疆' },
        { value: '33', label: '香港' },
        { value: '34', label: '澳门' },
    ],

    // 平台批准结果: R-refuse P-passed
    cvApprovalResult: [
        { value: 'R', label: '不同意' },
        { value: 'P', label: '同意' },
    ],
}