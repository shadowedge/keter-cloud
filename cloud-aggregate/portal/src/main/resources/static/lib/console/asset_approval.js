var Main = {
    data () {
        return {
            projects: [], // 未审批数据
            projects2: [], // 已审批数据
            activeName: 'first',
        };
    },

    created () {
        this.getApproveData();
    },

    methods: {
        // 获取待审批数据
        getApproveData () {
            this.myGet('/api/v1/admin/assets/approve/waiting/associator')
            .then(function (node) {
                this.projects = node.data;
            })
            .catch(function (err) {
                this.$message({
                    message: err.error,
                    type: 'error'
                });
            });
        },

        // 获取已审批的数据
        getApproveData2 () {
            this.myGet('/api/v1/admin/assets/approve/associator')
            .then(function (node) {
                this.projects2 = node.data;
                console.info(this.projects2)
            })
            .catch(function (err) {
                this.$message({
                    message: err.error,
                    type: 'error'
                });
            });
        },

        // 发送邮件
        sendMail2User (projectId, associatorId, nimaId) {
            this.myGet('/api/v1/admin/asset/notify/project/' + projectId + '/associator/' + associatorId)
            .then(function (data) {
                // 控制小飞机启动
                $('#' + nimaId)
                .removeClass('dn')
                .addClass('flyaway')
                .one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function() {
                    $('#' + nimaId).removeClass('flyaway').addClass('dn');
                });

                this.$message({
                    message: '邮件已发送给所有者，等待审批',
                    type: 'success'
                });
            })
            .catch(function (err) {
                this.$message({
                    message: '系统忙，邮件发送失败，请稍后再试',
                    type: 'info'
                });
            });

        },

        tabClick (flag) {
            switch (this.activeName) {
                case 'first':
                    this.getApproveData();
                break;
                case 'second':
                   this.getApproveData2();
                break;
            }
        },
    },

    filters: {
       cvApprovalResult (value) {
           let type = codeList.cvApprovalResult.filter(item => item.value == value );
           if (type.length > 0) return type[0].label;
           return "看吧";
        },
    }
}

var Ctor = Vue.extend(Main);
var haha = new Ctor().$mount('#app');
