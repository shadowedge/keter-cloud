var Main = {
    data () {
        return {
            projects: [], // 未审批数据
            projects2: [], // 已审批数据
            activeName: 'first',
            dialogVisible: false,
            dialogVisible2: false,
            applyForm: {},

            radio: false,
        };
    },

    created () {
        this.getApproveData();
    },

    methods: {
        // 获取待审批数据
        getApproveData () {
            this.myGet('/api/v1/admin/assets/approve/waiting/carevault')
            .then(function (node) {
                this.projects = node.data;
            })
            .catch(function (err) {
                console.info(err);
            });
        },

        // 获取已审批的数据
        getApproveData2 () {
            this.myGet('/api/v1/admin/assets/approve/carevault')
            .then(function (node) {
                this.projects2 = node.data;
                console.info(this.projects2)
            })
            .catch(function (err) {
                this.$message({
                    message: err.error,
                    type: 'error'
                });
            });
        },

        // 打开登录框
        openApply (row) {
            this.dialogVisible = true;
            this.applyForm.applicationIds = row.applicant.id;
            this.applyForm.projectId = row.project.id;
            this.applyForm.projectName = row.project.name;
            this.applyForm.associators = row.associators;
            this.applyForm.userName = row.applicant.name;
        },

        // 审批
        agree () {
            if (!this.radio) {
                this.$message({
                    message: '请选择审批意见，同意或不同意',
                    type: 'error'
                });
                return;
            }

            let param = {
                projectId: this.applyForm.projectId,
                assetIds: this.applyForm.associators.map(node => node.assets).flat().map(node => node.asset_id),
                applicationIds: this.applyForm.applicationIds,
                approval: this.radio,
            }

            this.myPost('/api/v1/admin/assets/admin/authorization', param)
            .then(function (res) {
                this.dialogVisible = false;
                this.$message({
                    message: '审批成功，通知邮件已发出',
                    type: 'success'
                });
                this.getApproveData();
            })
            .catch(function (err) {
                this.dialogVisible = false;
                this.$message({
                    message: err.error,
                    type: 'error'
                });
            });
        },

        tabClick (flag) {
            switch (this.activeName) {
                case 'first':
                    this.getApproveData();
                break;
                case 'second':
                   this.getApproveData2();
                break;
            }
        },
    },

    filters: {
        cvApprovalResult (value) {
            let type = codeList.cvApprovalResult.filter(item => item.value == value );
            if (type.length > 0) return type[0].label;
            return "看吧";
        },
    }
}

var Ctor = Vue.extend(Main);
var haha = new Ctor().$mount('#app');
