var Main = {
    data () {
        return {
            state1: '',
            dataSetFromDB: [],
            dataSet: [],

            dataSetCondition: [],
            searchItem: [],
            myDate: '',

            pageNo: 1,
            pageSize: 10,
        };
    },

    created () {
        this.searchConfig().then(function () {
            this.getMixDataSet(this.pageNo, this.pageSize);
        });
    },

    methods: {
        // 查询功能
        querySearch(queryString, cb) {
            let ds = this.dataSetFromDB;
            let results = queryString ? ds.filter(this.createFilter(queryString)) : ds;
            // 调用 callback 返回建议列表的数据
            cb(results);
        },

        createFilter(queryString) {
            return (ds) => {
                return (ds.name.toLowerCase().indexOf(queryString.toLowerCase()) === 0);
            };
        },

        // 查询混合后的数据集
        getMixDataSet (pageNo, pageSize) {
            this.getDataSet()
            .then(data => this.getOrg(data))
            .then(data => this.getUsers(data))
            .then(data => {

                // 混合显示
                let params = [
                    { key: 'associator_id', key2: 'id', srcName: 'name', targetName: 'associator_name' },
                    { key: 'op_id', key2: 'id', srcName: 'name', targetName: 'op_name' },
                    { key: 'provider_id', key2: 'id', srcName: 'name', targetName: 'provider_name' },
                ];
                let result = myMixinData(data, params);
                if (result) {
                    this.dataSetFromDB = result;
                    this.dataSet = result.slice((pageNo - 1) * pageSize, pageNo * pageSize);
                } else {
                    this.dataSetFromDB = [];
                    this.dataSet = [];
                }
            });
        },

        // 查询条件配置
        searchConfig () {
            return this.myGet('/api/v1/admin/assets/browse/my/asset')
            .then(res => {
                let data = res.data.proAssets;
                this.searchItem = [
                    {'title': '数据状态', 'key': 'assetStatus', 'value': codeList.assetStatus},
                    {'title': '原生衍生', 'key': 'assetLifeCycles', 'value': codeList.assetLifeCycles},
                    {'title': '数据类型', 'key': 'assetTypes', 'value': codeList.assetTypes},
                ];
                let item = {
                    title: '数据用途', key: 'assetUsage', 'value': data.map(node => ({ value: node.id, label: node.purpose }))
                };
                this.searchItem.push(item);
                this.getOrg({}).then(data => {
                    // 配置会员查询条件
                    item = {
                        title: '所属会员', key: 'assetMember', 'value': data.b.map(node => ({ value: node.id, label: node.name }))
                    };
                    this.searchItem.push(item);
                });
            })
            .catch(err => {
                this.$message({
                    message: err.error,
                    type: 'error'
                });
            });
        },

        // 获取数据集
        getDataSet () {
            let param = {}
            // 获取筛选条件
            let keys = ['assetStatus', 'assetLifeCycles', 'assetTypes', 'assetUsage', 'assetMember'];
            if (this.state1) param.name = this.state1;
            keys.forEach(key => {
                let temp = this.dataSetCondition.filter(node => node.key == key).map(node => "'" + node.value + "'").join(',');
                if (temp || temp != "") param[key] = temp;
            });
            // 获取筛选日期
            if (this.myDate && this.myDate.length === 2) {
                param.begindate = formatDate(this.myDate[0]);
                param.enddate = formatDate(this.myDate[1]);
            }

            return this.myGet('/api/v1/admin/assets/browse/my/asset', param)
            .then(res => ({ a: res.data.proAssets }) )
            .catch(err => {
                this.$message({
                    message: err.error,
                    type: 'error'
                });
            });
        },

        // 获取组织机构
        getOrg (data) {
            return this.myGet('/api/v1/admin/orgs')
            .then(res => {
                data.b = res.data;
                return data;
            })
            .catch(err => console.info(err));
        },

        // 获取用户信息
        getUsers (data) {
            return this.myGet('/api/v1/admin/users')
            .then(res => {
                data.c = res.data;
                data.d = res.data;
                return data;
            })
            .catch(err => console.info(err));
        },

        // 设置页面显示数量
        sizeChange (pageSize) {
            this.pageSize = pageSize;
            this.pageNo = 1;
            this.getMixDataSet(this.pageNo, this.pageSize);
        },

        // 翻页
        pageChange (pageNo) {
            this.pageNo = pageNo;
            this.getMixDataSet(this.pageNo, this.pageSize);
        },

        // 按名称查询
        getSetByName () {
            this.getMixDataSet(1, this.pageSize);
        },

        // 按多条件查询
        getSetByItem () {
            this.getMixDataSet(1, this.pageSize);
        },

        // 日期查询
        getSetByDate (args) {
//            let dateArray = args.map(node => Date.parse(node));
            this.getMixDataSet(1, this.pageSize);
        },

    },

    mounted () {
    },

    filters: {
       changeAssetType (value) {
           let type = codeList.assetTypes.filter(item => item.value == value );
           if (type.length > 0) return type[0].label;
           return "未知类型";
        },

       changeAssetStatus (value) {
           let status = codeList.assetStatus.filter(item => item.value == value );
           if (status.length > 0) return status[0].label;
           return "未知类型";
       },

       changeAssetLifeCycle (value) {
           let LifeCycle = codeList.assetLifeCycles.filter(item => item.value == value );
           if (LifeCycle.length > 0) return LifeCycle[0].label;
           return "未知类型";
       },
    },

}

var Ctor = Vue.extend(Main);
var haha = new Ctor().$mount('#app');

