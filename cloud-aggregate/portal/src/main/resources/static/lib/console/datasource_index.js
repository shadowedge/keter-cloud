var Main = {
    data () {
        return {
            state1: '',
            dataResourceFromDB: [],
        };
    },

    created () {
        this.getDataResource();
    },

    methods: {
        // 查询功能
        querySearch(queryString, cb) {
            let ds = this.dataResourceFromDB;
            let results = queryString ? ds.filter(this.createFilter(queryString)) : ds;
            // 调用 callback 返回建议列表的数据
            cb(results);
        },

        createFilter(queryString) {
            return (ds) => {
                return (ds.name.toLowerCase().indexOf(queryString.toLowerCase()) === 0);
            };
        },

        // 获取数据集
        getDataResource (pageNo, pageSize) {
            let param = {};
//            if (this.state1 != '') {
//                param.name = this.state1;
//            }
//            if (this.hehe.length > 0) {
//                let value = this.hehe.filter(node => node.key == 'orgType').map(node => node.value);
//                param.type = value.join(',');
//            }
            this.myGet('/api/v1/admin/assets/browse/asset', param)
            .then(function (res) {
                this.dataResourceFromDB = res.data.proAssets;
//                this.orgData = res.data.slice((pageNo - 1) * pageSize, pageNo * pageSize);
            })
            .catch(function (err) {
                this.$message({
                    message: err.error,
                    type: 'error'
                });
            });
        },

        // 按名称查询
        getResourceByName () {

        },
    },

    mounted () {
    },

    filters: {
    },

}

var Ctor = Vue.extend(Main);
var haha = new Ctor().$mount('#app');
