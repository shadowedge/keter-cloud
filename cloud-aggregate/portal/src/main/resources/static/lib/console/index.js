var Main = {
    data () {
        return {
            daTou: [
                { name: '东软-盛京合作项目', num: 6, num2: 1 },
                { name: '心电 心律异常研究', num: 5, num2: 1 },
                { name: '东软医大心肺研究', num: 2, num2: 2 },
                { name: '阿尔兹海默症早期诊断 医大一', num: 2, num2: 2 },
            ],

            dataSet: [
                { img: '../images/haha93.png', name: 'MIT数据集', dep: 'MIT' },
                { img: '../images/haha94.png', name: '熙康静态12导联心电数据集', dep: '熙康' },
                { img: '../images/haha95.png', name: '盛京医院 MRI脑部数据集', dep: '盛京医院' },
                { img: '../images/haha97.png', name: '眼科医院眼底镜数据集', dep: '眼科医院' },
                { img: '../images/haha98.png', name: '肿瘤医院病理数据集', dep: '肿瘤医院' },
                { img: '../images/haha99.png', name: 'MIT数据集', dep: 'MIT' },
            ],

            chartSettings: {
                area: true
            },

            chartData: {
                columns: ['日期', '使用量'],
                rows: [
                    { '日期': 'Jan', '使用量': 1393 },
                    { '日期': 'Feb', '使用量': 3530 },
                    { '日期': 'Mar', '使用量': 2923 },
                    { '日期': 'Apr', '使用量': 1723 },
                    { '日期': 'May', '使用量': 3792 },
                    { '日期': 'June', '使用量': 4593 }
                ]
            },

            chartData2: {
                columns: ['日期', '下载量'],
                rows: [
                    { '日期': 'Jan', '下载量': 393 },
                    { '日期': 'Feb', '下载量': 530 },
                    { '日期': 'Mar', '下载量': 923 },
                    { '日期': 'Apr', '下载量': 723 },
                    { '日期': 'May', '下载量': 792 },
                    { '日期': 'June', '下载量': 593 }
                ]
            },

            chartExtend: {
                yAxis: {
                    splitLine:{
                        show: true,
                        lineStyle: {
                            color: ['#DCDFE6'],
                            type:'dashed',
                        }
                    },//去除网格线
                    axisLine: {
                        show: false,
                        lineStyle: {
                            color: '#9fa2a9',
                            width: 1
                        }
                    }
                },
                xAxis: {
                    splitLine:{show: false},//去除网格线
                    axisLine: {
                        show: false,
                        lineStyle: {
                            color: '#9fa2a9',
                            width: 1
                        }
                    }
                }
            },

        };
    },

    created () {

    },

    methods: {
    },

    mounted () {
    },

    filters: {
    },

}

var Ctor = Vue.extend(Main);
var haha = new Ctor().$mount('#app');
