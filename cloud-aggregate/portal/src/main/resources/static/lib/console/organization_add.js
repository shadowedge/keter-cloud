var Main = {
    data () {
        return {
            orgForm: {
                name: '',
                address: '',
                type: '',
                email: '',
                contact: '',
                contact_phone: '',
                contact_mobile: '',
            },

            orgTypes : codeList.orgTypes,

            orgRules: {
                name: [
                    { required: true, message: '名称不能为空', trigger: 'blur' },
                    { min: 3, max: 50, message: '长度在 3 到 50 个字符', trigger: 'blur' }
                ],
                address: [
                    { required: true, message: '地址不能为空', trigger: 'blur' },
                ],
                type: [
                    { required: true, message: '类型不能为空', trigger: 'change' },
                ],
                email: [
                    { required: true, message: '类型不能为空', trigger: 'blur' },
                    { type: 'email', message: '请输入正确的邮箱地址', trigger: ['blur', 'change'] }
               ],
           },

        };
    },

    created () {
        if (id) this.getOrganizationById();
    },

    methods: {
        // 获取组织机构
        getOrganizationById () {
            this.myGet('/api/v1/admin/orgs/' + id)
            .then(function (res) {
                this.orgForm = res.data;
            })
            .catch(function (err) {
                this.$message({
                    message: err.error,
                    type: 'error'
                });
            });
        },

        // 添加组织机构
        saveOrganization (formName) {
            let url = ''
                , method = null;
            if (!id) { // 新增
                url = '/api/v1/admin/orgs';
                method = this.myPost;
            } else { // 修改
                url = '/api/v1/admin/orgs/' + id;
                method = this.myPatch;
            }

            this.$refs[formName].validate((valid) => {
                if (valid) {
                    method.call(this, url, this.orgForm)
                    .then(function (res) {
                        this.$refs.orgForm.resetFields();
                        this.$message({
                            message: '保存成功',
                            type: 'success'
                        });
                        setTimeout(() => {
                            window.location.href = '/console/organization';
                        }, 800);
                    })
                    .catch(function (err) {
                        this.$message({
                            message: err.error,
                            type: 'error'
                        });
                    });
                } else {
                  return false;
                }
            });
        },
    },

    mounted () {
    },

    filters: {
        changeOrgType (value) {
            let type = codeList.orgTypes.filter(item => item.value == value );
            if (type.length > 0) return type[0].label;
            return "未知类型";
        }
    },

}

var Ctor = Vue.extend(Main);
var haha = new Ctor().$mount('#app');
