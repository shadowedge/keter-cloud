var Main = {
    data () {
        return {
            orgCurrent: null,
            orgCurrentPageNo: 1,
            orgData: [],
            orgDataFromDB: [],
            orgPageSize: 10,


            showProvinceFlag: false,
            searchItem: [
                {'title': '会员种类', 'key': 'orgType', 'value': codeList.orgTypes},
//                {'title': '会员地址', 'key': 'orgAddress', 'value': codeList.ChinaProvince},
            ],

            state1: '',
            hehe: [],
        };
    },

    created () {
        let instance = this;
        this.orgTypes = codeList.orgTypes;
        this.getOrganization(this.orgCurrentPageNo, this.orgPageSize);
    },

    methods: {
        // 查询功能
        querySearch(queryString, cb) {
            let restaurants = this.orgDataFromDB;
            let results = queryString ? restaurants.filter(this.createFilter(queryString)) : restaurants;
            // 调用 callback 返回建议列表的数据
            cb(results);
        },

        createFilter(queryString) {
            return (restaurant) => {
                return (restaurant.name.toLowerCase().indexOf(queryString.toLowerCase()) === 0);
            };
        },

        // 获取组织机构
        getOrganization (pageNo, pageSize) {
            let param = {};
            if (this.state1 != '') {
                param.name = this.state1;
            }
            if (this.hehe.length > 0) {
                let value = this.hehe.filter(node => node.key == 'orgType').map(node => node.value);
                param.type = value.join(',');
            }
            this.myGet('/api/v1/admin/orgs/query', param)
            .then(function (res) {
                this.orgDataFromDB = res.data;
                this.orgData = res.data.slice((pageNo - 1) * pageSize, pageNo * pageSize);
            })
            .catch(function (err) {
                this.$message({
                    message: err.error,
                    type: 'error'
                });
            });
        },

        // 按照名称查询
        getOrganizationByName () {
            this.getOrganization(1, this.orgPageSize);
        },

        // 按照条件查询组织机构
        getOrganizationByCondition (item, items) {
            this.getOrganization(1, this.orgPageSize);
        },

        // 设置每页显示的记录数
        handleOrgSizeChange (pageSize) {
            this.orgPageSize = pageSize;
            this.orgCurrentPageNo = 1;
            this.getOrganization(1, this.orgPageSize);
        },

        // 组织机构分页
        handleOrgPageChange (pageNo) {
            this.orgCurrentPageNo = pageNo;
            this.getOrganization(pageNo, this.orgPageSize);
        },

        // 跳转到组织机构新增和修改页面
        openOrganizationBox (item) {
            if (item) {
                window.location.href = '/console/organization/update/' + item.id;
            } else {
                window.location.href = '/console/organization/add';
            }
        },

        // 删除组织机构
        orgHandleDelete () {
            this.$confirm('此操作将永久删除该组织, 是否继续?', '提示', {
                confirmButtonText: '确定',
                cancelButtonText: '取消',
                type: 'warning'
            }).then(() => {
                this.$message({
                    type: 'success',
                    message: '暂不支持删除!'
                });
            }).catch(() => {
              this.$message({
                  type: 'info',
                  message: '已取消删除'
              });
            });
        },

        // 跳转到添加用户界面
        openUserManageBox (item) {
            window.location.href = '/console/organization/manage/user/' + item.id;
        },


        // 查询角色
        getRoles () {
            this.myGet('/auth/me')
            .then(function (res) {
                console.info(res);
            })
            .catch(function (err) {
                console.info(err);
            });
        },
    },

    mounted () {
    },

    filters: {
        changeOrgType (value) {
            let type = codeList.orgTypes.filter(item => item.value == value );
            if (type.length > 0) return type[0].label;
            return "未知类型";
        }
    },

}

var Ctor = Vue.extend(Main);
var haha = new Ctor().$mount('#app');
