var Main = {
    data () {
        return {
            orgForm: {
                name: '',
                address: '',
                type: '',
                email: '',
                contact: '',
                contact_phone: '',
                contact_mobile: '',
                created: '',
            },
            dialogVisible: false,
            users: [],

            usersD: [], usersDB: [], usersDB2: [], userD: '', pageNo: 1,
            bindUser: [],

        };
    },

    created () {
        this.getOrganizationById();
        this.getUsers();
    },

    methods: {
        // 获取组织机构
        getOrganizationById () {
            this.myGet('/api/v1/admin/orgs/' + id)
            .then(function (res) {
                this.orgForm = res.data;
            })
            .catch(function (err) {
                this.$message({
                    message: err.error,
                    type: 'error'
                });
            });
        },

        // 获取组织机构下的用户列表
        getUsers () {
            this.myGet('/api/v1/admin/orgs/' + id + '/users')
            .then(function (res) {
                this.users = res.data;
            })
            .catch(function (err) {
                this.$message({
                    message: err.error,
                    type: 'error'
                });
            });
        },

        // 获取未分配组织的用户
        getEmptyUsers (pageNo, pageSize) {
            this.myGet('/api/v1/admin/orgs/mangliu')
            .then(function (res) {
                this.pageNo = pageNo;
                this.usersDB2 = res.data;
                if (this.usersD && this.userD != '') {
                    this.usersDB = res.data.filter(node => node.name.indexOf(this.userD) === 0);
                    this.usersD = res.data.filter(node => node.name.indexOf(this.userD) === 0).slice((pageNo - 1) * pageSize, pageNo * pageSize);
                } else {
                    this.usersDB = res.data;
                    this.usersD = res.data.slice((pageNo - 1) * pageSize, pageNo * pageSize);
                }
            })
            .catch(function (err) {
                this.$message({
                    message: err.error,
                    type: 'error'
                });
            });
        },

        // 查询用户
        querySearch (queryString, cb) {
            let users = this.usersDB2;
            let results = queryString ? users.filter(node => node.name.indexOf(queryString) === 0 || node.username.indexOf(queryString) === 0) : users;
            cb(results);
        },

        querySelect (item) {
            this.getEmptyUsers(1, 5);
        },

        openBox () {
            this.dialogVisible = true;
            this.getEmptyUsers(1, 5);
        },

        // 解绑
        unbind (user) {
            this.$confirm('您确定要解绑该用户吗?', '提示', {
                confirmButtonText: '确定',
                cancelButtonText: '取消',
                type: 'warning'
            })
            .then(() => {
                this.$message({
                    type: 'success',
                    message: '解绑成功!'
                });
                this.myDelete('/api/v1/admin/orgs/' + id + '/users/' + user.id)
                .then(res => {
                    this.getUsers();
                })
                .catch(err => {
                    this.$message({
                        type: 'error',
                        message: err.error
                    });
                });
            })
        },

        // 绑定
        bind () {
            if (this.bindUser.length == 0) {
                this.$message({
                    message: '请选择用户！',
                    type: 'error'
                });
                return;
            }
            this.myGet('/api/v1/admin/orgs/' + id + '/users/' + this.bindUser.map(node => node.id).join(','), {})
            .then(res => {
                this.$message({
                    message: '绑定成功',
                    type: 'success'
                });
                this.dialogVisible = false;
                this.getUsers();
            })
            .catch(err => {
                this.$message({
                    message: err.error,
                    type: 'error'
                });
            })
        },

        // 选择
        selectionChange (data) {
            this.bindUser = data;
        },

        pageChange (pageNo) {
            this.pageNo = pageNo;
            this.usersD = this.usersDB.slice((pageNo - 1) * 5, pageNo * 5);
        },
    },

    mounted () {
    },

    filters: {
        changeOrgType (value) {
            let type = codeList.orgTypes.filter(item => item.value == value );
            if (type.length > 0) return type[0].label;
            return "未知类型";
        }
    },

}

var Ctor = Vue.extend(Main);
var haha = new Ctor().$mount('#app');
