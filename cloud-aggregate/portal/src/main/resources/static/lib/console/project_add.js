var Main = {
    data () {
        return {
            // 项目信息
            projectForm: {
                name: '',
                type: '',
                contact_id: '',
                contact_name: '',
                rcontext: '',
                status: '',
                memo: '',
            },

            projectRules: {
                name: [
                    { required: true, message: '名称不能为空', trigger: 'blur' },
                    { min: 4, max: 64, message: '长度在 4 到 64 个字符', trigger: 'blur' }
                ],
                type: [
                    { required: true, message: '类型不能为空', trigger: 'change' },
                ],
                rcontext: [
                    { required: true, message: '研究方向不能为空', trigger: 'change' },
                ],
                contact_name: [
                    { required: true, message: '联系人不能为空', trigger: 'change' },
                ],
                status: [
                     { required: true, message: '状态不能为空', trigger: 'change' },
                ],
            },

            orgData: [],
            projectTypes : [],

            projectStatus : [],
            userstatus: [],

            personName: '',
            puserName: '',
            OrgId: '',

            // 用户信息
            dialogVisible: false,
            userDataFromDB: [],
            userData: [],
//            user: {
//                id: '',
//                personname: '',
//            },
            userPageNo: 1,
            userPageSize: 5,
        };
    },

    created () {
        this.projectTypes = codeList.projectTypes;
        this.projectStatus = codeList.projectStatus;
        this.getProject();
    },

    methods: {

        // 获取项目
        getProject () {
            if (id == '') return;
            return this.myGet('/api/v1/admin/projects/' + id)
            .then(function (res) {
                let project = res.data;
                this.projectForm.contact_id = project.contact_id;
                Object.assign(this.projectForm, project);
                this.getUserById(project.contact_id);
            })
            .catch(function (err) {
                // 错误处理
            });
        },

        // 按照id查询用户
        getUserById (userId) {
            this.myGet('/api/v1/admin/users/' + userId)
            .then(function (res) {
                let user = res.data;
                this.projectForm.contact_name = user.name;
            })
            .catch(function (err) {
                // 错误处理
            });
        },

        getUserbycondition (pageNo, pageSize) {
            this.userStatus = codeList.userStatus;
            url = '/api/v1/admin/users/user/like';
            i=0;
            if (this.personName != '')  {
                url = url+'?name='+this.personName;
                i = i+1;
            }
            if (this.puserName != ''){
                if (i>0){
                    url = url+'&username='+this.puserName;
                }else{
                    url = url+'?username='+this.puserName;
                }
                i = i+2;
            }
            if (this.OrgId){
                if (i>0){
                    url = url+'&orgid='+this.OrgId;
                }else{
                    url = url+'?orgid='+this.OrgId;
                }
            }
            this.myGet(url)
            .then(function (res) {
                this.dialogVisible = true;
                this.userDataFromDB = res.data;
                this.userData = res.data.slice((pageNo - 1) * pageSize, pageNo * pageSize);
            })
            .catch(function (err) {
                this.$message({
                    message: err.error,
                    type: 'error'
                });
            });
        },

        //查询组织结构
        getOrgs () {
            this.myGet('/api/v1/admin/orgs')
            .then(function (res) {
                this.orgData  = res.data;
                //console.info(this.orgData);
            })
            .catch(function (err) {
                // 错误处理
            });
        },

        // 获取所有用户
        getUser (pageNo, pageSize, callback) {
            this.getOrgs();
            this.myGet('/api/v1/admin/users/user/like')
            .then(function (res) {
                this.dialogVisible = true;
                this.userDataFromDB = res.data;
                this.userData = res.data.slice((pageNo - 1) * pageSize, pageNo * pageSize);
                if (callback && this.userDataFromDB.length > 0) callback(this.userDataFromDB);
            })
            .catch(function (err) {
                this.$message({
                    message: err,
                    type: 'error'
                });
            });

        },

        // 选择用户
        chooseUser (user) {
            this.projectForm.contact_name = user.name;
            this.projectForm.contact_id = user.id;
            this.dialogVisible = false;
        },

        // 用户分页点击事件
        handleUserPageChange (pageNo) {
            this.userPageNo = pageNo;
            this.getUserbycondition(pageNo,this.userPageSize);
        },

        // 保存项目
        saveProject () {
            let url = ''
                , method = null;
            if (id == '') { // 新增
                url = '/api/v1/admin/projects/create';
                method = this.myPost;
            } else { // 修改
                url = '/api/v1/admin/projects/' + id
                method = this.myPatch;
            }

            this.$refs.projectForm.validate((valid) => {
                if (valid) {
                    method.call(this, url, {
                        name: this.projectForm.name,
                        type: this.projectForm.type,
                        rcontext: this.projectForm.rcontext,
                        contact_id: this.projectForm.contact_id,
                        memo: this.projectForm.memo,
                        status: this.projectForm.status,
                    })
                    .then(function (res) {
                        this.$refs.projectForm.resetFields();
                        this.$message({
                            message: '保存成功',
                            type: 'success'
                        });
                        setTimeout(() => {
                            window.history.go(-1);
                        }, 1000);
                    })
                    .catch(function (err) {
                        this.$message({
                            message: err.error,
                            type: 'error'
                        });
                    });

                } else {
                  return false;
                }
            });
        }
    },

    filters: {
        changeuserStatus (value) {
                let status = codeList.userStatus.filter(item => item.value == value );
                if (status.length > 0) return status[0].label;
                return "未知类型";
        },

        userFilter (id) {

        }
    }
}

var Ctor = Vue.extend(Main);
new Ctor().$mount('#app');
