var Main = {
    data () {
        return {
            dialogVisible: false,

            // 项目信息
            project: {},
            proAssetMultipleSelect: [],
            proAssetDataFromDB: [],
            proAssetData: [],
            proAssetCurrentPageNo: 1,
            proAssetPageNo: 1,
            proAssetPageSize: 10,
            allCheck: false,

            // ....................................

            // 数据集信息
            assetMultipleSelect: [],
            assetTypes : [],
            assetStatus : [],
            assetLifeCycle : [],
            assetDate : [],
            orgName: '',
            orgId: '',


            assetDataFromDB: [],
            assetData: [],
            assetCurrentPageNo: 1,
            assetPageNo: 1,
            assetPageSize: 5,

            assetName: '',
            assetType: '',
            assetPurpose: '',
            assetAbstract: '',
            assetId: '',
            startdate: '',
            enddate: '',

            assetRules: {
                name: [
                    { max: 64, message: '长度少于64个字符', trigger: 'blur' }
                ],
                purpose: [
                    { max: 128, message: '长度少于128个字符', trigger: 'blur' },
                ],
                abstract: [
                    { max: 255, message: '长度少于128个字符', trigger: 'blur' },
                ],
                id: [
                    { type: 'number', message: '请输入数字，最大长度11位！', trigger: ['blur,change'] }
                ],
            },
            orgData: [],

            // 获取项目
            userData: [],
        };
    },

    created () {
        this.getProject().then(function () {
            this.getProAssetById(this.proAssetCurrentPageNo);
        });
    },

    methods: {

        // 获取项目
        getProject () {
            if (id == '') return;
            return this.myGet('/api/v1/admin/projects/' + id)
            .then(function (res) {
                this.project = res.data;
            })
            .catch(function (err) {
                 this.$message({
                      message: err.error,
                      type: 'error'
                 });
            });
        },

        // 按照项目id查询项目数据集
        getProAssetById (pageNo) {
            j = 0;
            var tempdata,orgs,pageorgs;
            this.myGet('/api/v1/admin/assets/browse/authorization?projectid='+id)
            .then(function (res) {
                if (!$.isEmptyObject(res.data)){
                    this.proAssetData = res.data['proAssets'];
                    this.proAssetMultipleSelect = [];
                    orgs = res.data['orgs'];
                    for (var i = this.proAssetData.length - 1; i >= 0; i--) {
                        this.proAssetData[i].hehe = false;
                        this.proAssetMultipleSelect.push(false);
                        if (this.proAssetData[i].cv_approval_result === 'D') {
                            this.proAssetData.splice(i, 1);
                        } else {
                            orgs.forEach((node2) => {
                                if (node2.id == this.proAssetData[i].associator_id) {
                                    this.proAssetData[i].orgname = node2.name;
                                    this.proAssetData[i].address = node2.address;
                                    return;
                                }
                            });
                        }
                    }
                    pageorgs = this.proAssetData;
                    this.proAssetDataFromDB = this.proAssetData;
                    this.proAssetData = pageorgs.slice((pageNo - 1) * this.proAssetPageSize, pageNo * this.proAssetPageSize);
                }
            })
            .catch(function (err) {
                console.info(err);
                this.$message({
                    message: err.error,
                    type: 'error'
                });
            });
        },

        //查询组织结构
        getOrgs () {
            this.myGet('/api/v1/admin/orgs')
            .then(function (res) {
                this.orgData  = res.data;
            })
            .catch(function (err) {
                 this.$message({
                      message: err.error,
                      type: 'error'
                 });
            });
        },

        //按ID,名称、类型、用途、摘要、组织机构、注册时间查询数据集
        browseAssetHanldeByMultipleItem(pageNo,callback){
            var orgs,pageorgs;
            url = '/api/v1/admin/assets/browse/asset';
            i=0;
            if (this.assetId){
                url = url+'?id='+this.assetId;
                i = i+1;
            }
            if (this.assetName){
                if (i>0){
                   url = url+'&name='+this.assetName;
                }else{
                   url = url+'?name='+this.assetName;
                }
                i = i+1;
            }
            if (this.assetType){
                if (i>0){
                   url = url+'&type='+this.assetType;
                }else{
                   url = url+'?type='+this.assetType;
                }
                i = i+1;
            }
            if (this.orgId){
                if (i>0){
                   url = url+'&orgid='+this.orgId;
                }else{
                   url = url+'?orgid='+this.orgId;
                }
                i = i+1;
            }
            if (this.assetPurpose){
                if (i>0){
                   url = url+'&purpose='+this.assetPurpose;
                }else{
                   url = url+'?purpose='+this.assetPurpose;
                }
                i = i+1;
            }
            if (this.assetAbstract){
                if (i>0){
                   url = url+'&asabstract='+this.assetAbstract;
                }else{
                   url = url+'?asabstract='+this.assetAbstract;
                }
                i = i+1;
            }
            if (this.startdate){
               if (i>0){
                  url = url + '&begindate='+this.startdate;
               }else{
                  url = url + '?begindate='+this.startdate;
               }
               i = i+1;
            }
            if (this.enddate){
               if (i>0){
                  url = url + '&enddate='+this.enddate;
               }else{
                  url = url + '?enddate='+this.enddate;
               }
            }

            this.myGet(url)
            .then(function (res) {
               if (!$.isEmptyObject(res.data)){  //判断返回结果是否为空,空返回true.
                  this.assetData = res.data['proAssets'];
                  orgs = res.data['orgs'];

                  let haha = this.assetData.map((node) => {
                    orgs.forEach((node2) => {
                        if (node2.id == node.associator_id) {
                            node.orgname = node2.name;
                            node.address = node2.address;
                            return;
                        }
                    });
                    return node;
                  });

                  for (var i = this.assetData.length - 1; i >= 0; i--) {
                     if (this.assetData[i].status == 1) {
                         this.assetData.splice(i, 1);
                     }
                  }
               }else{
                 this.assetData = '';
               }
               pageorgs = this.assetData;
               this.assetDataFromDB = this.assetData;
               this.assetData = pageorgs.slice((pageNo - 1) * this.assetPageSize, pageNo * this.assetPageSize);
               if (callback && this.assetDataFromDB.length > 0) callback(this.assetDataFromDB);
            })
            .catch(function (err) {
                 this.$message({
                      message: err.error,
                      type: 'error'
                 });
            });
        },

        setTime (value) {
            if (value){
               this.startdate = value[0];
               this.enddate = value[1];
            }else{
               this.startdate = '';
               this.enddate = '';
            }
        },

        //选中或取消
        toggleSelection(rows) {
           if (rows) {
              rows.forEach(row => {
                 this.$refs.proAssetTable.toggleRowSelection(row);
              });
           } else {
                 this.$refs.proAssetTable.clearSelection();
           }
        },

        //选中或取消
        assettoggleSelection(rows) {
           if (rows) {
              rows.forEach(row => {
                 this.$refs.assetTable.toggleRowSelection(row);
              });
           } else {
                 this.$refs.assetTable.clearSelection();
           }
        },

        //check选择记录
        handleSelectionAssetChange(val) {
             this.assetMultipleSelect = val;
        },


        //点击添加项目数据集按钮
        proAssetHanldeAdd(){
            this.dialogVisible = true;
            this.assetTypes = codeList.assetTypes;
            this.getOrgs ();
            this.browseAssetHanldeByMultipleItem(this.assetCurrentPageNo);
        },

        //单击数据集确定按钮
        addAssetToProject(){
           i = 0;
           var aIds='';
           for ( i = 0; i< this.assetMultipleSelect.length;i++){
               if  (this.assetMultipleSelect[i].type ==='O'){
                   i = 0;
                   alert('数据集:'+this.assetMultipleSelect[i].file_name+'  为开放数据集,无需申请.请直接使用!');
                   return;
               }
               if  (this.assetMultipleSelect[i].type ==='P'){
                   i = 0;
                   alert('数据集:'+this.assetMultipleSelect[i].file_name+'  是私有数据集,您无权申请!');
                   return;
               }
               if (i == 0) {
                  aIds =  this.assetMultipleSelect[i].id;
               } else{
                  aIds = aIds+','+this.assetMultipleSelect[i].id;
               }
           }
           if (i > 0){
               this.myPost('/api/v1/admin/projects/'+id+'/assets',{
                             assetIds: aIds,
                          })
               .then(function (res) {
                    this.$message({
                        message: '数据集加入项目成功',
                        type: 'success'
                    });
               })
               .catch(function (err) {
                    this.$message({
                        message: err.error,
                        type: 'error'
                    });
               });
           }else{
               alert('请选择数据集后再确认!');
           }
        },

        // 选择全部项目
        checkAll (item) {
            this.proAssetMultipleSelect = [];
            if (item) {
                this.proAssetData.forEach(node => {
                    this.proAssetMultipleSelect.push(node.id);
                    node.hehe = true;
                });
            } else {
                this.proAssetData.forEach(node => {
                    this.proAssetMultipleSelect.push(false);
                    node.hehe = false;
                });
            }
        },

        // 点击删除项目数据集按钮
        proAssetHanldeDel () {
            var aIds = this.proAssetMultipleSelect.filter(node => node);
            if (aIds.length === 0) {
                this.$message({ message: '请选择数据集后,再点击删除项目数据集!', type: 'error' });
                return;
            }
            this.$confirm('此操作将该数据集从项目中删除,并且不能为本项目再次申请该数据集 是否继续?', '提示', {
                confirmButtonText: '确定',
                cancelButtonText: '取消',
                type: 'warning',
            }).then(() => {
                this.myPost('/api/v1/admin/projects/delete/hehe/' + id + '/assets', {assetIds: aIds})
                .then(function (res) {
                    this.$message({
                        message: '删除成功',
                        type: 'success',
                    });
                    this.getProAssetById(id,this.proAssetPageNo, this.proAssetPageSize);
                })
                .catch(function (err) {
                   this.$message({
                       message: err.error,
                       type: 'error',
                   });
                });
            })
            .catch(() => this.$message({ type: 'info', message: '已取消' }));
        },

        //点击返回按钮
        goback(){
          this.assetName = '';
          this.assetType = '';
          this.assetPurpose = '';
          this.assetAbstract = '';
          this.assetId = '';
          this.startdate = '';
          this.enddate = '';
          this.dialogVisible = false;
        },

        // 用户分页点击事件
        handleAssetPageChange (pageNo) {
            this.assetCurrentPageNo = pageNo;
            this.browseAssetHanldeByMultipleItem(pageNo);
        },

        handleAssetPageSizeChange(psize) {
            this.assetPageSize = psize;
            this.browseAssetHanldeByMultipleItem(this.assetCurrentPageNo);
        },

        // 用户分页点击事件
        handleProAssetPageChange (pageNo) {
            this.proAssetCurrentPageNo = pageNo;
            this.getProAssetById(id,pageNo);
        },

        // 设置显示页面分页
        handleProAssetPageSizeChange(psize) {
            this.proAssetPageSize = psize;
            this.getProAssetById(id,this.proAssetCurrentPageNo);
        },


    },

    filters: {
        changeAssetType (value) {
            let type = codeList.assetTypes.filter(item => item.value == value );
            if (type.length > 0) return type[0].label;
            return "未知类型";
        },

        changeAssetStatus (value) {
            let status = codeList.assetStatus.filter(item => item.value == value );
            if (status.length > 0) return status[0].label;
            return "未知类型";
        },

        changeAssetLifeCycle (value) {
            let LifeCycle = codeList.assetLifeCycles.filter(item => item.value == value );
            if (LifeCycle.length > 0) return LifeCycle[0].label;
            return "未知类型";
        },

        changeProjectType (value) {
            let type = codeList.projectTypes.filter(item => item.value == value );
            if (type.length > 0) return type[0].label;
            return "未知类型";
        },

        changeProjectStatus (value) {
            let status = codeList.projectStatus.filter(item => item.value == value );
            if (status.length > 0) return status[0].label;
            return "未知类型";
        },

        cvApprovalResult (value) {
            let status = codeList.cvApprovalResult.filter(item => item.value == value );
            if (status.length > 0) return status[0].label;
            return "看吧";
        },
    },

    watch: {
        proAssetMultipleSelect (newArgs, oldArgs) {
            let csl = newArgs.filter(node => node).length;
            let tdl = this.proAssetData.length;
            if (csl === tdl) {
                this.allCheck = true;
            } else {
                this.allCheck = false;
            }
        }
    }
}

var Ctor = Vue.extend(Main);
new Ctor().$mount('#app');
