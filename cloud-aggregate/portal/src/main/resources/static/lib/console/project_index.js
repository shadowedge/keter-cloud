﻿var Main = {
    data () {
        return {
            dialogVisible0: false,
            dialogVisible1: false,
            dialogVisible2: false,

            isUpdate: false,
            isUpdate2: false,

            v_projectId: null,
            v_orgId: null,

            projectTypes : [],
            projectStatuses : [],
            proDate: [],

            proType: '',
            proName: '',
            proRContext: '',
            proStatus: '',
            startdate: '',
            enddate: '',
            projectCurrent: null,
            projectCurrentPageNo: 1,
            projectData: [],
            projectDataFromDB: [],
            projectPageSize: 10,
        };
    },

    created () {
        let instance = this;
        this.projectTypes = codeList.projectTypes;
        this.projectStatuses = codeList.projectStatus;
        this.getproject(this.projectCurrentPageNo);

    },

    methods: {
        // 查询项目（按名称、类型）
        getproject (pageNo) {
            var url = '/api/v1/admin/projects/browse/projects?1=1';
            if (this.proName) {
                url = url + '&name='+this.proName;
            }
            if (this.proType){
                url = url + '&type='+this.proType;
            }
            if (this.proRContext){
                url = url + '&rcontext='+this.proRContext;
            }
            if (this.proStatus != null){
                console.info(this.proStatus);
                url = url + '&status='+this.proStatus;
            }
            if (this.startdate){
                url = url + '&begindate='+this.startdate;
            }
            if (this.enddate){
                url = url + '&enddate='+this.enddate;
            }

            this.myGet(url)
            .then(function (res) {
                this.projectDataFromDB = res.data;
                this.projectData = res.data.slice((pageNo - 1) * this.projectPageSize, pageNo * this.projectPageSize);
            })
            .catch(function (err) {
                this.$message({
                    message: err.error,
                    type: 'error'
                });
            });
        },

        // 项目分页
        handleprojectPageChange (pageNo) {
            this.projectCurrentPageNo = pageNo;
            this.getproject(pageNo);
        },

        // 设置显示页面分页
        handleSizeChange(psize,pageNo) {
            this.projectPageSize = psize;
            this.getproject(this.projectCurrentPageNo);
        },

        // 打开对话框，调用增加页面
        projectHanldeAdd () {
            window.location.href='/console/project/add';
        },

        // 修改组织机构
        projectHandleEdit(index,projectid) {
            window.location.href='/console/project/update/'+projectid;
        },

        projectHandleAsscoiator (index,projectid) {
            window.location.href='/console/project/'+projectid+'/associators';
        },

        projectHandleMember (index,projectid) {
            window.location.href='/console/project/'+projectid+'/members';
        },

        projectHandleAsset (index,projectid) {
            window.location.href='/console/project/'+projectid+'/asset';
        },

        setTime (value) {
            if (value){
               this.startdate = value[0];
               this.enddate = value[1];
            }else{
               this.startdate = '';
               this.enddate = '';
            }
        },

    },

    filters: {
        changeProjectType (value) {
            let type = codeList.projectTypes.filter(item => item.value == value );
            if (type.length > 0) return type[0].label;
            return "未知类型";
        },
        changeProjectStatus (value) {
            let status = codeList.projectStatus.filter(item => item.value == value );
            if (status.length > 0) return status[0].label;
            return "未知类型";
        },
    }

}

var Ctor = Vue.extend(Main);
var haha = new Ctor().$mount('#app');
