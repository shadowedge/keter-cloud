var Main = {
    data () {
        return {
            dialogVisible: false,
            dialogVisible1: false,

            // 项目信息
            project: {
                id: '',
                name: '',
                type: '',
                contact_id: '',
                rcontext: '',
                status: '',
                memo: '',
            },
            orgName: '',
            orgType: '',
            proorgDataFromDB: [],
            proorgData: [],
            proorgCurrentPageNo: 1,
            proorgPageNo: 1,
            proorgPageSize: 10,
            orgCurrent: null,
            orgCurrentPageNo: 1,
            orgData: [],
            orgDataFromDB: [],
            orgPageSize: 10,
            userDataFromDB: [],
            userDataSelect: [],
            oldUserData: [],
            allCheck: false,
            heheSelect: [],
        };
    },

    created () {
        this.projectTypes = codeList.projectTypes;
        this.projectStatus = codeList.projectStatus;
        this.getProject().then(function () {
            this.getProOrgId(id,this.proorgPageNo);
        });
    },

    methods: {

        // 获取项目
        getProject () {
            if (id == '') return;
            return this.myGet('/api/v1/admin/projects/' + id)
            .then(function (res) {
                let project = res.data;
                Object.assign(this.project, project);
            })
            .catch(function (err) {
                // 错误处理
            });
        },

        // 按照项目id查询项目会员
        getProOrgId (id,pageNo) {
            var tempdata2,orgs,pageorgs;
            this.myGet('/api/v1/admin/projects/query/project/orgs?id='+id)
            .then(function (res) {
                tempdata2 = res.data;
                if (tempdata2.proOrgs) {
                    this.proorgData = tempdata2['proOrgs'];
                    orgs = tempdata2['Orgs'];
                    for (var i = this.proorgData.length - 1; i >= 0; i--) {
                        if (this.proorgData[i].status == 1) {
                            this.proorgData.splice(i, 1);
                        } else {
                            orgs.forEach((node2) => {
                                if (node2.id == this.proorgData[i].id ) {
                                    this.proorgData[i].hehe = false;
                                    this.proorgData[i].name = node2.name;
                                    this.proorgData[i].type = node2.type;
                                    this.proorgData[i].orgstatus = node2.status;
                                    this.proorgData[i].address = node2.address;
                                    this.proorgData[i].contact_phone = node2.contact_phone;
                                    this.proorgData[i].contact_mobile = node2.contact_mobile;
                                    this.proorgData[i].contact = node2.contact;
                                    this.proorgData[i].email = node2.email;
                                    this.proorgData[i].orgcreated = node2.created;
                                    for (var j=0;j<codeList.orgStatus.length;j++){
                                        if (node2.status == codeList.orgStatus[j].value){
                                            this.proorgData[i].orgstatusname = codeList.orgStatus[j].label;
                                            return;
                                        }
                                    }
                                }
                            });
                        }
                    }
                    pageorgs = this.proorgData;
                    this.proorgDataFromDB = this.proorgData;
                    this.proorgData = pageorgs.slice((pageNo - 1) * this.proorgPageSize, pageNo * this.proorgPageSize);
                }
            })
            .catch(function (err) {
                this.$message({
                    message: err,
                    type: 'error'
                });
            });
        },

        //按会员名称或类型查询会员信息
        associatorHanldeByNameType(pageNo){
            url = '/api/v1/admin/orgs/query';
            i=0;
                if (this.orgName) {
                   url = url+'?name='+this.orgName;
                   i = i+1;
                }
                if (this.orgType){
                   if (i>0){
                      url = url+'&type='+this.orgType;
                   }else{
                      url = url+'?type='+this.orgType;
                   }
                }
            this.myGet(url)
            .then(function (res) {
                var orgs,
                orgs = res.data;
                for (var j=0; j<this.proorgData.length;j++){
                    for (var k =0 ;k < orgs.length;k++){
                        if (this.proorgData[j].id == orgs[k].id && this.proorgData[j].status == 0){
                           orgs.splice(k,1);
                        }
                    }
                }
                this.orgDataFromDB = orgs;
                this.orgData = orgs.slice((pageNo - 1) * this.orgPageSize, pageNo * this.orgPageSize);
            })
            .catch(function (err) {
                this.$message({
                    message: err,
                    type: 'error'
                });
            });

        },

        //check选择记录
        handleSelectionChange(val) {
             this.orgmultipleSelect = val;
        },

        //check选择记录
        handleSelectmemberChange(val) {
             this.usermultipleSelect = val;
        },

        promembertoggleSelection(rows) {
           if (rows) {
              rows.forEach(row => {
                 this.$refs.prouserTable.toggleRowSelection(row);
              });
           } else {
                 this.$refs.prouserTable.clearSelection();
           }
        },

        handleMemDelSelectionChange(val) {
             this.prousermultipleSelect = val;
        },

        //点击添加会员按钮
        associatorHanldeAdd(){
            this.dialogVisible1 = true;
            this.orgTypes = codeList.orgTypes;
            this.associatorHanldeByNameType(this.orgCurrentPageNo);
        },

        associatorHandleGoBack(){
            this.orgName='';
            this.orgType='';
            this.dialogVisible1 = false;
        },


        //单击会员确定按钮
        addOrgToProject(){
           i = 0;
           var orgIds = '';
           for ( i = 0; i< this.orgmultipleSelect.length;i++){
               if (i == 0) {
                  orgIds =  this.orgmultipleSelect[i].id;
               } else{
                  orgIds = orgIds+','+this.orgmultipleSelect[i].id;
               }
           }
           if (i > 0){
               this.myPost('/api/v1/admin/projects/'+id+'/associators',{
                   associatorIds: orgIds,
               })
               .then(function (res) {
                    this.$message({
                        message: '加入项目成功',
                        type: 'success'
                    });
                    this.dialogVisible1 = false;
                    this.getProOrgId(id, this.proorgPageNo);
               })
               .catch(function (err) {
                    this.$message({
                        message: err.error,
                        type: 'error'
                    });
               });
           }
        },

        //单击增加成员进行成员查询
        memberHandleByOrgId (org) {
            var tempdata, users;
            this.dialogVisible = true;
            this.orgCurrent = org.name + " - 用户列表";
            this.myGet('/api/v1/admin/projects/browse/membersinproject/' + id + '?orgid=' + org.id)
            .then(function (res) {
                if (res.data) {
                    tempdata = res.data['orgusers'];
                    users = res.data['ProUsers'].filter(node => node.status == 0);
                    let haha = []; // 未选中人员
                    let hehe = []; // 已选中人员
                    for (var i = tempdata.length - 1; i >= 0; i--) {
                        users.forEach((node2) => {
                            if (node2.id == tempdata[i].id) {
                                hehe.push(tempdata[i].id);
                                return;
                            }
                        });
                        haha.push({ key: tempdata[i].id, label: tempdata[i].personname, disabled: false });
                    }
                    this.userDataFromDB = haha;
                    this.userDataSelect = hehe;
                    this.oldUserData = hehe;
                }
            })
            .catch(function (err) {
                this.$message({
                    message: err.error,
                    type: 'error'
                });
            });
        },

        // 添加项目组成员
        addUserToProject (users, arrow) {
            this.myPost('/api/v1/admin/projects/add/user/' + id, {
                oldUserId: this.oldUserData,
                newUserId: users,
            })
            .then(function (res) {
                let msg = arrow == 'right' ? '成员加入项目成功' : '成员移除项目成功';
                this.$message({
                    message: msg,
                    type: 'success'
                });
             })
            .catch(function (err) {
                this.$message({
                    message: err.error,
                    type: 'error'
                });
            });
        },

        //点击会员退出项目按钮
        associatorHandledelete(){
            let ids = this.heheSelect.filter(node => node).join(",");
            if (ids.length === 0){
                this.$message({message: '请先选中一个会员，再进行删除！', type: 'error'});
                return;
            }
            this.$confirm('此操作将该会员从项目中退出, 是否继续?', '提示', {
                confirmButtonText: '确定',
                cancelButtonText: '取消',
                type: 'warning'
            }).then(() => {
                url = '/api/v1/admin/projects/delete/'+id+'/associators';
                this.myPost(url, {
                    associatorIds: ids,
                })
                .then(function (res) {
                    this.$message({
                        message: '退出项目成功',
                        type: 'success'
                    });
                    this.getProOrgId(id, this.proorgPageNo);
                })
                .catch(function (err) {
                    this.$message({
                        message: err.error,
                        type: 'error'
                    });
                });
            })
            .catch(() => {
                this.$message({
                    type: 'info',
                    message: '已取消退出项目'
                });
            });
        },

        proUserHandleGoBack(){
            this.prousermultipleSelect='';
            this.dialogVisible2 = false;
        },

        handleProOrgPageChange (pageNo) {
            this.proorgCurrentPageNo = pageNo;
            this.getProOrgId(id,pageNo);
        },

        handleProOrgPageSizeChange(psize){
        	this.proorgPageSize = psize;
            this.getProOrgId(id,this.proorgCurrentPageNo);
        },

        //会员分页
        handleOrgPageChange (pageNo) {
            this.orgCurrentPageNo = pageNo;
            this.associatorHanldeByNameType(pageNo);
        },

        handleOrgPageSizeChange(psize){
            this.orgPageSize = psize;
            this.associatorHanldeByNameType(this.orgCurrentPageNo);
        },

        // 选择全部项目
        checkAll (item) {
            this.heheSelect = [];
            if (item) {
                this.proorgData.forEach(node => {
                    this.heheSelect.push(node.id);
                    node.hehe = true;
                });
            } else {
                this.proorgData.forEach(node => {
                    this.heheSelect.push(false);
                    node.hehe = false;
                });
            }
        },

    },

    filters: {
        changeOrgType (value) {
            let type = codeList.orgTypes.filter(item => item.value == value );
            if (type.length > 0) return type[0].label;
            return "未知类型";
        },

        associatorStatus (value) {
            let status = codeList.associatorStatus.filter(item => item.value == value );
            if (status.length > 0) return status[0].label;
            return "未知类型";
        },

        changeProjectType (value) {
            let type = codeList.projectTypes.filter(item => item.value == value );
            if (type.length > 0) return type[0].label;
            return "未知类型";
        },

        changeProjectStatus (value) {
            let status = codeList.projectStatus.filter(item => item.value == value );
            if (status.length > 0) return status[0].label;
            return "未知类型";
        },
    },

    watch: {
        heheSelect (newArgs, oldArgs) {
            let csl = newArgs.filter(node => node).length;
            let tdl = this.proorgData.length;
            if (csl === tdl) {
                this.allCheck = true;
            } else {
                this.allCheck = false;
            }
        }
    }
}

var Ctor = Vue.extend(Main);
new Ctor().$mount('#app');
