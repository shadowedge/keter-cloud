var Main = {
    data () {
        return {
            // 项目信息
            project: {
                id: '',
                name: '',
                type: '',
                contact_id: '',
                rcontext: '',
                status: '',
                memo: '',
            },
            prousermultipleSelect: [],

            checkList: ['项目成员','项目负责人'],

            prouserDataFromDB: [],
            prouserData: [],
            prouserCurrentPageNo: 1,
            prouserPageNo: 1,
            prouserPageSize: 10,
        };
    },

    created () {
        this.getProject();
        this.memberHandleInProjectByOrgId(this.prouserPageNo);
    },

    methods: {

        // 获取项目
        getProject () {
            if (id == '') return;
            return this.myGet('/api/v1/admin/projects/' + id)
            .then(function (res) {
                let project = res.data;
                Object.assign(this.project, project);
            })
            .catch(function (err) {
               this.$message({
                 message: err.error,
                 type: 'error'
               });
            });
        },

        handleMemDelSelectionChange(val) {
            this.prousermultipleSelect = val;
        },

        //点击项目内成员
        memberHandleInProjectByOrgId (pageNo) {
            var tempdata,users,pageusers;
            this.myGet('/api/v1/admin/projects/browse/membersinproject/'+id)
            .then(function (res) {
                if (res.data){
                    tempdata = res.data['ProUsers'];
                    users = res.data['orgusers'];
                    for (var i = tempdata.length - 1; i >= 0; i--) {
                        if (tempdata[i].status != 0) {
                           tempdata.splice(i, 1);
                        } else {
                            users.forEach((node2) => {
                                if (node2.id == tempdata[i].id){
                                    tempdata[i].username = node2.username;
                                    tempdata[i].name = node2.personname;
                                    tempdata[i].email = node2.email;
                                    tempdata[i].mobile = node2.mobile;
                                    tempdata[i].orgname = node2.orgname;
                                    return;
                                }
                            });
                        }
                    }
                    this.prouserData = tempdata;
                    this.prouserDataFromDB = tempdata;
                    this.prouserData = tempdata.slice((pageNo - 1) * this.prouserPageSize, pageNo * this.prouserPageSize);
                }
            })
            .catch(function (err) {
                this.$message({
                    message: err.error,
                    type: 'error'
                });
            });
        },

        //单击成员员退出项目按钮
        memberHandleDelete(){
            var memberid='';
            if (this.prousermultipleSelect.length > 0){
                for ( i = 0; i < this.prousermultipleSelect.length; i++){
                    if (i == 0) {
                        memberid =  this.prousermultipleSelect[i].id;
                    } else {
                        memberid = memberid+','+this.prousermultipleSelect[i].id;
                    }
                }
            } else {
                this.$message({
                    message: '请选择成员进行删除!',
                    type: 'error'
                });
                return;
            }
            this.$confirm('此操作将所选成员从项目中删除, 是否继续?', '提示', {
                confirmButtonText: '确定',
                cancelButtonText: '取消',
                type: 'warning'
            })
            .then(() => {
                url = '/api/v1/admin/projects/delete/'+id+'/members';
                this.myPost(url, {
                    memberIds: memberid,
                })
                .then(function (res) {
                    this.$message({
                        message: '退出项目成功',
                        type: 'success'
                    });
                    this.memberHandleInProjectByOrgId(1);
                })
                .catch(function (err) {
                    this.$message({
                        message: err.error,
                        type: 'error'
                    });
                });
            })
            .catch(() => {
                this.$message({
                    type: 'info',
                    message: '已取消退出项目'
                });
            });
        },

        // 设置管理员
        setManager (user, type) {
            this.myPatch('/api/v1/admin/projects/set/member/type', {
                projectId: id,
                userId: user.id,
                memberType: type
            })
            .then(function (res) {
                this.$message({
                    message: '设置成功',
                    type: 'success'
                });
                this.memberHandleInProjectByOrgId(this.prouserCurrentPageNo);
            })
            .catch(function (err) {
                this.$message({
                    message: err.error,
                    type: 'error'
                });
            });
        },

        // 用户分页点击事件
        handleProUserPageChange (pageNo) {
            this.prouserCurrentPageNo = pageNo;
            this.memberHandleInProjectByOrgId(pageNo);
        },

        handleProUserPageSizeChange(psize){
            this.prouserPageSize = psize;
            this.memberHandleInProjectByOrgId(this.prouserCurrentPageNo);
        },
    },

    filters: {
        memberType (value) {
            let membertype = codeList.memberType.filter(item => item.value == value );
            if (membertype.length > 0) return membertype[0].label;
            return "未知类型";
        },
    }
}

var Ctor = Vue.extend(Main);
new Ctor().$mount('#app');
