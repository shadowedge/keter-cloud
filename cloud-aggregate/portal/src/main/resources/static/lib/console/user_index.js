var Main = {
    data () {
        return {
            userForm: {
                username: '',
                name: '',
                role: [],
                roleName: [],
                mobile: '',
                email: '',
                remark: '',
            },

            users: [],
            roles: [],
            usersFromDB: [],
            usersDel: [],
            pageNo: 1,
            pageSize: 10,
            isUpdate: false,

            userRules: {
                username: [
                    { required: true, message: '不能为空', trigger: 'blur' },
                ],
                name: [
                    { required: true, message: '不能为空', trigger: 'change' },
                ],
                role: [
                    { required: true, message: '请至少少选择一个角色', trigger: 'change' },
                ],
                email: [
                    { required: true, message: '不能为空', trigger: 'blur' },
                    { type: 'email', message: '请输入正确的邮箱地址', trigger: ['blur', 'change'] }
                ],
            },

            roles: [],

            dialogVisible: false,

        };
    },

    created () {
        this.getUsers(this.pageNo, this.pageSize);
        this.getRoles();
    },

    methods: {
        // 获取用户信息
        getUsers (pageNo, pageSize) {
            this.myGet('/api/v1/admin/users')
            .then(function (res) {
                this.usersFromDB = res.data;
                this.users = res.data.slice((pageNo - 1) * pageSize, pageNo * pageSize);
            })
            .catch(function (err) {
                this.$message({
                    message: err.error,
                    type: 'error'
                });
            });
        },

        // 获取角色
        getRoles () {
            this.myGet('/api/v1/admin/users/roles')
            .then(function (res) {
                this.roles = res.data;
            })
            .catch(function (err) {
                this.$message({
                    message: err.error,
                    type: 'error'
                });
            });
        },

        openUserBox () {
            this.dialogVisible = true;
            this.isUpdate = false;
            this.resetForm('userForm');
        },

        // 打开修改用户页面
        userHandleEdit (row) {
            this.dialogVisible = true;
            this.isUpdate = true;

            let _row = clone(row);
            for (let p in _row) {
                if (p != 'role') this.userForm[p] = _row[p];
            }

            // 获取用户角色
            this.myGet('/api/v1/admin/users/' + row.id  + '/roles')
            .then(function (res) {
                this.userForm.role = res.data.map(node => node.id);
            })
            .catch(function (err) {
                this.$message({
                    message: err.error,
                    type: 'error'
                });
            });
        },

        saveUser () {
            let url = ''
                , method = null;
            if (this.isUpdate == false) {
                url = '/api/v1/admin/users';
                method = this.myPost;
            } else {
                let userId = this.userForm.id;
                url = '/api/v1/admin/users/' + userId;
                method = this.myPatch;
            }

            let param = {
                name: this.userForm.name,
                username: this.userForm.username,
                email: this.userForm.email,
                mobile: this.userForm.mobile,
                role_ids: this.userForm.role.join(','),
                remark: this.userForm.remark,
            };

            this.$refs.userForm.validate((valid) => {
                if (valid) {
                    method.call(this, url, param)
                    .then(function (res) {
                        this.dialogVisible = false;
                        this.$refs.userForm.resetFields();
                        this.getUsers(this.pageNo, this.pageSize);
                        this.$message({
                            message: '保存成功',
                            type: 'success'
                        });
                    })
                    .catch(function (err) {
                        this.$message({
                            message: err.error,
                            type: 'error'
                        });
                    });
                } else {
                  return false;
                }
            });
        },

        // 删除用户
        delUser () {
            if (this.usersDel.length == 0) {
                this.$message({
                    type: 'info',
                    message: '请选择删除的用户!'
                });
                return;
            }
            this.$confirm('此操作将永久删除所选用户, 是否继续?', '提示', {
                confirmButtonText: '确定',
                cancelButtonText: '取消',
                type: 'warning'
            }).then(() => {
                this.$message({
                    type: 'success',
                    message: '暂未开通删除功能!'
                });
            }).catch(() => {
              this.$message({
                  type: 'info',
                  message: '已取消删除'
              });
            });
        },

        // 显示角色
        showRole (id) {
            // 获取用户角色
            this.myGet('/api/v1/admin/users/' + id  + '/roles')
            .then(function (res) {
                this.userForm.roleName = res.data.map(node => node.label).join(',');
            })
            .catch(function (err) {
                this.$message({
                    message: err.error,
                    type: 'error'
                });
            });
        },

        // 重置对话框
        resetForm (formName) {
            for (var p in this[formName]) {
                this[formName][p] = '';
            }
            this.userForm.role = [];
        },

        sizeChange (size) {
            this.pageSize = size;
            this.getUsers(this.pageNo, this.pageSize);
        },

        pageChange (pageNo) {
            this.pageNo = pageNo;
            this.getUsers(this.pageNo, this.pageSize);
        },

        // 选择用户
        selectionChange (val) {
            this.usersDel = val;
        },
    },

    filters: {
    }
}

var Ctor = Vue.extend(Main);
var haha = new Ctor().$mount('#app');
