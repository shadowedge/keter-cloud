var Main = {
    data () {
        return {
            dialogVisible: false,

            deviceForm: {
                id: '',
                status: '',
                apply_userid: '',
                apply_start: '',
                apply_end: '',
                apply_remark: '',
                access_address: '',
                access_info: '',
                remark: ''
            },

            deviceStatus : [],

            deviceRules: {
                access_address: [
                    { required: false, message: '访问地址不能为空', trigger: 'blur' },
                    { min: 1, max: 64, message: '长度在 1 到 64 个字符', trigger: 'blur' }
                ],
                access_info: [
                    { required: false, message: '访问信息不能为空', trigger: 'blur' },
                    { min: 1, max: 64, message: '长度在 1 到 64 个字符', trigger: 'blur' }
                ],
                remark: [
                    { required: true, message: '备注不能为空', trigger: 'blur' },
                    { min: 1, max: 1024, message: '长度在 1 到 1024 个字符', trigger: 'blur' }
                ]
            },

            deviceCurrent: null,
            deviceCurrentPageNo: 1,
            deviceData: [],
            deviceDataFromDB: [],
            devicePageSize: 10,
        };
    },

    created () {
        let instance = this;
        this.deviceStatus = codeList.deviceStatus;
        this.getVDI(this.deviceCurrentPageNo, this.devicePageSize, function (devices) {
            instance.$refs.deviceTable.setCurrentRow(devices[0]);
        })
    },

    methods: {

        // 获取虚拟桌面列表
        getVDI (pageNo, pageSize, callback) {
            this.myGet('/api/v1/admin/devices/vdi')
            .then(function (res) {
                let mixinData = myMixinData(res.data, [{ key: 'apply_userid', key2: 'id', srcName: 'name', targetName: 'apply_user_name' }])
                this.deviceDataFromDB = mixinData;
                this.deviceData = mixinData.slice((pageNo - 1) * pageSize, pageNo * pageSize);
                if (callback && this.deviceDataFromDB.length > 0) callback(this.deviceDataFromDB);
            })
            .catch(function (err) {
                this.$message({
                    message: err,
                    type: 'error'
                });
            });
        },

        // 虚拟桌面分页
        handleDevicePageChange (pageNo) {
            this.deviceCurrentPageNo = pageNo;
            this.getVDI(pageNo, this.devicePageSize);
        },

        // 同意分配虚拟桌面
        approveDevicePass (formName) {
            let url = '';

            this.$refs[formName].validate((valid) => {
                if (valid) {
                    let param = {}
                    Object.assign(param, this.deviceForm);

                    this.myPost('/api/v1/admin/devices/vdi/approve', param)
                    .then(function (res) {
                        this.deviceCurrentPageNo = 1;
                        this.getVDI(this.deviceCurrentPageNo, this.devicePageSize);
                        this.dialogVisible = false;
                        this.$refs['deviceForm'].resetFields();
                        this.$message({
                            message: '保存成功',
                            type: 'success'
                        });
                    })
                    .catch(function (err) {
                        this.$message({
                            message: err,
                            type: 'error'
                        });
                    });
                } else {
                    return false;
                }
            });
        },

        // 拒绝分配虚拟桌面
        approveDeviceNotPass (formName) {
            let url = '';

            this.$refs[formName].validate((valid) => {
                if (valid) {
                    let param = {}
                    Object.assign(param, this.deviceForm);

                    this.myPatch('/api/v1/admin/devices/vdi/approve', param)
                    .then(function (res) {
                        this.deviceCurrentPageNo = 1;
                        this.getVDI(this.deviceCurrentPageNo, this.devicePageSize);
                        this.dialogVisible = false;
                        this.$refs['deviceForm'].resetFields();
                        this.$message({
                            message: '保存成功',
                            type: 'success'
                        });
                    })
                    .catch(function (err) {
                        this.$message({
                            message: err,
                            type: 'error'
                        });
                    });
                } else {
                  return false;
                }
            });
        },

        // 编辑虚拟桌面
        deviceHandleEdit (index, row) {
            let currentIndex = index + (this.deviceCurrentPageNo - 1) * this.devicePageSize;
            this.deviceForm = clone(this.deviceDataFromDB[currentIndex]);
            this.dialogVisible = true;
        },

        // 重置对话框
        resetForm (formName) {
            for (var p in this[formName]) {
                this[formName][p] = '';
            }
        },

        // 表点击事件
        deviceHandleTableChange (val) {
            if (val) {
                this.deviceCurrent = val;
            }
        }
    },

    filters: {
        changeDeviceStatus (value) {
            let type = codeList.deviceStatus.filter(item => item.value == value );
            if (type.length > 0) return type[0].label;
            return "未知类型";
        }
    }
}

var Ctor = Vue.extend(Main);
var haha = new Ctor().$mount('#app');