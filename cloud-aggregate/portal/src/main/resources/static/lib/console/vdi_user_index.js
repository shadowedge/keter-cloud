var Main = {
    data () {
        return {
            dialogVisible1: false,
            dialogVisible2: false,

            isUpdate: false,

            deviceForm: {
                id: '',
                status: '',
                apply_userid: '',
                apply_start: '',
                apply_end: '',
                apply_remark: '',
                access_address: '',
                access_info: '',
                remark: ''
            },

            deviceStatus : [],

            deviceRules: {
                apply_remark: [
                    { required: true, message: '申请备注不能为空', trigger: 'blur' },
                    { min: 1, max: 1024, message: '长度在 1 到 1024 个字符', trigger: 'blur' }
                ]
            },

            deviceCurrent: null,
            deviceCurrentPageNo: 1,
            deviceData: [],
            deviceDataFromDB: [],
            devicePageSize: 10,
            deviceDate: [], // 设备使用时间
        };
    },

    created () {
        let instance = this;
        this.deviceStatus = codeList.deviceStatus;
        this.getVDI(this.deviceCurrentPageNo, this.devicePageSize, function (devices) {
            instance.$refs.deviceTable.setCurrentRow(devices[0]);
        })
    },

    methods: {

        // 获取虚拟桌面列表
        getVDI (pageNo, pageSize, callback) {
            this.myGet('/api/v1/user/devices/vdi/user')
            .then(function (res) {
                var rs = res.data.map(function (node) {
                    node.apply_user_name = Carevault.getItem('name');
                    return node;
                });
                this.deviceDataFromDB = rs;
                this.deviceData = rs.slice((pageNo - 1) * pageSize, pageNo * pageSize);
                if (callback && this.deviceDataFromDB.length > 0) callback(this.deviceDataFromDB);
            })
            .catch(function (err) {
                this.$message({
                    message: err,
                    type: 'error'
                });
            });
        },

        // 虚拟桌面分页
        handleDevicePageChange (pageNo) {
            this.deviceCurrentPageNo = pageNo;
            this.getVDI(pageNo, this.devicePageSize);
        },

        // 打开对话框
        openDeviceBox () {
            this.dialogVisible1 = true;
            this.isUpdate = false;
            this.resetForm('deviceForm');
            this.deviceDate = [];
        },

        // 保存虚拟桌面
        saveDevice (formName) {
            let url = '';

            this.$refs[formName].validate((valid) => {
                if (valid) {

                    if(!(this.deviceForm.apply_start > 0 && this.deviceForm.apply_end > 0)){
                        this.$message({
                            message: '请填写使用时间'
                        })
                        return;
                    }

                    let param = {}
                    Object.assign(param, this.deviceForm);

                    if (!this.isUpdate) {// 新增
                        this.myPost('/api/v1/user/devices/vdi', param)
                        .then(function (res) {
                            this.deviceCurrentPageNo = 1;
                            this.getVDI(this.deviceCurrentPageNo, this.devicePageSize);
                            this.dialogVisible1 = false;
                            this.$refs['deviceForm'].resetFields();
                            this.$message({
                                message: '保存成功',
                                type: 'success'
                            });
                        })
                        .catch(function (err) {
                            this.$message({
                                message: err,
                                type: 'error'
                            });
                        });
                    } else {// 修改
                        this.myPatch('/api/v1/user/devices/vdi', param)
                        .then(function (res) {
                            this.deviceCurrentPageNo = 1;
                            this.getVDI(this.deviceCurrentPageNo, this.devicePageSize);
                            this.dialogVisible1 = false;

                            this.$refs['deviceForm'].resetFields();
                            this.$message({
                                message: '保存成功',
                                type: 'success'
                            });
                        })
                        .catch(function (err) {
                            this.$message({
                                message: err,
                                type: 'error'
                            });
                        });
                    }
                } else {
                  return false;
                }
            });
        },

        // 编辑虚拟桌面
        deviceHandleEdit (index, row) {
            let currentIndex = index + (this.deviceCurrentPageNo - 1) * this.devicePageSize;
            this.deviceForm = clone(this.deviceDataFromDB[currentIndex]);

            this.deviceDate = [new Date(this.deviceForm.apply_start), new Date(this.deviceForm.apply_end)];
            this.deviceForm.apply_start = Date.parse(this.deviceForm.apply_start);
            this.deviceForm.apply_end = Date.parse(this.deviceForm.apply_end);

            this.isUpdate = true;
            this.dialogVisible1 = true;
        },

        // 查看虚拟桌面
        deviceHandleView (index, row) {
            let currentIndex = index + (this.deviceCurrentPageNo - 1) * this.devicePageSize;
            this.deviceForm = clone(this.deviceDataFromDB[currentIndex]);

            this.dialogVisible2 = true;
        },

        // 重置对话框
        resetForm (formName) {
            for (var p in this[formName]) {
                this[formName][p] = '';
            }
        },

        // 表点击事件
        deviceHandleTableChange (val) {
            if (val) {
                this.deviceCurrent = val;
            }
        },

        setTime (value) {
            if(value){
                this.deviceForm.apply_start = new Date(value[0]).getTime();
                this.deviceForm.apply_end = new Date(value[1]).getTime();
            }else{
                this.deviceForm.apply_start = '';
                this.deviceForm.apply_end = '';
            }
        }
    },

    filters: {
        changeDeviceStatus (value) {
            let type = codeList.deviceStatus.filter(item => item.value == value );
            if (type.length > 0) return type[0].label;
            return "未知类型";
        }
    }
}

var Ctor = Vue.extend(Main);
var haha = new Ctor().$mount('#app');