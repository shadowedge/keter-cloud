var Main = {
    data () {
        return {
            dialogVisible: false,

            deviceForm: {
                id: '',
                status: '',
                apply_userid: '',
                apply_start: '',
                apply_end: '',
                cpu: '',
                gpu: '',
                mem: '',
                disk: '',
                os: '',
                framework: '',
                apply_remark: '',
                access_address: '',
                access_info: '',
                remark: ''
            },

            deviceStatus : [],

            deviceRules: {
                access_address: [
                    { required: false, message: '访问地址不能为空', trigger: 'blur' },
                    { min: 1, max: 64, message: '长度在 1 到 64 个字符', trigger: 'blur' }
                ],
                access_info: [
                    { required: false, message: '访问信息不能为空', trigger: 'blur' },
                    { min: 1, max: 64, message: '长度在 1 到 64 个字符', trigger: 'blur' }
                ],
                remark: [
                    { required: true, message: '备注不能为空', trigger: 'blur' },
                    { min: 1, max: 1024, message: '长度在 1 到 1024 个字符', trigger: 'blur' }
                ]
            },

            deviceCurrent: null,
            deviceCurrentPageNo: 1,
            deviceData: [],
            deviceDataFromDB: [],
            devicePageSize: 10,
        };
    },

    created () {
        let instance = this;
        this.deviceStatus = codeList.deviceStatus;
        this.getVM(this.deviceCurrentPageNo, this.devicePageSize, function (devices) {
            instance.$refs.deviceTable.setCurrentRow(devices[0]);
        })
    },

    methods: {

        // 获取虚拟主机列表
        getVM (pageNo, pageSize, callback) {
            this.myGet('/api/v1/admin/devices/vm')
            .then(function (res) {
                let mixinData = res.data.device.map(function (currentValue) {
                    res.data.user.forEach(function (data) {
                        let keyValue = currentValue["apply_userid"];
                        let key2Value = data["id"];
                        if (keyValue == key2Value) {
                            currentValue["apply_user_name"] = data["name"];
                            return;
                        }
                    });
                    return currentValue;
                });

                let mixinData1 = mixinData.map(function (currentValue) {
                    res.data.project.forEach(function (data) {
                        let keyValue = currentValue["project_id"];
                        let key2Value = data["id"];
                        if (keyValue == key2Value) {
                            currentValue["project_name"] = data["name"];
                            return;
                        }
                    });
                    return currentValue;
                });

                this.deviceDataFromDB = mixinData1;
                this.deviceData = mixinData1.slice((pageNo - 1) * pageSize, pageNo * pageSize);
                if (callback && this.deviceDataFromDB.length > 0) callback(this.deviceDataFromDB);
            })
            .catch(function (err) {
                this.$message({
                    message: err,
                    type: 'error'
                });
            });
        },

        // 虚拟主机分页
        handleDevicePageChange (pageNo) {
            this.deviceCurrentPageNo = pageNo;
            this.getVM(pageNo, this.devicePageSize);
        },

        // 同意分配虚拟主机
        approveDevicePass (formName) {
            let url = '';

            this.$refs[formName].validate((valid) => {
                if (valid) {
                    let param = {}
                    Object.assign(param, this.deviceForm);

                    this.myPost('/api/v1/admin/devices/vm/approve', param)
                    .then(function (res) {
                        this.deviceCurrentPageNo = 1;
                        this.getVM(this.deviceCurrentPageNo, this.devicePageSize);
                        this.dialogVisible = false;
                        this.$refs['deviceForm'].resetFields();
                        this.$message({
                            message: '保存成功',
                            type: 'success'
                        });
                    })
                    .catch(function (err) {
                        this.$message({
                            message: err,
                            type: 'error'
                        });
                    });
                } else {
                    return false;
                }
            });
        },

        // 拒绝分配虚拟主机
        approveDeviceNotPass (formName) {
            let url = '';

            this.$refs[formName].validate((valid) => {
                if (valid) {
                    let param = {}
                    Object.assign(param, this.deviceForm);

                    this.myPatch('/api/v1/admin/devices/vm/approve', param)
                    .then(function (res) {
                        this.deviceCurrentPageNo = 1;
                        this.getVM(this.deviceCurrentPageNo, this.devicePageSize);
                        this.dialogVisible = false;
                        this.$refs['deviceForm'].resetFields();
                        this.$message({
                            message: '保存成功',
                            type: 'success'
                        });
                    })
                    .catch(function (err) {
                        this.$message({
                            message: err,
                            type: 'error'
                        });
                    });
                } else {
                  return false;
                }
            });
        },

        // 编辑虚拟主机
        deviceHandleEdit (index, row) {
            let currentIndex = index + (this.deviceCurrentPageNo - 1) * this.devicePageSize;
            this.deviceForm = clone(this.deviceDataFromDB[currentIndex]);
            this.dialogVisible = true;
        },

        // 重置对话框
        resetForm (formName) {
            for (var p in this[formName]) {
                this[formName][p] = '';
            }
        },

        // 表点击事件
        deviceHandleTableChange (val) {
            if (val) {
                this.deviceCurrent = val;
            }
        }
    },

    filters: {
        changeDeviceStatus (value) {
            let type = codeList.deviceStatus.filter(item => item.value == value );
            if (type.length > 0) return type[0].label;
            return "未知类型";
        },

        changeCPUType (value) {
            let type = codeList.cpuType.filter(item => item.value == value );
            if (type.length > 0) return type[0].label;
            return "未知类型";
        },

        changeGPUType (value) {
            let type = codeList.gpuType.filter(item => item.value == value );
            if (type.length > 0) return type[0].label;
            return "未知类型";
        },

        changeMEMType (value) {
            let type = codeList.memType.filter(item => item.value == value );
            if (type.length > 0) return type[0].label;
            return "未知类型";
        },

        changeDISKType (value) {
            let type = codeList.diskType.filter(item => item.value == value );
            if (type.length > 0) return type[0].label;
            return "未知类型";
        },

        changeOSType (value) {
            let type = codeList.osType.filter(item => item.value == value );
            if (type.length > 0) return type[0].label;
            return "未知类型";
        },

        changeFrameworkType (value) {
            let type = codeList.frameworkType.filter(item => item.value == value );
            if (type.length > 0) return type[0].label;
            return "未知类型";
        },
    }
}

var Ctor = Vue.extend(Main);
var haha = new Ctor().$mount('#app');