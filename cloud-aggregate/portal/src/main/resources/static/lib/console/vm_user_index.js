var Main = {
    data () {
        return {
            dialogVisible1: false,
            dialogVisible2: false,

            isUpdate: false,

            flag: true,

            deviceForm: {
                id: '',
                status: '',
                apply_userid: '',
                apply_start: '',
                apply_end: '',
                cpu: '',
                gpu: '',
                mem: '',
                disk: '',
                os: '',
                framework: '',
                apply_remark: '',
                access_address: '',
                access_info: '',
                remark: '',
                project_id: ''
            },

            cpuType : codeList.cpuType,
            gpuType : codeList.gpuType,
            memType : codeList.memType,
            diskType : codeList.diskType,
            osType : codeList.osType,
            frameworkType : codeList.frameworkType,

            projects:[],

            deviceStatus : [],

            deviceRules: {
                project_id: [
                    { required: true, message: '相关项目不能为空', trigger: 'change' },
                ],
                cpu: [
                    { required: true, message: 'CPU核数不能为空', trigger: 'change' },
                ],
                gpu: [
                    { required: true, message: 'GPU数量不能为空', trigger: 'change' },
                ],
                mem: [
                    { required: true, message: '内存容量GB不能为空', trigger: 'change' },
                ],
                disk: [
                    { required: true, message: '数据磁盘容量GB不能为空', trigger: 'change' },
                ],
                os: [
                    { required: true, message: '操作系统不能为空', trigger: 'change' },
                ],
                framework: [
                    { required: true, message: '深度学习框架不能为空', trigger: 'change' },
                ],
                apply_remark: [
                    { required: true, message: '申请备注不能为空', trigger: 'blur' },
                    { min: 1, max: 1024, message: '长度在 1 到 1024 个字符', trigger: 'blur' }
                ]
            },

            deviceCurrent: null,
            deviceCurrentPageNo: 1,
            deviceData: [],
            deviceDataFromDB: [],
            devicePageSize: 10,
            deviceDate: [], // 设备使用时间
        };
    },

    created () {
        let instance = this;
        this.deviceStatus = codeList.deviceStatus;

        this.myGet('/api/v1/user/projects/query/member/currentUserProjects')
        .then(function (res) {
            res.data.forEach((node) => {
                this.projects.push({value: node.id,label: node.name})
            });

            //console.info(this.projects)
        })
        .catch(function (err) {
            this.$message({
                message: err,
                type: 'error'
            });
        });


        this.getVM(this.deviceCurrentPageNo, this.devicePageSize, function (devices) {
            instance.$refs.deviceTable.setCurrentRow(devices[0]);
        })
    },

    methods: {

        // 获取虚拟主机列表
        getVM (pageNo, pageSize, callback) {
            this.myGet('/api/v1/user/devices/vm/user')
            .then(function (res) {
                let mixinData = myMixinData(res.data, [{ key: 'project_id', key2: 'id', srcName: 'name', targetName: 'project_name' }],1);
                var rs = mixinData.map(function (node) {
                    node.apply_user_name = Carevault.getItem('name');
                    return node;
                });
                this.deviceDataFromDB = rs;
                this.deviceData = rs.slice((pageNo - 1) * pageSize, pageNo * pageSize);
                if (callback && this.deviceDataFromDB.length > 0) callback(this.deviceDataFromDB);
            })
            .catch(function (err) {
                this.$message({
                    message: err,
                    type: 'error'
                });
            });
        },

        // 虚拟主机分页
        handleDevicePageChange (pageNo) {
            this.deviceCurrentPageNo = pageNo;
            this.getVDI(pageNo, this.devicePageSize);
        },

        // 打开对话框
        openDeviceBox () {
            this.dialogVisible1 = true;
            this.isUpdate = false;
            this.resetForm('deviceForm');
            this.deviceDate = [];
            this.flag = true;
        },

        // 保存虚拟主机
        saveDevice (formName) {
            let url = '';

            this.$refs[formName].validate((valid) => {
                if (valid) {

                    if(!(this.deviceForm.apply_start > 0 && this.deviceForm.apply_end > 0)){
                        this.$message({
                            message: '请填写使用时间'
                        })
                        return;
                    }

                    let param = {}
                    Object.assign(param, this.deviceForm);

                    if (!this.isUpdate) {// 新增
                        this.myPost('/api/v1/user/devices/vm', param)
                        .then(function (res) {
                            this.deviceCurrentPageNo = 1;
                            this.getVM(this.deviceCurrentPageNo, this.devicePageSize);
                            this.dialogVisible1 = false;
                            this.$refs['deviceForm'].resetFields();
                            this.$message({
                                message: '保存成功',
                                type: 'success'
                            });
                        })
                        .catch(function (err) {
                            this.$message({
                                message: err,
                                type: 'error'
                            });
                        });
                    } else {// 修改
                        this.myPatch('/api/v1/user/devices/vm', param)
                        .then(function (res) {
                            this.deviceCurrentPageNo = 1;
                            this.getVM(this.deviceCurrentPageNo, this.devicePageSize);
                            this.dialogVisible1 = false;

                            this.$refs['deviceForm'].resetFields();
                            this.$message({
                                message: '保存成功',
                                type: 'success'
                            });
                        })
                        .catch(function (err) {
                            this.$message({
                                message: err,
                                type: 'error'
                            });
                        });
                    }
                } else {
                  return false;
                }
            });
        },

        // 保存虚拟主机和虚拟桌面
        saveVMAndVDI (formName) {
            let url = '';

            this.$refs[formName].validate((valid) => {
                if (valid) {

                    if(!(this.deviceForm.apply_start > 0 && this.deviceForm.apply_end > 0)){
                        this.$message({
                            message: '请填写使用时间'
                        })
                        return;
                    }

                    let param = {}
                    Object.assign(param, this.deviceForm);

                    if (!this.isUpdate) {// 新增
                        this.myPost('/api/v1/user/devices/vm', param)
                        .then(function (res) {
                            this.deviceCurrentPageNo = 1;
                            this.getVM(this.deviceCurrentPageNo, this.devicePageSize);
                            this.dialogVisible1 = false;
                            this.$refs['deviceForm'].resetFields();
                            this.$message({
                                message: '保存成功',
                                type: 'success'
                            });
                        })
                        .catch(function (err) {
                            this.$message({
                                message: err,
                                type: 'error'
                            });
                        });

                        this.myPost('/api/v1/user/devices/vdi', param)
                        .then(function (res) {
                            this.deviceCurrentPageNo = 1;
                            this.getVM(this.deviceCurrentPageNo, this.devicePageSize);
                            this.dialogVisible1 = false;
                            this.$refs['deviceForm'].resetFields();
                            this.$message({
                                message: '保存成功',
                                type: 'success'
                            });
                        })
                        .catch(function (err) {
                            this.$message({
                                message: err,
                                type: 'error'
                            });
                        });
                    }
                } else {
                  return false;
                }
            });
        },

        // 编辑虚拟主机
        deviceHandleEdit (index, row) {
            let currentIndex = index + (this.deviceCurrentPageNo - 1) * this.devicePageSize;
            this.deviceForm = clone(this.deviceDataFromDB[currentIndex]);

            this.deviceDate = [new Date(this.deviceForm.apply_start), new Date(this.deviceForm.apply_end)];
            this.deviceForm.apply_start = Date.parse(this.deviceForm.apply_start);
            this.deviceForm.apply_end = Date.parse(this.deviceForm.apply_end);

            this.isUpdate = true;
            this.dialogVisible1 = true;

            this.flag = false;
        },

        // 查看虚拟主机
        deviceHandleView (index, row) {
            let currentIndex = index + (this.deviceCurrentPageNo - 1) * this.devicePageSize;
            this.deviceForm = clone(this.deviceDataFromDB[currentIndex]);

            this.dialogVisible2 = true;
        },

        // 重置对话框
        resetForm (formName) {
            for (var p in this[formName]) {
                this[formName][p] = '';
            }
        },

        // 表点击事件
        deviceHandleTableChange (val) {
            if (val) {
                this.deviceCurrent = val;
            }
        },

        setTime (value) {
            if(value){
                this.deviceForm.apply_start = new Date(value[0]).getTime();
                this.deviceForm.apply_end = new Date(value[1]).getTime();
            }else{
                this.deviceForm.apply_start = '';
                this.deviceForm.apply_end = '';
            }
        }
    },

    filters: {
        changeDeviceStatus (value) {
            let type = codeList.deviceStatus.filter(item => item.value == value );
            if (type.length > 0) return type[0].label;
            return "未知类型";
        },

        changeCPUType (value) {
            let type = codeList.cpuType.filter(item => item.value == value );
            if (type.length > 0) return type[0].label;
            return "未知类型";
        },

        changeGPUType (value) {
            let type = codeList.gpuType.filter(item => item.value == value );
            if (type.length > 0) return type[0].label;
            return "未知类型";
        },

        changeMEMType (value) {
            let type = codeList.memType.filter(item => item.value == value );
            if (type.length > 0) return type[0].label;
            return "未知类型";
        },

        changeDISKType (value) {
            let type = codeList.diskType.filter(item => item.value == value );
            if (type.length > 0) return type[0].label;
            return "未知类型";
        },

        changeOSType (value) {
            let type = codeList.osType.filter(item => item.value == value );
            if (type.length > 0) return type[0].label;
            return "未知类型";
        },

        changeFrameworkType (value) {
            let type = codeList.frameworkType.filter(item => item.value == value );
            if (type.length > 0) return type[0].label;
            return "未知类型";
        },
    }
}

var Ctor = Vue.extend(Main);
var haha = new Ctor().$mount('#app');