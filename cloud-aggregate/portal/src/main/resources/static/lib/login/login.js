var userTable = new Vue({
    el: '#app',
    data: {
        username: '',   //账号
        password: '',
    },
    created: function () {
        sessionStorage.removeItem('name');
        sessionStorage.removeItem('roles');
    },
    methods: {
        login: function () {
            this.myPost('/authenticate', { username: this.username, password: this.password })
            .then(function (res) {
                let token = res.data;
                Carevault.setToken(token);

                let haha = Base64.decode(token);
                if (haha.includes('ROLE_ADMIN') && haha.includes('ROLE_USER')) {
                    window.location.href = this.$http.options.root + '/console/index';
                } else {
                    window.location.href = this.$http.options.root + '/console/user/index';
                }
            })
            .catch(function (err) {
                this.$message({
                    message: '用户名或密码错误，请重新输入',
                    type: 'error'
                });
            });
        },
    }
});