// 管理员菜单
var consoleData = [
    { 'index': '1', 'menuName': '首页', 'url': '/console/index', 'icon': 'el-icon-s-home' },
    {
        'index': '2', 'menuName': '会员管理', 'icon': 'el-icon-s-custom',
        'children': [
            { 'index': '21', 'menuName': '会员管理', 'url': '/console/organization' },
            { 'index': '22', 'menuName': '用户管理', 'url': '/console/user' },
        ]
    },
    {
        'index': '3', 'menuName': '项目管理', 'url': '/console/project', 'icon': 'el-icon-s-flag'
    },
    {
        'index': '5', 'menuName': '基础设施管理', 'icon': 'el-icon-s-platform',
        'children': [
            { 'index': '51', 'menuName': '虚拟主机', 'url': '/console/vm'},
            { 'index': '52', 'menuName': '虚拟桌面', 'url': '/console/vdi'},
            { 'index': '32', 'menuName': 'Docker', 'url': '#'},
        ]
    },
    {
        'index': '4', 'menuName': '数据资产管理', 'icon': 'el-icon-s-data',
        'children': [
            { 'index': '41', 'menuName': '数据资产查询', 'url': '/console/asset' },
            {
                'index': '42', 'menuName': '数据资产审批', 'url': '#',
                'children': [
                    { 'index': '421', 'menuName': '会员资产', 'url': '/console/asset/approval' },
                    { 'index': '422', 'menuName': '平台资产', 'url': '/console/asset/approval2' },
                ]
            },
        ]
    },
];

// 用户菜单
var userData = [
    { 'index': '1', 'menuName': '首页', 'url': '/console/user/index', 'icon': 'el-icon-s-home' },
    {
        'index': '2', 'menuName': '基础设施', 'icon': 'el-icon-s-platform',
        'children': [
            { 'index': '21', 'menuName': '申请虚拟桌面', 'url': '/console/vdi_user' },
            { 'index': '22', 'menuName': '申请虚拟主机', 'url': '/console/vm_user' },
            { 'index': '23', 'menuName': '申请Docker', 'url': '#' },
        ]
    },
    {
        'index': '3', 'menuName': '数据集', 'icon': 'el-icon-s-data',
        'children': [
            { 'index': '31', 'menuName': '申请数据集', 'url': '/console/user/datasource' },
            { 'index': '32', 'menuName': '查看我的申请', 'url': '/console/user/datasource/approval' },
        ]
    },
];

var menuData = {
    console: consoleData,
    user: userData
};