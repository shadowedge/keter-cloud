var Main = { data () { return {
            projects: [1, 2],
            activeName: 'first',
        };
    },

    created () {
        this.getApplication();
    },

    methods: {
        // 获取请求列表
        getApplication () {
            this.myGet('/api/v1/user/assets/get/application')
            .then(function (node) {
                this.projects = node.data;
                this.projects.map(node => {
                    let haha = [];
                    let id = node.id.split(',')
                        , asset_apply = node.asset_apply ? node.asset_apply.split(',') : null
                        , asset_id = node.asset_id ? node.asset_id.split(',') : null
                        , asset_name = node.asset_name ? node.asset_name.split(',') : null
                        , asset_type = node.asset_type ? node.asset_type.split(',') : null
                        , asset_file_name = node.asset_file_name ? node.asset_file_name.split(',') : null
                        , haha_comment = node.haha_comment ? node.haha_comment.split(',') : null
                        , haha_date = node.haha_date ? node.haha_date.split(',') : null
                        , haha_result = node.haha_result ? node.haha_result.split(',') : null
                        , ping_date = node.ping_date ? node.ping_date.split(',') : null
                        , ping_id = node.ping_id ? node.ping_id.split(',') : null
                        , ping_name = node.ping_name ? node.ping_name.split(',') : null
                        , ping_result = node.ping_result ? node.ping_result.split(',') : null
                        , ti_date = node.ti_date ? node.ti_date.split(',') : null;

                    for (var i = 0; i < asset_id.length; i++) {
                        let temp = {};
                        temp.id = id[i];
                        temp.asset_apply = asset_apply[i];
                        temp.asset_id = asset_id[i];
                        temp.asset_name = asset_name[i];
                        temp.asset_type = asset_type[i];
                        temp.asset_file_name = asset_file_name[i];
                        temp.asset_type = asset_type[i];
                        temp.haha_comment = haha_comment ? haha_comment[i] : null;
                        temp.haha_date = haha_date ? haha_date[i] : null;
                        temp.haha_result = haha_result ? haha_result[i] : null;
                        temp.ping_date = ping_date ? ping_date[i] : null;
                        temp.ping_id = ping_id ? ping_id[i] : null;
                        temp.ping_name = ping_name ? ping_name[i] : null;
                        temp.ping_result = ping_result ? ping_result[i] : null;
                        temp.ti_date = ti_date[i];
                        haha.push(temp);
                    }

                    node.ti_date = ti_date[0];
                    node.haha = haha;
                    return node;
                });
            })
            .catch(function (err) {
                this.$message({
                    message: err.error,
                    type: 'error'
                });
            });
        },

        // 取消申请
        cancelApply (id) {
            this.myPost('/api/v1/user/assets/cancel/application/' + id)
            .then(function (res) {
                let result = res.data;
                if (result) {
                    this.$message({
                        message: '取消成功！',
                        type: 'success'
                    });
                    this.getApplication();
                } else {
                    this.$message({
                        message: '该申请在审批中，不能删除',
                        type: 'error'
                    });
                }
            })
            .catch(function (err) {
                this.$message({
                    message: err.error,
                    type: 'error'
                });
            });
        },

    },

    filters: {
       cvApprovalResult (value) {
           let type = codeList.cvApprovalResult.filter(item => item.value == value );
           if (type.length > 0) return type[0].label;
           return "看吧";
        },

       cvApprovalResult2 (value) {
           if (value == 'P') return "俺也一样！！";
           return "看吧";
        },
    }
}

var Ctor = Vue.extend(Main);
var haha = new Ctor().$mount('#app');

