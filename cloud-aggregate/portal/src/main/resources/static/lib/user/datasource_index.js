var Main = {
    data () {
        return {
            state1: '',
            dataSetFromDB: [],
            dataSet: [],

            dataSetCondition: [],
            searchItem: [],
            myDate: '',

            pageNo: 1,
            pageSize: 10,

            dialogVisible: false,

            checkBoxValues: [], allCheck: false, // checkbox设置
            checkBoxValues2: [], checkBoxValues3: [],

            projects: [],
        };
    },

    created () {
        this.getMixDataSet(this.pageNo, this.pageSize)
        .then(this.searchConfig);
    },

    methods: {
        // 查询功能
        querySearch(queryString, cb) {
            let ds = this.dataSetFromDB;
            let results = queryString ? ds.filter(this.createFilter(queryString)) : ds;
            // 调用 callback 返回建议列表的数据
            cb(results);
        },

        createFilter(queryString) {
            return (ds) => {
                return (ds.name.toLowerCase().indexOf(queryString.toLowerCase()) === 0);
            };
        },

        // 查询混合后的数据集
        getMixDataSet (pageNo, pageSize) {
            return this.getDataSet()
            .then(data => {
                // 混合显示
                let params = [
                    { key: 'associator_id', key2: 'id', srcName: 'name', targetName: 'associator_name' },
                    { key: 'op_id', key2: 'id', srcName: 'name', targetName: 'op_name' },
                    { key: 'provider_id', key2: 'id', srcName: 'name', targetName: 'provider_name' },
                ];
                let result = myMixinData(data, params);
                if (result) {
                    this.dataSetFromDB = result;
                    this.dataSet = result.slice((pageNo - 1) * pageSize, pageNo * pageSize);
                } else {
                    this.dataSetFromDB = [];
                    this.dataSet = [];
                }
                return data;
            })
            .catch(err => console.info(err));
        },

        // 查询条件配置
        searchConfig (data) {
            this.searchItem = [
                {'title': '数据状态', 'key': 'assetStatus', 'value': codeList.assetStatus},
                {'title': '原生衍生', 'key': 'assetLifeCycles', 'value': codeList.assetLifeCycles},
                {'title': '数据类型', 'key': 'assetTypes', 'value': codeList.assetTypes.filter(node => node.value !== 'P')},
            ];
            let item = {
                title: '数据用途', key: 'assetUsage', 'value': data.a.map(node => ({ value: node.id, label: node.purpose }))
            };
            this.searchItem.push(item);
            item = {
                title: '所属会员', key: 'assetMember', 'value': data.b.map(node => ({ value: node.id, label: node.name }))
            };
            this.searchItem.push(item);
        },

        // 获取数据集
        getDataSet (callback) {
            let param = {}
            // 获取筛选条件
            let keys = ['assetStatus', 'assetLifeCycles', 'assetTypes', 'assetUsage', 'assetMember'];
            if (this.state1) param.name = this.state1;
            keys.forEach(key => {
                let temp = this.dataSetCondition.filter(node => node.key == key).map(node => "'" + node.value + "'").join(',');
                if (temp || temp != "") param[key] = temp;
            });
            // 获取筛选日期
            if (this.myDate && this.myDate.length === 2) {
                param.begindate = formatDate(this.myDate[0]);
                param.enddate = formatDate(this.myDate[1]);
            }

            return this.myGet('/api/v1/user/assets/browse/my/asset', param)
            .then(res => ({ a: res.data.proAssets, b: res.data.orgs, c: res.data.users, d: res.data.users }))
            .catch(err => {
                this.$message({
                    message: err.error,
                    type: 'error'
                });
            });
        },

        configCheckBox () {
            this.checkBoxValues = [];
            for (var i = 0; i < 1000; i ++) {
                this.checkBoxValues.push(null);
            }
        },

        checkAll (item) {
            this.allCheck = item;
            if (item) {
                this.checkBoxValues = [];
                this.dataSetFromDB.forEach((node, index) => this.checkBoxValues.push(node.id + '_' + index));
            } else {
                this.configCheckBox();
            }
        },

        // 设置页面显示数量
        sizeChange (pageSize) {
            this.pageNo = 1;
            this.pageSize = pageSize;
            this.configCheckBox();
            this.getMixDataSet(this.pageNo, this.pageSize);
        },

        // 翻页
        pageChange (pageNo) {
            this.pageNo = pageNo;
            this.getMixDataSet(this.pageNo, this.pageSize);
        },

        // 按名称查询
        search () {
            this.configCheckBox();
            this.getMixDataSet(1, this.pageSize);
        },

        // 查看数据集详细信息
        requestDataSet (item) {
            console.info(item);
        },

        // 打开申请使用数据集文本框
        openShenqing () {
            this.dialogVisible = true;
            this.checkBoxValues2 = clone(this.checkBoxValues).filter(node => node).map(node => node.split('_')[0]);
            this.checkBoxValues3 = [];
            // 获取参与项目
            this.myGet('/api/v1/user/projects/query/member/currentUserProjects')
            .then(function (res) {
                this.projects = res.data;
                this.projects.forEach(node => this.checkBoxValues3.push(false));
            })
            .catch(function (err) {
                console.info(err);
            });
        },

        // 发送邮件
        sendMail () {
            let assetIds = this.checkBoxValues2.filter(node => node);
            let projectIds = this.checkBoxValues3.filter(node => node);

            if (assetIds.length == 0 || projectIds.length == 0) {
                this.$message({
                    message: '请选择需要申请的项目和数据',
                    type: 'error'
                });
                return;
            }

            this.myPost('/api/v1/user/assets/create/application', {assetIds: assetIds, projectIds: projectIds})
            .then(function (node) {
                this.dialogVisible = false;
                this.$message({
                    message: '申请已上传，请等待审批，审批通过后会在邮件中通知',
                    type: 'success'
                });
            })
            .catch(function (err) {
                this.$message({
                    message: err.error,
                    type: 'error'
                });
            });

        },
    },

    mounted () {
    },

    filters: {
       changeAssetType (value) {
           let type = codeList.assetTypes.filter(item => item.value == value );
           if (type.length > 0) return type[0].label;
           return "未知类型";
        },

       changeAssetStatus (value) {
           let status = codeList.assetStatus.filter(item => item.value == value );
           if (status.length > 0) return status[0].label;
           return "未知类型";
       },

       changeAssetLifeCycle (value) {
           let LifeCycle = codeList.assetLifeCycles.filter(item => item.value == value );
           if (LifeCycle.length > 0) return LifeCycle[0].label;
           return "未知类型";
       },
    },

    watch: {
        checkBoxValues (newArgs, oldArgs) {
            let csl = newArgs.filter(node => node).length;
            let tdl = this.dataSetFromDB.length;
            if (csl === tdl) {
                this.allCheck = true;
            } else {
                this.allCheck = false;
            }
        }
    }

}

var Ctor = Vue.extend(Main);
var haha = new Ctor().$mount('#app');
