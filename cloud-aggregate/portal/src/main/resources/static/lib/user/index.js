var Main = {
    data () {
        return {
            dataSet: [
                { img: '/images/haha93.jpg', name: '小咖喱黄不辣', dep: '少林神档' },
                { img: '/images/haha94.jpg', name: '古汉语专家', dep: '清华池洗浴' },
                { img: '/images/haha95.jpg', name: '天下第一大善人', dep: '妙峰山' },
                { img: '/images/haha97.jpg', name: '快来', dep: '水月庵' },
                { img: '/images/haha98.jpg', name: '尿频', dep: '滴答宫' },
                { img: '/images/haha99.jpg', name: '朴一生', dep: '王子殿下' },
                { img: '/images/user-header.jpg', name: '川普大将军', dep: '铠甲巨人' },
                { img: '/images/user-header2.jpg', name: '宇宙大元帅', dep: '猩猩巨人' },
                { img: '/images/user-header3.jpg', name: '安倍小玛丽', dep: '马车巨人' },
            ],

            chartSettings: {
                area: true
            },

            chartData: {
                columns: ['日期', '使用量'],
                rows: [
                    { '日期': 'Jan', '使用量': 1393 },
                    { '日期': 'Feb', '使用量': 3530 },
                    { '日期': 'Mar', '使用量': 2923 },
                    { '日期': 'Apr', '使用量': 1723 },
                    { '日期': 'May', '使用量': 3792 },
                    { '日期': 'June', '使用量': 4593 }
                ]
            },

            chartData2: {
                columns: ['日期', '下载量'],
                rows: [
                    { '日期': 'Jan', '下载量': 393 },
                    { '日期': 'Feb', '下载量': 530 },
                    { '日期': 'Mar', '下载量': 923 },
                    { '日期': 'Apr', '下载量': 723 },
                    { '日期': 'May', '下载量': 792 },
                    { '日期': 'June', '下载量': 593 }
                ]
            },

            chartExtend: {
                yAxis: {
                    splitLine:{
                        show: true,
                        lineStyle: {
                            color: ['#DCDFE6'],
                            type:'dashed',
                        }
                    },//去除网格线
                    axisLine: {
                        show: false,
                        lineStyle: {
                            color: '#9fa2a9',
                            width: 1
                        }
                    }
                },
                xAxis: {
                    splitLine:{show: false},//去除网格线
                    axisLine: {
                        show: false,
                        lineStyle: {
                            color: '#9fa2a9',
                            width: 1
                        }
                    }
                }
            },

        };
    },

    created () {

    },

    methods: {
    },

    mounted () {
    },

    filters: {
    },

}

var Ctor = Vue.extend(Main);
var haha = new Ctor().$mount('#app');
