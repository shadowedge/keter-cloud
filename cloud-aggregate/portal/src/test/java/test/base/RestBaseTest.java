package test.base;

import com.google.common.collect.Maps;
import com.keter.framework.test.base.AbstractBaseTest;
import org.apache.commons.lang3.Validate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;

import java.util.Map;

import static io.restassured.RestAssured.given;
import static io.restassured.path.json.JsonPath.from;

/**
 * Created by gulx@neusoft.com on 2017/6/22.
 */
public class RestBaseTest  extends AbstractBaseTest {

    protected static String context = "http://127.0.0.1:7777";
//    protected static String Context = "http://192.168.1.111:41503";

    private static final Logger logger = LoggerFactory.getLogger(RestBaseTest.class);

    public  static void setContext(String context){
        RestBaseTest.context = context;
    }

    public Map jwtHeaders(String username , String password){
        Map headers = Maps.newHashMap();
        headers.put("Content-Type", MediaType.APPLICATION_JSON_UTF8_VALUE);
        headers.put("XAuthorization", "Bearer " + token(username, password));
        return headers;
    }

    public Map jwtHeaders(String token){
        Map headers = Maps.newHashMap();
        headers.put("Content-Type", MediaType.APPLICATION_JSON_UTF8_VALUE);
        headers.put("XAuthorization", "Bearer " + token);//Authorization
        return headers;
    }

    protected String token(String username, String password){
        Map headers = Maps.newHashMap();
        headers.put("Content-Type", MediaType.APPLICATION_JSON_UTF8_VALUE);
        String auth = given().headers(headers).body("{\"username\":\""+username+"\",\"password\":\""+password+"\"}").post(context+"/authenticate").asString();
        logger.info("auth:{}",auth);
        String token = from(auth).get("data");
        Validate.notEmpty(token,"认证失败!!！");
        return token;
    }

}
