package test.base;

import app.keter.PortalAppStart;
import app.keter.portal.security.CurrentUser;
import app.keter.portal.security.SecurityService;
import com.alibaba.fastjson.JSONArray;
import com.keter.framework.test.base.AbstractBaseTest;
import com.keter.framework.web.component.sequence.SIDHolder;
import app.keter.portal.security.core.UserDetailsImpl;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.Before;
import org.mockito.MockitoAnnotations;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Import;

/**
 * 测试基类:适用于Web环境的测试
 */
@SpringBootTest(classes = WebBaseTest.class)
@Import(PortalAppStart.class)
public class WebBaseTest extends AbstractBaseTest {
    /**
     * Logger for this class
     */
    private static final Logger logger = LoggerFactory.getLogger(WebBaseTest.class);

    @Before
    public void mock() {
        MockitoAnnotations.initMocks(this);
    }

    @Autowired
    protected SIDHolder sidHolder;

    /**
     * 模拟当前登录用户
     *
     * @param id
     */
    protected UserDetailsImpl mockCurrentUser(String id) {
       return  mockCurrentUser(id, "anyone", "any", "anymail", "ROLE_USER");
    }

    /**
     * 模拟当前登录用户
     *
     * @param username
     * @param email
     */
    protected UserDetailsImpl mockCurrentUser(
            String username,
            String email) {
       return  mockCurrentUser(RandomStringUtils.randomAlphabetic(5), username, "any", email, "ROLE_USER");
    }

    /**
     * 完整模拟当前登录用户
     *
     * @param username
     * @param email
     */
    protected UserDetailsImpl mockCurrentUser(
            String id,
            String username,
            String password,
            String email,
            String role) {
        JSONArray roles = new JSONArray();
        roles.add(role);

        CurrentUser.setCurrentUser(new UserDetailsImpl(id, username, password, email, roles));
        UserDetailsImpl user = new UserDetailsImpl(id, username, password, email, roles);
        return user;
    }

    protected Long mockSid(){
        return sidHolder.getOrGenerate();
    }

}
