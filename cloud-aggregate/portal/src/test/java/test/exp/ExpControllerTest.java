package test.exp;

import app.keter.portal.exp.ExpController;
import com.keter.framework.web.result.JSONResult;
import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import test.base.WebBaseTest;

import javax.servlet.ServletRequest;

public class ExpControllerTest extends WebBaseTest {
	/**
	* Logger for this class
	*/
	private static final Logger logger = LoggerFactory.getLogger(ExpControllerTest.class);

	@Autowired
	ExpController controller;

	@Test
	public void emptyDataArray(){
		JSONResult result = controller.emptyDataArray();
		logger.info("res:{}",result);
	}

	/**
	 * 舱壁隔离
	 */
	@Test
	public void isolate(){
		boolean result = controller.   isolate();
		logger.info("舱壁隔离：两个restTemplate的HashCode应不同:{}",result);
		Assert.assertEquals(true,result);
	}

//	@Test
	public void timeout(){
		logger.info("测试熔断器的超时是否全局可用！");
		try {
			logger.info("res:{}",controller.timeout());
		} catch (Exception e) {
			logger.error("捕获异常：", e);
		}
	}

//	@Test
	public void defaultTimeout(){
		logger.info("测试RestTemplate的默认超时机制(120秒以上)。。。");
		try {
			logger.info("res:{}",controller.defaultTimeout());
		} catch (Exception e) {
			logger.error("捕获异常：", e);
		}
		ServletRequest request = null;
		request.getParameter("");
	}
}
