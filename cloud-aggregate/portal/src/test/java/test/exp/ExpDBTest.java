package test.exp;

import com.jfinal.plugin.activerecord.Db;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Import;
import test.base.WebBaseTest;
import test.base.DBComponent;

/**
 * 验证测试是否具备数据库能力
 */
@Import(DBComponent.class)  //导入数据库能力组件
public class ExpDBTest extends WebBaseTest {
	/**
	* Logger for this class
	*/
	private static final Logger logger = LoggerFactory.getLogger(ExpDBTest.class);

	/**
	 * DB能力测试
	 */
	@Test
	public void db() {
		logger.info("codelist size:{}",Db.use("common").queryInt("select count(1) from codelist"));
	}


}
