package test.exp;

import app.keter.portal.exp.ExpFeignController;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import test.base.WebBaseTest;

public class ExpFeignControllerTest extends WebBaseTest {
	/**
	* Logger for this class
	*/
	private static final Logger logger = LoggerFactory.getLogger(ExpFeignControllerTest.class);

	@Autowired
	ExpFeignController controller;

	@Test
	public void sid(){
		String result = controller.sid(mockSid());
		logger.info("result:{}",result);
	}

	@Test
	public void aggr(){
		controller.aggrObject();
	}

//	@Test
	public void timeout(){
		logger.info("测试熔断器的超时是否可用");
		try {
			logger.info("res:{}",controller.timeout());
		} catch (Exception e) {
			logger.error("捕获异常：", e);
		}
	}
}
