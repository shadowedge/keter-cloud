package test.exp;

import app.keter.portal.exp.ExpGzipController;
import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import test.base.WebBaseTest;

import java.io.IOException;

public class ExpGzipControllerTest extends WebBaseTest {
	/**
	* Logger for this class
	*/
	private static final Logger logger = LoggerFactory.getLogger(ExpGzipControllerTest.class);

	@Autowired
	ExpGzipController controller;

	/**
	 * SID幂等
	 */
	@Test
	public void gzip() throws IOException {
		Assert.assertTrue(controller.gzipOKHttp().contains("哈哈"));
	}

//    @Test //Failed
	public void gzipHttpClient() throws IOException {
		Assert.assertTrue(controller.gzipHttpClient().contains("哈哈"));
	}
}
