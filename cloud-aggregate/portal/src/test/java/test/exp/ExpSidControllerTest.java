package test.exp;

import app.keter.portal.exp.ExpSidController;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import test.base.WebBaseTest;

import java.util.concurrent.ExecutionException;

public class ExpSidControllerTest extends WebBaseTest {
	/**
	* Logger for this class
	*/
	private static final Logger logger = LoggerFactory.getLogger(ExpSidControllerTest.class);

	@Autowired
	ExpSidController controller;

	/**
	 * SID幂等
	 */
	@Test
	public void sid(){
		logger.info("调用方法时，产生的sid应相同。");
		controller.sid(mockSid());
	}

	@Test
	public void sidInThread() throws ExecutionException, InterruptedException {
		logger.info("调用方法时，产生的sid应相同。");
		controller.sidInThread(mockSid());
	}
}
