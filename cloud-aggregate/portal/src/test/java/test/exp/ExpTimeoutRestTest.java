package test.exp;

import com.google.common.util.concurrent.ListeningExecutorService;
import com.jfinal.kit.HttpKit;
import com.keter.framework.core.util.ConcurrentUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import test.base.RestBaseTest;

import java.util.concurrent.atomic.AtomicInteger;

public class ExpTimeoutRestTest extends RestBaseTest {
    // 声明线程池
    ListeningExecutorService service = ConcurrentUtil.service();

    /**
     * Logger for this class
     */
    private static final Logger logger = LoggerFactory.getLogger(ExpTimeoutRestTest.class);

    /**
     * 框架提供的Resttemplate组件超时测试
     * 测试结果：即时原子服务响应时间很长，聚合服务可自行超时并正常处理其他请求
     */
//    @Test
    public void commonTimeout() throws InterruptedException {
        AtomicInteger count1 = new AtomicInteger();
        for (int i = 0; i < 100; i++) {
            Thread.sleep(10);
            service.submit(() -> {
                logger.info("Pool1: Sending request {} ...", count1.getAndIncrement()+1);
                String res2 = HttpKit.get("http://192.168.1.11:7777/public/timeout/common");
                logger.info("res:{}", res2);
            });
        }
        logger.info("haha");
        Thread.sleep(Integer.MAX_VALUE);
    }

    /**
     * 默认RestTemplate超时测试
     * 测试结果：聚合服务全部挂起，无法正常处理其他请求
     */
//    @Test
    public void simpleTimeout() throws InterruptedException {
        AtomicInteger count1 = new AtomicInteger();
        for (int i = 0; i < 10000; i++) {
            Thread.sleep(10);
            service.submit(() -> {
                logger.info("Pool1: Sending request {} ...", count1.getAndIncrement()+1);
                String res2 = HttpKit.get("http://192.168.1.11:7777/public/timeout/default");
                logger.info("res:{}", res2);
            });
        }
        logger.info("haha");
        Thread.sleep(Integer.MAX_VALUE);
    }

}
