package test.portal.admin;

import app.keter.portal.domain.admin.OrgAdminFeignController;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.jfinal.plugin.activerecord.Db;
import com.keter.framework.web.result.JSONResult;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Import;
import test.base.DBComponent;
import test.base.WebBaseTest;

@Import(DBComponent.class) //使测试类具有DB操作的能力
public class OrgAdminControllerTest extends WebBaseTest {

	/**
	* Logger for this class
	*/
	private static final Logger logger = LoggerFactory.getLogger(OrgAdminControllerTest.class);

	@Autowired
	OrgAdminFeignController controller;

	@Test
	public void  findAll(){
		JSONResult res = controller.findAll();
		logger.info("res:{}",res);
		JSONArray orgs = res.data();
		Assert.assertEquals("总部", orgs.getJSONObject(0).get("name"));
	}

	@Test
	public void  findOne(){
		JSONResult res = controller.findById("1");
		logger.info("res:{}",res.object());
		Assert.assertEquals("总部", res.object().getString("name"));
	}

	@Test
	public void  add(){
		Db.update("delete from org where name like 'test-portal%'");
		String orgName = "test-portal-"+RandomStringUtils.randomAlphabetic(5);
		JSONResult res = controller.add(mockNewOrg(orgName));
		logger.info("res:{}",res.object());
	}

	@Test
	public void  addUsers(){
		JSONResult res = controller.addUsers("999","1,2");
		logger.info("res:{}",res);
	}

	@Test
	public void  findUsersById(){
		JSONResult res = controller.findUsersById("999");
		logger.info("res:{}",res);
		Assert.assertNotNull(res.array());
		res = controller.findUsersById("99999");
		logger.info("res:{}",res);
		Assert.assertTrue(res.isEmpty());
	}

	protected JSONObject mockNewOrg(String name){
		JSONObject org = new JSONObject();
		org.put("name",name);
		org.put("address","机构地址");
		org.put("contact","联系人名称");
		org.put("contact_phone","0248366666");
		org.put("contact_mobile","13800138000");
		org.put("email","test@test.como");
		org.put("type","T");
		org.put("remark","unittest");
		return org;
	}
}
