package test.portal.admin;

import app.keter.portal.domain.admin.UserAdminController;
import app.keter.portal.security.auth.AuthService;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.jfinal.plugin.activerecord.Db;
import com.keter.framework.web.result.JSONResult;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Import;
import org.springframework.security.authentication.BadCredentialsException;
import test.base.DBComponent;
import test.base.WebBaseTest;

@Import(DBComponent.class) //使测试类具有DB操作的能力
public class UserAdminControllerTest extends WebBaseTest {
	/**
	* Logger for this class
	*/
	private static final Logger logger = LoggerFactory.getLogger(UserAdminControllerTest.class);

	@Autowired
    UserAdminController controller;

	@Autowired
	private AuthService authenService;

	@Test
	public void findAll(){
		JSONResult res = controller.findAll();
		logger.info("res:{}",res);
		JSONArray users = res.array();
		Assert.assertEquals("admin", users.getJSONObject(0).get("username"));
	}

	@Test
	public void findById(){
		JSONResult res = controller.findById("1");
		logger.info("res:{}",res);
		Assert.assertEquals("admin", res.object().get("username"));
	}

	@Test
	public void update(){
		JSONObject obj = new JSONObject();
		obj.put("id","99");
		obj.put("name","test-user-update-普通的Disco我们普通的摇~~~");
		String remark = RandomStringUtils.randomAlphabetic(5);
		obj.put("remark",remark);
		JSONResult res = controller.update(obj);
		logger.info("res:{}",res);
		Assert.assertEquals(remark, controller.findById("99").object().getString("remark"));
	}

	@Test
	public void  add() throws InterruptedException {
		Db.update("delete from user where username like 'test-portal%'");
		mockCurrentUser("anyone","gulixing@msn.com");
		JSONObject user = new JSONObject();
		String username = "test-portal-"+ RandomStringUtils.randomAlphabetic(5);
		user.put("name","portal-test-"+RandomStringUtils.randomAlphabetic(5));
		user.put("username",username);
//		user.put("password",RandomStringUtils.randomAlphabetic(6));
		user.put("remark","portal-Controller-Test");
		user.put("role_id","99");
		user.put("org_id","99");
		user.put("email",RandomStringUtils.randomAlphabetic(5)+"@portal-test.com");
		JSONResult res = controller.add(user);
		logger.info("res:{}",res);
		Assert.assertEquals(username,res.object().get("username"));
		//给发邮件留点时间。。。
		Thread.sleep(3000);
//		Assert.assertEquals(username, controller.findById(res.data().getString("id")).data().get("username"));
	}
	
//	@Test
	public void  passChange(){
		logger.info("确认用户使用原密码可以登录成功...");
		authenService.login("user","1");
		mockCurrentUser("99");
		JSONObject obj = new JSONObject();
		obj.put("oldpass","1");
		obj.put("newpass","99");
		controller.passChange(obj);
		logger.info("验证用户是否可以用新密码登录...");
		authenService.login("user","99");

		logger.info("改回默认密码，防止后续测试失败:");
		obj.put("oldpass","99");
		obj.put("newpass","1");
		controller.passChange(obj);
		authenService.login("user","1");

		logger.info("原密码输入错误测试...");
		obj.put("oldpass","error");
		obj.put("newpass","any");
		JSONResult result = controller.passChange(obj);
		logger.info("res:{}",result);
		Assert.assertTrue(result.isError());
		logger.info("error:{}",result.error());

		logger.info("验证密码修改是否失败...");
		try {
			authenService.login("user","any");
		} catch (BadCredentialsException e) {
			logger.error("登录失败:{}", e.getMessage());
		}
	}
}
