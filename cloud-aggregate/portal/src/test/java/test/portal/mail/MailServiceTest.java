package test.portal.mail;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import app.keter.portal.mail.MailService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import test.base.WebBaseTest;

public class MailServiceTest extends WebBaseTest { //
	/**
	* Logger for this class
	*/
	private static final Logger logger = LoggerFactory.getLogger(MailServiceTest.class);

	@Autowired
	MailService service;

//	@Test
	public void mailTest(){
		service.send(makeMail());
	}

	private JSONObject makeMail(){
		JSONObject obj = new JSONObject();
		obj.put("title","CareVault平台用户账号创建成功！");
		obj.put("html","请登录<a href=\"http://carevault.keter.com/\">CareVault平台</a>，修改密码！");
		JSONArray to = new JSONArray();
		to.add("gulixing@msn.com");
		obj.put("to",to);
		JSONArray cc = new JSONArray();
		cc.add("gulixing@msn.com");
		obj.put("cc",cc);
		return  obj;
	}
}
