package test.portal.security;

import app.keter.portal.security.auth.AuthController;
import com.alibaba.fastjson.JSONObject;
import com.keter.framework.web.result.JSONResult;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import test.base.WebBaseTest;

import javax.servlet.http.HttpServletRequest;

import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

public class AuthControllerTest extends WebBaseTest {
	/**
	* Logger for this class
	*/
	private static final Logger logger = LoggerFactory.getLogger(AuthControllerTest.class);

	@Autowired
	AuthController authenController;

	@Mock HttpServletRequest request;

	JSONObject authObject;

	@Before
	public  void mock(){
		MockitoAnnotations.initMocks(this);
		authObject = new JSONObject();
		authObject.put("username","admin");
		authObject.put("password","1");
//		when(authenController.me()).thenReturn(WrapUtil.wrap());
	}

	private void mockRequest() {
//		String token = authenController.authenticate(authObject).ensure().string();
		String token = authenController.authenticate(authObject).data();
		//模拟Request header请求：放入一个合法的token
		when(request.getHeader(anyString())).thenReturn("Bearer "+token);
	}

	private void mockInvalidToken() {
		//模拟非法token
		when(request.getHeader(anyString())).thenReturn("Bearer "+"some invalid token");
	}

	@Test
	public void authenticate(){
		String token = authenController.authenticate(authObject).data();
		logger.info("token:{}",token);
		Assert.assertNotNull(token);
	}

//	@Test
	public void authenticateFaild(){
		authObject = new JSONObject();
		authObject.put("username","admin");
		authObject.put("password","xxx");
		String token = authenController.authenticate(authObject).data();
		logger.info("token:{}",token);
		Assert.assertNotNull(token);
	}

	@Test
	public void refresh(){
		mockRequest();//不能放入 @Before 方法内，因为无法获取注入的authController对象！
		JSONResult res = authenController.refresh(request);
		logger.info("token:{}",res);
		Assert.assertNotNull(res.data());

		mockInvalidToken();
		res = authenController.refresh(request);
		logger.info("error:{}",res.isError());
		Assert.assertTrue(res.isError());
	}

	@Test
	public void refreshError(){
		mockInvalidToken();
		JSONResult res = authenController.refresh(request);
		logger.info("error: {}",res.isError());
		Assert.assertTrue(res.isError());
	}

	@Test
	public void me() throws InterruptedException {
		mockCurrentUser(
				"123",
				"sxc",
				"cxsj",
				"nmsl@daidai.com",
				"tianhuang");
		JSONResult res = authenController.me();
		logger.info("res:{}",res);
		Assert.assertEquals("123",res.string("id"));
		Thread.sleep(1000);
	}

	@Test
	public void meOfHys() throws InterruptedException {
		mockCurrentUser(
				"123",
				"sxc",
				"cxsj",
				"nmsl@daidai.com",
				"tianhuang");
		authenController.meOfHys(null);
		Thread.sleep(1000);
	}
}
