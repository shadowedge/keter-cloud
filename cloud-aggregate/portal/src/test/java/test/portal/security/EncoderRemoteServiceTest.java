package test.portal.security;

import app.keter.portal.security.core.PasswordEncoderImpl;
import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import test.base.WebBaseTest;

public class EncoderRemoteServiceTest extends WebBaseTest { //
	/**
	* Logger for this class
	*/
	private static final Logger logger = LoggerFactory.getLogger(EncoderRemoteServiceTest.class);

	@Autowired
	PasswordEncoderImpl encoder;

	@Test
	public void encodeMatcherTest(){
		String encStr = encoder.encode("123");
		logger.info("enc:{}",encStr);
		Assert.assertTrue(encoder.matches("123",encStr));
	}

}
