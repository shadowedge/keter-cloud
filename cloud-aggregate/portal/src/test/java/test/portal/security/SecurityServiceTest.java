package test.portal.security;

import app.keter.portal.security.SecurityService;
import app.keter.portal.security.core.UserDetailsImpl;
import com.alibaba.fastjson.JSONArray;
import com.keter.framework.web.result.JSONResult;
import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import test.base.WebBaseTest;

import java.util.concurrent.ExecutionException;

// 安全类服务实现比较特殊：
// 由于跟Spring框架结合紧密，必须导入完整包
// 因此直接继承了ControllerBaseTest
public class SecurityServiceTest extends WebBaseTest { //
	/**
	* Logger for this class
	*/
	private static final Logger logger = LoggerFactory.getLogger(SecurityServiceTest.class);

	@Autowired
    SecurityService service;

	@Test
	public void generate(){
		String token = genToken();
		logger.info("token:{}",token);
		Assert.assertNotNull(token);
	}

	@Test
	public void getUser() throws ExecutionException {
		String token = genToken();
		UserDetailsImpl user = service.fromToken(token);
		Assert.assertEquals("haha",user.getUsername());
		//TODO:写一个理想的角色判断的断言
		logger.info("authorities:{}",user.getAuthorities());
	}

	@Test
	public void refreshResult(){
		String token1 = genToken();
		logger.info("token before:{}",token1);
		JSONResult token2  = service.refresh(token1);
		logger.info("token after:{}",token2.string());
		Assert.assertNotEquals(token1,token2);
		JSONResult token3  = service.refresh("haha");
		Assert.assertTrue(token3.isError());
	}

	@Test
	public void refreshResponse(){
		String token1 = genToken();
		logger.info("token before:{}",token1);
		JSONResult token2  = service.refreshResponse(token1);
//		logger.info("token after:{}",token2.getData());
//		logger.info("token after:{}",token2.data());
		Assert.assertNotNull(token2.data());
		Assert.assertNotEquals(token1,token2);
//		JSONResponse<String> token3  = service.refreshResponse("haha");
//		Assert.assertTrue(token3.isError());
	}

	private String  genToken() {
//		List<LinkedHashMap> roles = new ArrayList<>();
		JSONArray roles = new JSONArray();
		roles.add("haha");
		roles.add("hehe");
		UserDetailsImpl user = new UserDetailsImpl("123","haha","pwd","email",roles);
		return service.toToken(user);
	}

}
