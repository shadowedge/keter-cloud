package test.portal.security;

import com.jfinal.kit.JsonKit;
import app.keter.portal.security.core.UserDetailsServiceImpl;
import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import test.base.WebBaseTest;

// 安全类服务实现比较特殊：由于跟Spring框架结合紧密
// 因此必须导入完整包，因此直接继承了ControllerBaseTest
public class UserDetailsRemoteServiceTest extends WebBaseTest { //
	/**
	* Logger for this class
	*/
	private static final Logger logger = LoggerFactory.getLogger(UserDetailsRemoteServiceTest.class);

	@Autowired
	UserDetailsServiceImpl service;

	@Test
	public void loadUserByUsername(){
		UserDetails user = service.loadUserByUsername("admin");
//		logger.info("roles:{}", user.getAuthorities());
//		logger.info("json roles:{}", JsonKit.toJson(user.getAuthorities()));
		logger.info("user:{}", JsonKit.toJson(user));
		Assert.assertEquals("admin",user.getUsername());
	}

	@Test
	public void loadUserNotExist(){
		Assert.assertTrue(service.loadUserByUsername("NotExist") == null);
	}

}
