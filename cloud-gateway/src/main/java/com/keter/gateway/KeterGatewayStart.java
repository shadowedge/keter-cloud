package com.keter.gateway;

import com.keter.framework.core.util.ConcurrentUtil;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.gateway.route.RouteLocator;
import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class KeterGatewayStart {

	/**
	 * 基本的转发
	 * 当访问http://localhost:8080/jd
	 * 转发到http://jd.com
	 * @param builder
	 * @return
	 */
	@Bean
	public RouteLocator customRouteLocator(RouteLocatorBuilder builder) {
		return builder.routes()
				//basic proxy
				.route("jd_route", r ->r.path("/jd")
						.uri("http://jd.com:80/").order(0))
				.build();
	}

	public static void main(String[] args) {
		// 仅用于测试框架组件是否可用
		ConcurrentUtil.service();
		SpringApplication.run(KeterGatewayStart.class, args);
	}
}
