package com.keter.gateway.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class FallbackController {
	private static final Logger logger = LoggerFactory.getLogger(FallbackController.class);

	@GetMapping("/fallback")
	public String fallback() {
		logger.info("invoke fallback!");
		return "fallback";
	}
}
