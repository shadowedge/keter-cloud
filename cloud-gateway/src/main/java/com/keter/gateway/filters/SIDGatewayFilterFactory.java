package com.keter.gateway.filters;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cloud.gateway.filter.GatewayFilter;
import org.springframework.cloud.gateway.filter.factory.AbstractNameValueGatewayFilterFactory;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.stereotype.Component;

/**
 * 自定义过滤器
 */
@Component
public class SIDGatewayFilterFactory extends AbstractNameValueGatewayFilterFactory {

    private static final Logger logger = LoggerFactory.getLogger(SIDGatewayFilterFactory.class);

//    @Autowired
//    private Sequencer sequencer;

    @Override
    public GatewayFilter apply(NameValueConfig config) {
        return (exchange, chain) -> {
//            long sid = sequencer.genId();
            long sid = 1L;
            logger.info("request sid:{}",sid);
            ServerHttpRequest request = exchange.getRequest().mutate()
                    .header("x-sid", String.valueOf(sid))
                    .build();
            return chain.filter(exchange.mutate().request(request).build());
        };
    }
}