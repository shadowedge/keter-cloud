#!/bin/bash
# 使用maven插件构建镜像
# 开启Docker远程服务支持：
# ExecStart=/opt/kube/bin/dockerd -H tcp://0.0.0.0:2375 -H unix:///var/run/docker.sock
SCRIPT_DIR=$(cd $(dirname $0); pwd)
MAVEN_PATH="${SCRIPT_DIR}"/../../
#DOCKER_HOST_IP=10.9.28.100
DOCKER_HOST_IP=192.168.1.222
DOCKER_HOST=tcp://${DOCKER_HOST_IP}:2375

# 绿字
gecho(){
 printf "\n----------------\n\n"
 echo -e "\033[32m  $* \033[0m"
 printf "\n\n"
}

# 找到target目录下编译后的Dockerfile
findDockerFile(){
  find "${MAVEN_PATH}"/target/|grep Dockerfile |sort -rn | head -n1
}

#ls $(findDockerFile)





