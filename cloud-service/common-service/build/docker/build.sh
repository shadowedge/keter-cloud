#!/bin/bash
# 使用maven插件构建镜像
# 开启Docker远程服务支持：
# ExecStart=/opt/kube/bin/dockerd -H tcp://0.0.0.0:2375 -H unix:///var/run/docker.sock
SCRIPT_DIR=$(cd $(dirname $0); pwd)
# 变量初始化
. "${SCRIPT_DIR}"/_env.sh

if [ -n "$1" ];then
    DOCKER_HOST=$1
fi
gecho "Using Docker API" "${DOCKER_HOST}" "to build image..."
sh "${SCRIPT_DIR}"/stop.sh "${DOCKER_HOST}"
sh "${SCRIPT_DIR}"/remove.sh "${DOCKER_HOST}"
gecho "Building Image"
cd "${MAVEN_PATH}" && mvn -DskipTests=true -Ddocker.host="${DOCKER_HOST}" -Dmaven.wagon.http.ssl.insecure=true -Dmaven.wagon.http.ssl.allowall=true package docker:build

# 可选操作：用于观察DockerFile
gecho "Moving DockerFile.."

cp $(findDockerFile) "${SCRIPT_DIR}"

gecho "All Done!"


# Example:
# sh build.sh tcp://192.168.1.221:2375
# sh build.sh tcp://10.9.28.100:2375

# DockerFile:
# target/docker/keter-template/keter-cloud-service/v2-SNAPSHOT/build/Dockerfile