#!/bin/bash
# 开发环境：运行远程镜像
SCRIPT_DIR=$(cd $(dirname $0); pwd)
# 变量初始化
. "${SCRIPT_DIR}"/_env.sh

# 注册中心
REGISTER_IP=192.168.1.222
#宿主机存放数据库文件的目录: 不能以斜线(/)开头，不知道为啥！
DB_PATH=sdb/keter/db

if [ -n "$1" ];then
    DOCKER_HOST=$1
fi

cd "${SCRIPT_DIR}"/ &&  sh stop.sh
cd "${MAVEN_PATH}" &&  mvn -Ddocker.host="${DOCKER_HOST}" -Dconfig.register="${REGISTER_IP}"  -Dconfig.hostname="${DOCKER_HOST_IP}" -Ddb.path="${DB_PATH}" docker:run