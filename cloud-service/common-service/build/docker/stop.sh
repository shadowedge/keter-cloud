#!/bin/bash
# 开发环境：销毁远程镜像
SCRIPT_DIR=$(cd $(dirname $0); pwd)
# 变量初始化
. "${SCRIPT_DIR}"/_env.sh

#DOCKER_HOST=tcp://10.9.28.100:2375
if [ -n "$1" ];then
    DOCKER_HOST=$1
fi
gecho " Stopping Container... "
cd "${MAVEN_PATH}" &&  mvn -Ddocker.host="${DOCKER_HOST}" docker:stop