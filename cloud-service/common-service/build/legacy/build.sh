#! /bin/sh
# 传统部署模式：需要
# 不依赖于CI的构建脚本
SCRIPT_DIR=$(cd `dirname "$0"`; pwd)
# 从util 中统一取端口、名称和版本号等信息：
# 通过[文件名+版本号]方式区分版本，因此需要保证POM.xml中不出现finalName属性，即使用maven默认的打包策略
. "${SCRIPT_DIR}"/util.sh
# 执行打包操作
cd "${SCRIPT_DIR}"/../../
mvn -DskipTests=true package
# 通过当前版本生成最新版本(latest)，以便提供给cloud-deploy使用
cp "${SCRIPT_DIR}"/$(name)-$(version).jar "${SCRIPT_DIR}"/$(name)-latest.jar