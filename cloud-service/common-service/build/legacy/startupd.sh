#! /bin/sh
# 获取当前路径
SCRIPT_DIR=$(cd `dirname $0`; pwd)
# 从util 中统一取端口、名称和版本号等信息
. ${SCRIPT_DIR}/util.sh
# 获取主机IP
HOST_NAME=$(ip addr | grep 'state UP' -A2 | tail -n1 | awk '{print $2}' | cut -f1 -d '/')

# 杀进程
$(pkill)

# Jenkins运行正常但会出现 “Process leaked file descriptors. ”警告
nohup java -jar "${SCRIPT_DIR}"/$(name)-$(version).jar  --spring.profiles.active=ci --config.enabled=true --config.hostname="${HOST_NAME}"  --config.uri=http://192.168.1.111:9100 >  "${SCRIPT_DIR}"/startup.log&

# Example
# java -jar common-service-xxx.jar --spring.profiles.active=ci --config.enabled=true --config.hostname=192.168.1.221  --config.uri=http://192.168.1.221:9100
