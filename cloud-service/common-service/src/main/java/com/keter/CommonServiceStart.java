package com.keter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.server.WebServerFactoryCustomizer;
import org.springframework.boot.web.servlet.context.ServletWebServerInitializedEvent;
import org.springframework.boot.web.servlet.server.ConfigurableServletWebServerFactory;
import org.springframework.context.ApplicationListener;
import org.springframework.context.annotation.ComponentScan;

/**
 * Spring Boot 应用的入口文件
 * 适用于所有com.keter服务，自动导入全部组件
 * @author gu
 */
@SpringBootApplication
@ComponentScan("com.keter.common")
public class CommonServiceStart implements ApplicationListener<ServletWebServerInitializedEvent>, WebServerFactoryCustomizer<ConfigurableServletWebServerFactory> {
    private static final Logger logger = LoggerFactory.getLogger(CommonServiceStart.class);

    public static void main(String[] args) {
        SpringApplication.run(CommonServiceStart.class, args);
        logger.info("COMMON service started!");
    }

    @Override
    public void customize(ConfigurableServletWebServerFactory factory) {
    }

    @Value("${spring.cloud.consul.discovery.instanceId}")
    String instId;

    @Value("${spring.cloud.config.enabled:false}")
    boolean remoteConfig;

    @Override
    public void onApplicationEvent(ServletWebServerInitializedEvent servletWebServerInitializedEvent) {
        if(remoteConfig){
            System.out.println("启用远程配置");
        }
        System.out.println("\nSERVER PORT: "+servletWebServerInitializedEvent.getWebServer().getPort()+" \nINSTANCE: "+instId+"");
    }
}
