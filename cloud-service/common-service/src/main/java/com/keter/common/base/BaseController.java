package com.keter.common.base;

import com.keter.framework.web.base.AbstractBaseController;
import com.keter.framework.web.result.JSONResult;
import org.springframework.stereotype.Controller;

@Controller
public class BaseController extends AbstractBaseController {
    protected JSONResult success() {
        return JSONResult.success();
    }
}
