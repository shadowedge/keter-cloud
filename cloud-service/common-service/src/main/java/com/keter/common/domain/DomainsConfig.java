package com.keter.common.domain;

import com.keter.common.domain.codelist.Codelist;
import com.keter.common.domain.org.Org;
import com.keter.common.domain.user.User;
import com.keter.framework.persist.DataBaseMappings;
import com.keter.framework.persist.config.ActiveRecordConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class DomainsConfig {

    private static final Logger logger = LoggerFactory.getLogger(DomainsConfig.class);
    ActiveRecordConfig activeRecordConfig;

    private static boolean inited;

    @Bean
    public DataBaseMappings mappings() {
        if(!inited) {
            logger.info("自定义映射。。。");
            return arp -> {
                arp.addMapping("codelist", Codelist.class);
                arp.addMapping("user", User.class);
                arp.addMapping("org", Org.class);
                inited = true;
            };
        }
        return arp -> logger.info("纯粹是为了兼容批量运行的单体测试。。。");
    }
}