package com.keter.common.domain.codelist;

import com.keter.framework.persist.base.CacheModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

/**
 *  代码表
 */
public class Codelist extends CacheModel<Codelist> {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = LoggerFactory.getLogger(Codelist.class);

	/**
	 * 相当于领域内部对象，不直接对外暴露
	 */
	private static final Codelist dao = new Codelist();

	/**
	 * 静态方法相当于服务的【聚合层】，即领域外部只可以通过聚合调用领域内部方法
	 * @param kind
	 * @return
	 */
	public static List<Codelist>  findByKind(String kind){
		logger.debug("我是调试日志");
		return dao.find("select id,name,value from codelist where kind = ?",kind);
	}

	public static List<Codelist>  ex(){
		logger.debug("我是调试日志");
		return dao.find("select id,name,value from non-codelist");
	}

	@Override
	protected void validate() {

	}
}
