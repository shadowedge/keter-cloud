package com.keter.common.domain.codelist;

import com.keter.common.base.BaseController;
import com.keter.framework.web.result.JSONResult;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *  用户管理
 */
@RestController
@RequestMapping("/api/v1/codelist")
public class CodelistController extends BaseController {

    @ApiOperation(value = "根据代码类别获取代码")
    @GetMapping("/kind/{kind}")
    public JSONResult findBykind(
            @ApiParam(value = "代码类别", required = true)
            @PathVariable String kind){
        return wrap(Codelist.findByKind(kind));
    }

}
