package com.keter.common.domain.user;

import com.alibaba.fastjson.JSONObject;
import com.keter.common.base.BaseController;
import com.keter.framework.web.result.JSONResult;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

/**
 *  用户管理
 */
@RestController
@RequestMapping("/api/v1/users")
public class UserController extends BaseController {
    private static final Logger logger = LoggerFactory.getLogger(UserController.class);

    @ApiOperation(value = "创建普通角色用户")
    @PostMapping
    public JSONResult add(
            @ApiParam(value = "用户对象", required = true)
            @RequestBody JSONObject obj) {
        logger.info("user to add :{}",obj);
        User user = new User();
        user.set("username",obj.get("username"));
        user.set("password",obj.getString("password"));
        user.set("name",obj.get("name"));
        user.set("email",obj.get("email"));
        user.set("status",obj.get("status"));
        user.set("remark",obj.get("remark"));
        // role_id 和 org_id 并非数据表字段，因此必须使用put方法
        user.put("role_id",obj.get("role_id"));
        user.put("org_id",obj.get("org_id"));
        // 使用角色保存用户，关联组织机构信息
        user.save();
        return wrap(user);
    }

    @ApiOperation(value = "获取全部用户")
    @GetMapping
    public JSONResult findAll(){
        return wrap(User.findAllUsers());
    }

    @ApiOperation(value = "根据用户ID获取单个用户")
    @GetMapping("/{id}")
    public JSONResult findById(
            @ApiParam(value = "用户ID", required = true)
            @PathVariable String id){
        return wrap(User.findUserById(id));
    }

    @ApiOperation(value = "根据用户账号获取单个用户")
    @GetMapping("/username/{username}")
    public JSONResult findByUsername(
            @ApiParam(value = "用户账号", required = true)
            @PathVariable String username){
        return wrap(User.findByUsername(username));
    }

    @ApiOperation(value = "根据用户ID获取组织机构ID")
    @GetMapping("/{id}/orgs")
    public JSONResult findOrgByUserId(
            @ApiParam(value = "用户ID", required = true)
            @PathVariable String id){
        return wrap(User.findOrgIdsByUserId(id));
    }

    @ApiOperation(value = "根据用户ID串获取多个用户")
    @GetMapping("/ids/{ids}")
    public JSONResult findUsersWithOrgInIds(
            @ApiParam(value = "用户ID串", required = true)
            @PathVariable String ids){
        return wrap(User.findUsersWithOrgInIds(ids));
    }

    @ApiOperation(value = "修改用户")
    @PatchMapping
    public JSONResult update(
            @ApiParam(value = "用户对象", required = true)
            @RequestBody JSONObject obj) {
        // 获取已有对象并自动映射属性
        User user = (User) User.findUserById(obj.getString("id")).parse(obj.toJSONString());
        // 使用【聚合】更新对象： 聚合会包含特定的业务逻辑，如校验和处理了不可修改的属性等
        return wrap(User.update(user));
    }

    @ApiOperation(value = "修改用户密码")
    @PostMapping("/passchange/{id}")
    public JSONResult passChange(
            @ApiParam(value = "用户ID", required = true)
            @PathVariable String id,
            @ApiParam(value = "密码对象:{oldpass:原密码,newpass:新密码}", required = true)
            @RequestBody JSONObject passObj) {
        User.updatePassword(
                id,
                passObj.getString("oldpass"),
                passObj.getString("newpass")
        );
        return success();
    }
}
