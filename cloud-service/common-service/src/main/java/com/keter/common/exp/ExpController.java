package com.keter.common.exp;

import com.keter.common.base.BaseController;
import com.keter.common.domain.codelist.Codelist;
import com.keter.common.domain.org.Org;
import com.keter.common.domain.user.User;
import com.keter.framework.web.annotation.Sid;
import com.keter.framework.web.component.sequence.Sequencer;
import com.keter.framework.web.result.JSONResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;

/**
 * 小白鼠：各类科学实验用！
 */
@RestController
@RequestMapping("/exp")
public class ExpController extends BaseController {
	private static final Logger logger = LoggerFactory.getLogger(ExpController.class);

	@Value("${config.appid:1}")
	String appId;

	@Autowired
	private Sequencer sequencer;

	@GetMapping("/exp")
	public JSONResult exp(){
//		int i = 1/0;
		Codelist.ex();
		return wrap(Org.dao.findById("1"));
	}


	/**
	 * 泛型结果集测试
	 * @return
	 */
	@GetMapping("/data/object")
	public JSONResult dataObject(){
		return wrap(Org.dao.findById("1"));
	}

	@GetMapping("/data/array")
	public JSONResult dataArray(){
		return wrap(User.findAllUsers());
	}

	@GetMapping("/data/string")
	public JSONResult dataString(){
		return wrap("haha");
	}

	@ResponseBody
	@GetMapping("/logme/{n}")
	public String logme(@PathVariable(required = false) int n) {
		for (int i = 0; i < n; i++) {
			logger.info("logged me hahaha 测试本地日志写入开销较大时，同时启动多个服务实例是否影响性能！！测试本地日志写入开销较大时，同时启动多个服务实例是否影响性能！！测试本地日志写入开销较大时，同时启动多个服务实例是否影响性能！！测试本地日志写入开销较大时，同时启动多个服务实例是否影响性能！！测试本地日志写入开销较大时，同时启动多个服务实例是否影响性能！！,{}", appId);
		}
		return "i am logged!";
	}

	/**
	 * 测试框架自动添加的SID以及SID反解等功能
	 * @return
	 */

	@GetMapping(value = "/sid")
	public JSONResult sid(String message, @Sid Long sid) {
		logger.info("当前请求的sid:{} , msg:{}", sid,message);
		if(sid!=null) {
			logger.info("sid反解后的内容:{}", sequencer.decode(sid));
		}
		return  wrap(sid);
	}

	/**
	 * 错误模拟：用于验证框架的全局异常处理机制
	 * @return
	 */
	@GetMapping(value = "/errors")
	public String errorMock() {
		logger.info("我错了...");
		int i = 1/0;
		return "timeout done!";
	}

	/**
	 * 超时测试：用于验证熔断机制、RestTemplate超时配置是否生效等
	 * @return
	 * @throws InterruptedException
	 */
	@GetMapping(value = "/timeout")
	public String timeout() throws InterruptedException {
		logger.info("我开始超时...");
		Thread.sleep(125000);
		logger.info("我超完时了！");
		return "timeout done!";
	}

	@GetMapping(value = "/ff/{content}")
	public String firefox(@PathVariable String content) {
		logger.info("Firefox:{}",content);
		return  "firefox: "+content;
	}
	@GetMapping(value = "/ff/pages/{content}")
	public String firefoxPages(@PathVariable String content) {
		logger.info("适配多级目录测试：Firefox Pages {}",content);
		return  "firefox Pages: "+content;
	}

	@GetMapping(value = "/chrome/{content}")
	public String chrome(@PathVariable String content) {
		logger.info("Chrome:{}",content);
		return  "chrome: "+content;
	}

}
