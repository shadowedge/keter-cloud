package com.keter.common.mail;

import com.keter.framework.core.config.AvoidScan;
import com.keter.mail.MailTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

// 因为其他小伙伴使用的DomainBaseTest只扫描当前路径下的组件
@AvoidScan
@Service
// 自动注入了KeterMail组件，所以需要从单体测试中排除
public class Mail {

    @Autowired
    MailTemplate mailSender;

    // 仅用于单体测试环境验证
    public void haha(){

    }

    // 只有收件人(to)
    public void sendMail(String title, String html, List<String> to) throws Exception {
        sendMail(title, html, to, new ArrayList<>(), new ArrayList<>());
    }

    // to cc
    public void sendMail(String title, String html, List<String> to, List<String> cc) throws Exception {
        sendMail(title, html, to, cc, new ArrayList<>());
    }

    // to cc bcc
    public void sendMail(String title, String html, List<String> to, List<String> cc, List<String> bcc) throws Exception {
        sendMail(title, html, to, cc, bcc, null);
    }

    // to cc bcc files
    public void sendMail(String title, String text, List<String> to, List<String> cc, List<String> bcc, List<File> files) throws Exception {
        mailSender.sendMail(title,text,to,cc,bcc,files);
    }
}