package com.keter.common.mail;

import com.alibaba.fastjson.JSONObject;
import com.keter.common.base.BaseController;
import com.keter.framework.core.config.AvoidScan;
import com.keter.framework.web.result.JSONResult;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *  用户管理
 */
@AvoidScan
@RestController
@RequestMapping("/api/v1/mails")
public class MailController extends BaseController {

    private static final Logger logger = LoggerFactory.getLogger(MailController.class);
    @Autowired
    Mail mail;

    // 仅用于单体测试环境验证
    public void haha(){

    }

    @ApiOperation(value = "发送邮件")
    @PostMapping
    public JSONResult sendMail(
            @ApiParam(value = "JSON格式的邮件实体模型，样例:\n" +
                    "  <pre>{" +
                    "  \"title\":\"邮件标题\",\n" +
                    "  \"to\":[\"gulixing@msn.com\",\"xxx@keter.com\"],\n" +
                    "  \"cc\":[\"gulixing@msn.com\",\"yyy@keter.com\"],\n" +
                    "  \"bcc\":[\"gulixing@msn.com\",\"zzz@keter.com\"],\n" +
                    "  \"html\":\"邮件内容，支持HTML格式标签，如：a<b>hehe</b>c\",\n" +
                    " }</pre>", required = true)
            @RequestBody JSONObject obj) throws Exception {
        //同时包含抄送和暗送
        if(obj.containsKey("cc") && obj.containsKey("bcc")) {
            logger.info("mail obj:{}",obj);
            mail.sendMail(
                    obj.getString("title"),
                    obj.getString("html"),
                    obj.getJSONArray("to").toJavaList(String.class),
                    obj.getJSONArray("cc").toJavaList(String.class),
                    obj.getJSONArray("bcc").toJavaList(String.class)
            );
        }
        //只有抄送
        else if(obj.containsKey("cc") && !obj.containsKey("bcc")) {
            logger.info("mail obj:{}",obj);
            mail.sendMail(
                    obj.getString("title"),
                    obj.getString("html"),
                    obj.getJSONArray("to").toJavaList(String.class),
                    obj.getJSONArray("cc").toJavaList(String.class),
                    null
            );
        }
        //只有暗送
        else if(obj.containsKey("bcc") && !obj.containsKey("cc")) {
            logger.info("mail obj:{}",obj);
            mail.sendMail(
                    obj.getString("title"),
                    obj.getString("html"),
                    obj.getJSONArray("to").toJavaList(String.class),
                    null,
                    obj.getJSONArray("bcc").toJavaList(String.class)
            );
        }
        //只有to
        else{
            mail.sendMail(
                    obj.getString("title"),
                    obj.getString("html"),
                    obj.getJSONArray("to").toJavaList(String.class)
            );
        }
//         return wrap("success");
         return success();
    }

}
