package com.keter.common.swagger;

import com.keter.framework.core.config.AvoidScan;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@AvoidScan //从单体测试排除：但扫描和创建REST其实并不是在Autowired里面做的！
@Configuration
@EnableSwagger2
public class SwaggerConfig {
    @Value("${spring.profiles.active}") String profile;
    @Bean
    public Docket api() {
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.any())
//                .paths(or(regex("/api/.*")))//过滤的接口
                .build()
                ;
    }
}