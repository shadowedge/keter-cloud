package com.keter.common.swagger;

import com.keter.common.base.BaseController;
import com.keter.framework.core.config.AvoidScan;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
@AvoidScan
public class SwaggerController extends BaseController {
	private static final Logger logger = LoggerFactory.getLogger(SwaggerController.class);
	@GetMapping("/")
	public String home() {
		return "redirect:/swagger-ui.html";
	}

}
