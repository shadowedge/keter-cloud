package com.keter.common.util;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.io.Serializable;

/**
 * 处理密码加密
 */
public class PasswordEncoderUtil implements Serializable {

    static PasswordEncoder encoder = new BCryptPasswordEncoder(10);

    public static String encode(String rawPassword) {
        return encoder.encode(rawPassword);
    }

    public static boolean  matches(CharSequence rawPassword, String encodedPassword) {
        return encoder.matches(rawPassword, encodedPassword);
    }

    public static void main(String[] args) {
        System.out.println(PasswordEncoderUtil.encode("1"));
    }
  
}

