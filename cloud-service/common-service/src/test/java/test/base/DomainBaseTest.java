package test.base;

import com.alibaba.fastjson.JSONObject;
import com.jfinal.ext.proxy.CglibProxyFactory;
import com.jfinal.proxy.ProxyManager;
import com.keter.common.domain.org.Org;
import com.keter.common.domain.user.User;
import com.keter.framework.core.config.AvoidScan;
import com.keter.framework.test.base.AbstractBaseTest;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.FilterType;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * 用于测试领域对象：
 * 只要无特殊框架组件引用
 * Created by gulixing@msn.com on 2018/11/27.
 */
//@RunWith(SpringRunner.class)
public class DomainBaseTest extends WebBaseTest {
    private static final Logger logger = LoggerFactory.getLogger(DomainBaseTest.class);

    @Before
    public void mock(){
        // 避免JDK11出现兼容性问题
        ProxyManager.me().setProxyFactory(new CglibProxyFactory());
        MockitoAnnotations.initMocks(this);
    }

    protected User mockNewUser(String username){
        User user = new User();
        user.put("username",username);
        user.put("password","1");
        user.put("status",User.STATUS_NORMAL);
        user.put("name","test-user");
        user.put("mobile","13800138000");
        user.put("email","test-mail@test.com");
        user.put("remark","unittest");
        user.put("role_id",User.ROLE_DEFAULT_ID);
        user.put("org_id","999");
        return user;
    }

    protected JSONObject mockNewUserObject(String username){
        User user = mockNewUser(username);
        return JSONObject.parseObject(user.toJson());
    }

    protected Org mockNewOrg(String name){
        Org org = new Org();
        org.put("name",name);
        org.put("address","机构地址");
        org.put("contact","联系人名称");
        org.put("contact_phone","0248366666");
        org.put("contact_mobile","13800138000");
        org.put("email","test@test.com");
        org.put("status",Org.STATUS_NORMAL);
        org.put("type",Org.TYPE_TECHNICAL);
        org.put("remark","unittest");
        return org;
    }

    protected JSONObject mockNewOrgObject(String name){
        Org org = mockNewOrg(name);
        return JSONObject.parseObject(org.toJson());
    }
}
