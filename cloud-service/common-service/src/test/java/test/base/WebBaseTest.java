package test.base;

import com.keter.CommonServiceStart;
import com.keter.framework.test.base.AbstractBaseTest;
import com.keter.framework.web.component.sequence.SIDHolder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Import;

/**
 *  WEB运行环境测试基类:
 *  启用完整的Web环境，适用于全部领域对象的测试，但运行开销较大
 */
@SpringBootTest(classes = WebBaseTest.class)
@Import(CommonServiceStart.class)
public  class WebBaseTest extends AbstractBaseTest {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = LoggerFactory.getLogger(WebBaseTest.class);

	@Autowired
	protected SIDHolder sidHolder;

	/**
	 * 为目标Controller模拟SID
	 */
	protected Long mockSid(){
		return sidHolder.getOrGenerate();
	}
}