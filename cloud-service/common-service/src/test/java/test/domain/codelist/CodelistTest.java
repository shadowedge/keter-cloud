package test.domain.codelist;

import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Record;
import com.keter.common.domain.codelist.Codelist;
import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import test.base.DomainBaseTest;
import test.base.WebBaseTest;

import java.util.List;

@EnableAutoConfiguration
public class CodelistTest extends DomainBaseTest {
	/**
	* Logger for this class
	*/
	private static final Logger logger = LoggerFactory.getLogger(CodelistTest.class);

	@Test
	public void findByKind(){
		logger.debug("我是调试日志");
		List<Codelist> list  = Codelist.findByKind("sex");
		logger.info("list:{}",list);
		Assert.assertEquals("1",list.get(0).get("value"));
		Assert.assertEquals("男",list.get(0).get("name"));
	}

	@Test
	public void findInParam(){
		List<Record> list = Db.find("select * from codelist where id in (?,?)",'1','2');
		logger.info("res:{}",list.size());
		Assert.assertEquals(2,list.size());
	}

}
