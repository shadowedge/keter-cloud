package test.domain.org;

import com.alibaba.fastjson.JSONObject;
import com.jfinal.plugin.activerecord.Db;
import com.keter.common.domain.org.OrgController;
import com.keter.framework.web.result.JSONResult;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import test.base.DomainBaseTest;

public class OrgControllerTest extends DomainBaseTest {
	/**
	* Logger for this class
	*/
	private static final Logger logger = LoggerFactory.getLogger(OrgControllerTest.class);

	@Autowired
	OrgController controller;

	@Test
	public void  findAll(){
		JSONResult res = controller.findAll();
		logger.info("res:{}",res);
	}

	@Test
	public void  findById(){
		JSONResult res = controller.findById("1");
		logger.info("res:{}",res);
		Assert.assertEquals("总部", res.string("name"));
	}

	@Test
	public void  findInIds(){
		JSONResult res = controller.findInIds("1,2,3");
//		logger.info("res:{}",res.array());
		logger.info("res:{}",res.string());
	}

	@Test
	public void  findUsersById(){
		JSONResult res = controller.findUsersById("1");
		logger.info("res:{}",res.array());
		Assert.assertEquals("管理员", res.array().getJSONObject(0).get("name"));

		res = controller.findUsersById("9999");
		logger.info("res:{}",res.array());
		Assert.assertEquals(0, res.array().size());
	}

	@Test
	public void  add(){
		Db.update("delete from org where name like '%test-service-controller-%'");
		String name = "test-service-controller-"+ RandomStringUtils.randomAlphabetic(5);
		JSONResult res = controller.add(mockNewOrgObject(name));
		logger.info("res:{}",res);
		JSONObject org = controller.findById(res.string("id")).data();
		Assert.assertEquals(name, org.getString("name"));
	}

	@Test
	public void  addUser(){
		clean();
		controller.addUsers("999","1,2,3");
		JSONResult res  = controller.findUsersById("999");
		logger.info("res:{}",res.string());
		Assert.assertEquals("管理员",res.array().getJSONObject(0).get("name"));
	}

	/**
	 *  清理关系表，以防主键冲突导致测试无法重复执行
	 */
	private void clean(){
		Db.update("delete from user_org where org_id=999");
	}

}
