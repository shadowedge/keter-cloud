package test.domain.org;

import com.jfinal.plugin.activerecord.Db;
import com.keter.common.domain.org.Org;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import test.base.DomainBaseTest;
import test.base.WebBaseTest;

import java.util.List;

public class OrgTest extends WebBaseTest {
	/**
	* Logger for this class
	*/

	private static final Logger logger = LoggerFactory.getLogger(OrgTest.class);

	@Test
	public void findAll(){
		List<Org> list  = Org.dao.findAll();
		logger.info("list:{}",list);
		Assert.assertEquals("总部",list.get(0).get("name"));
	}

	@Test
	public void findByID(){
		Org.dao.findById(1);
	}

	protected Org mockNewOrg(String name){
		Org org = new Org();
		org.put("name",name);
		org.put("address","机构地址");
		org.put("contact","联系人名称");
		org.put("contact_phone","0248366666");
		org.put("contact_mobile","13800138000");
		org.put("email","test@test.como");
		org.put("status",Org.STATUS_NORMAL);
		org.put("type",Org.TYPE_TECHNICAL);
		org.put("remark","unittest");
		return org;
	}

//	@Test
	public void txMock() {
		Db.update("delete from org where name like 'test-model-%'");
		String username1 = "test-model-"+ RandomStringUtils.randomAlphabetic(5);
		String username2 = "test-model-"+ RandomStringUtils.randomAlphabetic(5);
		Org org1 = mockNewOrg(username1);
		Org org2 = mockNewOrg(username2);
		logger.info("{},{}",username1,username2);
		try {
			Org.daox.saveTx2(org1,org2);
		} catch (Exception e) {
			logger.error("模拟事务异常：");
			logger.info("验证事务是否回滚， org1 id:{}", org1.getID());
			logger.info("org1:{}", Org.dao.findById(org1.getID()));
			Assert.assertTrue(Org.dao.findById(org1.getID())==null);
		}
	}
}
