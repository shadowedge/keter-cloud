package test.domain.user;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.jfinal.plugin.activerecord.Db;
import com.keter.common.domain.org.OrgController;
import com.keter.common.domain.user.User;
import com.keter.common.domain.user.UserController;
import com.keter.common.util.PasswordEncoderUtil;
import com.keter.framework.core.exception.ValidateException;
import com.keter.framework.web.result.JSONResult;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.RandomUtils;
import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import test.base.DomainBaseTest;

public class UserControllerTest extends DomainBaseTest {
	/**
	* Logger for this class
	*/
	private static final Logger logger = LoggerFactory.getLogger(UserControllerTest.class);

	@Autowired
	UserController controller;

	@Autowired
	OrgController orgController;

	@Test
	public void findAll(){
		JSONArray array = controller.findAll().data();
		logger.info("res:{}",array);
		Assert.assertEquals("管理员", array.getJSONObject(0).get("name"));
	}

	@Test
	public void findById(){
		JSONObject res = controller.findById("1").data();
		logger.info("res:{}",res);
		Assert.assertEquals("admin", res.get("username"));
	}

	@Test
	public void  add(){
		// 清除先前测试遗留数据
		Db.update("delete from user where username like '%test-controller-%'");
		String username = "test-controller-"+RandomUtils.nextLong();
		JSONObject obj = mockNewUserObject(username);
		JSONResult result = controller.add(obj);
		logger.info("user add:{}",result.string());
		JSONObject user = controller.findById(result.string("id")).data();
		Assert.assertEquals(username, user.getString("username"));
		Assert.assertEquals(User.STATUS_NORMAL, user.getString("status"));
		// 验证用户是否正确加入到99组织
		JSONArray arr = orgController.findUsersById("999").array();
		logger.info("arr:{}",arr);
		Assert.assertTrue(arr.toJSONString().contains(username));
		// 清除本次测试产生数据
		Db.update("delete from user_role where role_id='99' and user_id=?",user.get("id"));
	}

	@Test
	public void  update(){
		String remark = RandomStringUtils.randomAlphabetic(5);
		JSONObject obj = new JSONObject();
		obj.put("id","99");
		obj.put("name","test-user-update-普通的Disco我们普通的摇~~~");
		obj.put("remark",remark);
		controller.update(obj);
		JSONObject userFind = controller.findById("99").data();
		logger.info("user updated:{}",userFind);
		Assert.assertEquals(remark, userFind.getString("remark"));
	}

//	@Test
	public void  passwordChange(){
		JSONObject obj = new JSONObject();
		obj.put("oldpass","1");
		obj.put("newpass","99");
		controller.passChange("99",obj);
		JSONObject userFind = controller.findById("99").data();
		logger.info("user with newPass: {}",userFind);
		Assert.assertTrue(PasswordEncoderUtil.matches("99",userFind.getString("password")));

		logger.info("改回默认密码，防止后续测试失败:");
		obj.put("oldpass","99");
		obj.put("newpass","1");
		controller.passChange("99",obj);
		userFind = controller.findById("99").data();
		Assert.assertTrue(PasswordEncoderUtil.matches("1",userFind.getString("password")));

		logger.info("原密码输入错误测试...");
		obj.put("oldpass","error");
		obj.put("newpass","1");
		try {
			controller.passChange("99",obj);
		} catch (ValidateException e) {
			Assert.assertTrue(e.getMessage().contains("密码输入错误"));
		}
	}


	@Test
	public void findOrgById(){
		JSONObject userFind = controller.findOrgByUserId("1");
		logger.info("org ids:{}",userFind);
//		 userFind = controller.findOrgByUserId("2");
//		logger.info("org ids:{}",userFind);
	}

}
