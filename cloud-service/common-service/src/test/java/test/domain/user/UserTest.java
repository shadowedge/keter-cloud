package test.domain.user;

import com.alibaba.fastjson.JSONArray;
import com.jfinal.kit.JsonKit;
import com.jfinal.plugin.activerecord.Db;
import com.keter.common.domain.user.User;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import test.base.DomainBaseTest;

import java.util.List;

public class UserTest  extends DomainBaseTest {
	/**
	* Logger for this class
	*/
	private static final Logger logger = LoggerFactory.getLogger(UserTest.class);

//	@Test
	public void findAll(){
		List<User> users = User.findAllUsers();
		Assert.assertEquals(1,users.get(0).getID("id"));
		Assert.assertEquals("admin",users.get(0).get("username"));
	}

	@Test
	public void findByUsername(){
		User user = User.findByUsername("admin");
		logger.info("authorities:{}", JsonKit.toJson(user.get("authorities")));
		Assert.assertEquals("admin",user.get("username"));
	}

	@Test
	public void add(){
		//确保多次测试后只残留一条数据
		Db.update("delete from user where username like '%test-model-%'");
		String username = "test-model-"+ RandomStringUtils.randomAlphabetic(10);
		User user = mockNewUser(username);
		user.save();
		User result = User.findByUsername(username);
		logger.info("find user:{}",result);
		Assert.assertEquals(username,result.getStr("username"));
		Assert.assertEquals(User.STATUS_NORMAL,result.getStr("status"));
		JSONArray roles = result.get("authorities");
		logger.info("roles:{}",roles);
		Assert.assertEquals("ROLE_USER",roles.get(0));
	}

	@Test
	public void update(){
		String rand = RandomStringUtils.randomAlphabetic(5);
		User user = User.findUserById("99");
		user.set("remark",rand);
		user.set("name",rand);
		user.set("password","1");
		User.update(user);
		Assert.assertEquals(rand,User.findUserById("99").get("remark"));
		Assert.assertEquals(rand,User.findUserById("99").get("name"));
		Assert.assertNotEquals("1",User.findUserById("99").getStr("password"));

		try {
			user.set("password","1");
			User.update(user);
		} catch (Exception e) {
			logger.error("", e);
			e.printStackTrace();
		}

	}
}
