package test.exp;

import com.keter.common.exp.ConsulAPIExpController;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import test.base.WebBaseTest;

public class ExpConsulAPIControllerTest extends WebBaseTest {
	/**
	* Logger for this class
	*/
	private static final Logger logger = LoggerFactory.getLogger(ExpConsulAPIControllerTest.class);

	@Autowired
	ConsulAPIExpController controller;

	@Test
	public void  clean(){
		controller.cheakCleanService();
	}

}
