package test.exp;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.keter.common.exp.ExpController;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import test.base.WebBaseTest;

public class ExpControllerTest extends WebBaseTest {
	/**
	* Logger for this class
	*/
	private static final Logger logger = LoggerFactory.getLogger(ExpControllerTest.class);

	@Autowired
	ExpController controller;

	@Test
	public void  sid(){
		logger.info("res:{}",controller.sid("SID测试",mockSid()));
	}

//	@Test
//	public void  sid2(){
//		Long sid = mockSid(controller);
//		logger.info("res:{}",controller.sid2(sid));
//	}

	@Test
	public void  dataObject(){
		JSONObject org = controller.dataObject().data();
		logger.info("org:{}",org);
	}

	@Test
	public void  dataArray(){
		JSONArray users = controller.dataArray().data();
		logger.info("users:{}",users.size());
	}

	@Test
	public void  dataString(){
		String str = controller.dataString().data();
		logger.info("str:{}",str);
	}
}
