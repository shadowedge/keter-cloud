package test.exp;

import com.keter.framework.core.util.IOUtil;
import okhttp3.OkHttpClient;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.config.Registry;
import org.apache.http.config.RegistryBuilder;
import org.apache.http.conn.socket.ConnectionSocketFactory;
import org.apache.http.conn.socket.PlainConnectionSocketFactory;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.http.client.OkHttp3ClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

/**
 * Created by gulx@neusoft.com on 2019/12/6.
 */
public class ExpGzipRestTest {
    private static final Logger logger = LoggerFactory.getLogger(ExpGzipRestTest.class);

    @Test //OK!
    public void gzipISOWithSimpleRestTemplate() throws IOException {
        RestTemplate template = new RestTemplate();
        HttpEntity<String> response = template.exchange("http://127.0.0.1:9990/exp/gzip", HttpMethod.GET, getGzipHttpEntity(), String.class);
        Assert.assertTrue(response.getHeaders().get("Content-Encoding").contains("gzip"));
        logger.info("response body:{}", response.getBody());
        logger.info("response content:{}", IOUtil.decompressGzip(response.getBody()));
        Assert.assertTrue(IOUtil.decompressGzip(response.getBody()).contains("哈哈"));
    }

    @Test //OK!
    public void gzipISOWithOKHttp() throws IOException {
        RestTemplate template = okHttpTemplate();
        HttpEntity<String> response = template.exchange("http://127.0.0.1:9990/exp/gzip", HttpMethod.GET, getGzipHttpEntity(), String.class);
        Assert.assertTrue(response.getHeaders().get("Content-Encoding").contains("gzip"));
        logger.info("response body:{}", response.getBody());
        logger.info("response content:{}", IOUtil.decompressGzip(response.getBody()));
        Assert.assertTrue(IOUtil.decompressGzip(response.getBody()).contains("哈哈"));
    }

    @Test //Failed: GZIPInputStream无法对utf8字符集正确解码
    public void gzipu8WithSimpleTemplate() throws IOException {
        RestTemplate template = new RestTemplate();
        HttpEntity<String> response = template.exchange("http://127.0.0.1:9990/exp/gzipu8", HttpMethod.GET, getGzipHttpEntity(), String.class);
        logger.info("response headers:{}",response.getHeaders());
        Assert.assertTrue(response.getHeaders().get("Content-Encoding").contains("gzip"));
        logger.info("response content:{}", response.getBody());
        // 由于字符集原因无法被正确解码
        Assert.assertTrue(IOUtil.decompressGzip(response.getBody()).contains("哈哈"));
    }

    /**
     * 服务端自动返回gzip/utf8内容:
     * 浏览器访问功能一切正常
     * 服务端不对基于HttpClient发起的请求进行自动化gzip压缩，原因不明！
     * @throws IOException
     */
    @Test //Failed: HttpClient无法使服务端gzip请求自动生效！
    public void gzipu8WithHttpClient() throws IOException {
        RestTemplate template = httpClientTemplate();
        HttpEntity<String> response = template.exchange("http://127.0.0.1:9990/exp/gzipu8", HttpMethod.GET, getGzipHttpEntity(), String.class);
        logger.info("response headers:{}",response.getHeaders());
        Assert.assertTrue(response.getHeaders().get("Content-Encoding").contains("gzip"));
        logger.info("response content:{}", response.getBody());
        Assert.assertTrue(response.getBody().contains("哈哈"));
    }

    private HttpEntity<?> getGzipHttpEntity() {
        HttpHeaders headers = new HttpHeaders();
        headers.add(HttpHeaders.ACCEPT_ENCODING, "gzip");
        // 在使用GZIP进行压缩和解压时，如果压缩后的字节数组在传输过程中发生改变就会导致此异常的发生，
        // 所以如果不是直接传输压缩后的字节数组而是字符串时，
        // 在转换为字符串时，一定要使用ISO-8859-1这样的单字节编码，
        // 否则在将字符串转换为字节数组时会导致节数组产生变化，从而产生该异常。
        return new HttpEntity<>(null, headers);
    }

    private HttpEntity<?> getGzipHttpEntity2() {
        HttpHeaders headers = new HttpHeaders();
        MediaType type = MediaType.parseMediaType("text/compressed; charset=ISO-8859-1");
        headers.setContentType(type);
        headers.add(HttpHeaders.ACCEPT_ENCODING, "gzip");
        return new HttpEntity<>(null, headers);
    }

    public  RestTemplate httpClientTemplate(){
        // 使用HttpComponent构造restTemplate客户端
        HttpComponentsClientHttpRequestFactory factory = new HttpComponentsClientHttpRequestFactory(httpClient());
        // 设置超时时间
        factory.setConnectTimeout(2000);
        factory.setReadTimeout(1000 * 2);
        return new RestTemplate(factory);
    }

    public  RestTemplate okHttpTemplate(){
        OkHttpClient client = new OkHttpClient.Builder()
                .connectTimeout(5, TimeUnit.SECONDS)      //设置连接超时
                .readTimeout(10, TimeUnit.SECONDS)         //设置读超时
                .writeTimeout(60, TimeUnit.SECONDS)        //设置写超时
                .retryOnConnectionFailure(true)            //是否自动重连
                .build();
        // 使用OkHttp3构造restTemplate客户端
        RestTemplate restTemplate = new RestTemplate(new OkHttp3ClientHttpRequestFactory(client));
        return restTemplate;
    }

    private HttpClient httpClient() {
        Registry<ConnectionSocketFactory> registry = RegistryBuilder.<ConnectionSocketFactory>create()
                .register("http", PlainConnectionSocketFactory.getSocketFactory())
                .register("https", SSLConnectionSocketFactory.getSocketFactory())
                .build();
        PoolingHttpClientConnectionManager connectionManager = new PoolingHttpClientConnectionManager(registry);
        //设置整个连接池最大连接数 根据自己的场景决定
        connectionManager.setMaxTotal(200);
        //路由是对maxTotal的细分
        connectionManager.setDefaultMaxPerRoute(50);
        RequestConfig requestConfig = RequestConfig.custom()
                .setConnectTimeout(2000)//连接上服务器(握手成功)的时间，超出该时间抛出connect timeout
                .setSocketTimeout(2000) //服务器返回数据(response)的时间，超过该时间抛出read timeout
                .setConnectionRequestTimeout(5000)//从连接池中获取连接的超时时间，超过该时间未拿到可用连接，会抛出org.apache.http.conn.ConnectionPoolTimeoutException: Timeout waiting for connection from pool
                .build();
        return HttpClientBuilder.create()
                .setDefaultRequestConfig(requestConfig)
                .setConnectionManager(connectionManager)
                .build();
    }


}
