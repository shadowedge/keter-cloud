package test.exp;

import com.alibaba.fastjson.JSON;
import com.google.common.collect.Maps;
import com.jfinal.kit.JsonKit;
import org.assertj.core.util.Lists;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.LocalDateTime;
import java.time.temporal.ChronoField;
import java.util.List;
import java.util.Map;

public class ExpJsonPerfTest {
	/**
	* Logger for this class
	*/
	private static final Logger logger = LoggerFactory.getLogger(ExpJsonPerfTest.class);

	List list = Lists.newArrayList();
	Map a,b;

	@Before
	public void  init(){
		a = Maps.newHashMap();
        a.put("id",1);
        a.put("name","haha");
        b = Maps.newHashMap();

        b.put("id",2);
        b.put("name","hehe");
		list.add(a);
		list.add(b);
	}

	@Test
	public void  object(){
		int count = 10000;
		fastjsonObject(count,a);
		jsonkitObject(count,a);
	}

	@Test
	public void  list(){
		int count = 10000;
		jsonkitList(count);
		fastjsonList(count);
	}

	private void jsonkitObject(int count,Object obj) {
		System.out.println("fast json begin");
		Long time = LocalDateTime.now().getLong(ChronoField.MICRO_OF_DAY);
		for (int i = 0; i < count; i++) {
			JsonKit.toJson(obj);
		}
		System.out.println(LocalDateTime.now().getLong(ChronoField.MICRO_OF_DAY)-time);
	}

	private void fastjsonObject(int count, Object obj) {
		System.out.println("jsonkit json begin");
		Long time = LocalDateTime.now().getLong(ChronoField.MICRO_OF_DAY);
		for (int i = 0; i < count; i++) {
			JSON.toJSONString(obj);
		}
		System.out.println(LocalDateTime.now().getLong(ChronoField.MICRO_OF_DAY)-time);
	}

	private void jsonkitList(int count) {
		System.out.println("fast json begin");
		Long time = LocalDateTime.now().getLong(ChronoField.MICRO_OF_DAY);
		for (int i = 0; i < count; i++) {
			JsonKit.toJson(list);
		}
		System.out.println(LocalDateTime.now().getLong(ChronoField.MICRO_OF_DAY)-time);
	}

	private void fastjsonList(int count) {
		System.out.println("jsonkit json begin");
		Long time = LocalDateTime.now().getLong(ChronoField.MICRO_OF_DAY);
		for (int i = 0; i < count; i++) {
			JSON.toJSONString(list);
		}
		System.out.println(LocalDateTime.now().getLong(ChronoField.MICRO_OF_DAY)-time);
	}


}
