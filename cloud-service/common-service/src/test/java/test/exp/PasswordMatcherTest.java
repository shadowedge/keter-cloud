package test.exp;

import com.keter.common.util.PasswordEncoderUtil;
import org.junit.Assert;

/**
 * 验证密码是否匹配
 * Created by gulixing@msn.com on 2019/3/15.
 */
public class PasswordMatcherTest {

//    @Test
    public void passMatch(){
        String enc = "$2a$10$OQPAqRiQOBL3ut1StAlFVOz9mA7L9k.qbe5kzbzlyBye5AQywoMfC";
        Assert.assertTrue(PasswordEncoderUtil.matches("1", enc));
        enc = "$2a$10$TMt8.pCRgLkgA9P4F956/OotQl7oU2J6.caBUBtVP2rwcjbAG4Yr2";
//        Assert.assertTrue(PasswordEncoderUtil.matches("99", enc));
    }

}
