package test.mail;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.keter.common.mail.MailController;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import test.base.WebBaseTest;

public class MailControllerTest extends WebBaseTest {
	/**
	* Logger for this class
	*/
	private static final Logger logger = LoggerFactory.getLogger(MailControllerTest.class);

	@Autowired
	MailController controller;

	@Test
	public void haha(){
		controller.haha();
	}

//	@Test
	public void  sendMail() throws Exception {
		JSONObject obj = new JSONObject();
		JSONArray to = new JSONArray();
		to.add("gulixing@msn.com");
		JSONArray cc = new JSONArray();
		cc.add("gulixing@msn.com");
		JSONArray bcc = new JSONArray();
		bcc.add("gulixing@msn.com");
		obj.put("title","haha");
		obj.put("html","a<b>hehe</b>c");
		obj.put("to",to);
		obj.put("cc",cc);
		obj.put("bcc",bcc);
		logger.info("mail obj:{}",obj);
		controller.sendMail(obj);
	}

//	@Test
	public void  sendMailBcc() throws Exception {
		JSONObject obj = new JSONObject();
		JSONArray to = new JSONArray();
		to.add("gulixing@msn.com");
		JSONArray bcc = new JSONArray();
		bcc.add("gulixing@msn.com");
		obj.put("title","测试暗送");
		obj.put("html","暗送给他人了！");
		obj.put("to",to);
		obj.put("bcc",bcc);
		logger.info("mail obj:{}",obj);
		controller.sendMail(obj);
	}

}
