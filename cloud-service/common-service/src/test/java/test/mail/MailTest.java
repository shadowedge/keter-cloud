package test.mail;

import com.keter.common.mail.Mail;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import test.base.DomainBaseTest;

import java.util.ArrayList;
import java.util.List;

//处理JavaMail自动注入问题
@ComponentScan("com.keter.common.domain.mail") //自行扫描组件，不排除特例
@EnableAutoConfiguration //必须启用，否则无法初始化bean
public class MailTest extends DomainBaseTest{
	/**
	* Logger for this class
	*/
	private static final Logger logger = LoggerFactory.getLogger(MailTest.class);

	@Autowired
	Mail mail;

	@Test
	public void haha(){
		mail.haha();
	}

//	@Test
	public void sendMail() throws Exception {
		List<String> to =  new ArrayList();
		to.add("gulixing@msn.com");

		List<String> cc =  new ArrayList();
		cc.add("gulixing@msn.com");

		java.io.File f1 = new java.io.File("e:/temp/omega.pem");
		java.io.File f2 = new java.io.File("e:/temp/cv.sql");
		List<java.io.File> files = new ArrayList<>();
		files.add(f1);
		files.add(f2);
		mail.sendMail("to", "a<br>text", to);
	}

}
