package test.security;

import com.alibaba.fastjson.JSONObject;
import com.keter.common.domain.user.UserController;
import com.keter.common.security.SecurityController;
import com.keter.framework.core.exception.ValidateException;
import com.keter.framework.web.result.JSONResult;
import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import test.base.WebBaseTest;

public class SecurityControllerTest extends WebBaseTest { //
	/**
	* Logger for this class
	*/
	private static final Logger logger = LoggerFactory.getLogger(SecurityControllerTest.class);

	@Autowired
	SecurityController controller;

	@Autowired
	UserController userController;

	@Test
	public void  tokenGenerate(){
		JSONResult res = genToken();
		logger.info("res:{}",res);
		logger.info("token :{}",res.data().toString());
		Assert.assertNotNull(res.data().toString());
	}

	// 全局异常处理（GlobalExcetionHandler）在单体测试环境下不生效
	@Test(expected = ValidateException.class)
	public void  tokenRefresh(){
		JSONResult result = controller.refresh(genToken().data());
		logger.info("token refreshed:{}",result.data().toString());
		controller.refresh("error token");
	}

	@Test
	public void  tokenRegenerate(){
		JSONResult result = controller.regenerate(genToken().string(),userController);
		logger.info("token regenerated:{}",result.data().toString());
		JSONResult user = controller.getUser(result.data().toString());
		logger.info("user:{}",user.object());
	}

	@Test
	public void  getUser(){
		JSONObject user = controller.findByUsername("admin").data();
		logger.info("roles:{}",user.get("authorities"));
		String token = genToken(user).data();
		JSONObject userOfToken = controller.getUser(token).data();
		logger.info("user of token:{}",userOfToken);
		Assert.assertEquals("admin",userOfToken.getString("username"));
	}

	@Test(expected = ValidateException.class)
	public void  getEmptyUser(){
		controller.findByUsername("nonExist");
	}

	private JSONResult genToken() {
		JSONObject user = new JSONObject();
		user.put("id", "1");
		user.put("username", "admin");
		user.put("authorities", "haha");
		return controller.generate(user.toJSONString());
	}

	private JSONResult genToken(JSONObject user) {
		return controller.generate(user.toJSONString());
	}

	@Test
	public void encodeMatch(){
		String enc = controller.encode("123").data();
		logger.info("enc:{}",enc);
		Assert.assertTrue(Boolean.valueOf(controller.matches("123",enc).data()));
	}
}
