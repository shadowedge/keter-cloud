package test.security;

import com.alibaba.fastjson.JSONObject;
import com.keter.common.domain.user.User;
import com.keter.common.security.Security;
import com.keter.framework.test.base.AbstractBaseTest;
import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.ComponentScan;
import test.base.DomainBaseTest;
import test.base.WebBaseTest;

public class SecurityTest extends WebBaseTest {
	/**
	* Logger for this class
	*/
	private static final Logger logger = LoggerFactory.getLogger(Security.class);

	@Autowired
	Security service;

	@Test
	public void generateAndGet() {
		String token = genToken();
		logger.info("token:{}",token);
        User result = service.getUser(token);
        logger.info("user:{}",result);
        Assert.assertEquals("admin",result.getStr("username"));
	}

	private String genToken() {
		JSONObject user = new JSONObject();
		user.put("id","1");
		user.put("username","admin");
		user.put("authorities","haha");
		user.put("email","haha@haha.com");
		return service.generate(user);
	}

}
