-- --------------------------------------------------------
-- Host:                         192.168.1.239
-- Server version:               5.6.15 - MySQL Community Server (GPL)
-- Server OS:                    Win32
-- HeidiSQL Version:             9.3.0.4984
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping database structure for dtx
CREATE DATABASE IF NOT EXISTS `dtx` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;
USE `dtx`;


-- Dumping structure for table dtx.account_tbl
CREATE TABLE IF NOT EXISTS `account_tbl` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` varchar(255) DEFAULT NULL,
  `money` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table dtx.account_tbl: ~0 rows (approximately)
DELETE FROM `account_tbl`;
/*!40000 ALTER TABLE `account_tbl` DISABLE KEYS */;
/*!40000 ALTER TABLE `account_tbl` ENABLE KEYS */;


-- Dumping structure for table dtx.action_one
CREATE TABLE IF NOT EXISTS `action_one` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- Dumping data for table dtx.action_one: ~0 rows (approximately)
DELETE FROM `action_one`;
/*!40000 ALTER TABLE `action_one` DISABLE KEYS */;
INSERT INTO `action_one` (`id`, `name`) VALUES
	(1, 'mKdwY');
/*!40000 ALTER TABLE `action_one` ENABLE KEYS */;


-- Dumping structure for table dtx.action_two
CREATE TABLE IF NOT EXISTS `action_two` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- Dumping data for table dtx.action_two: ~0 rows (approximately)
DELETE FROM `action_two`;
/*!40000 ALTER TABLE `action_two` DISABLE KEYS */;
INSERT INTO `action_two` (`id`, `name`) VALUES
	(1, 'mKdwY');
/*!40000 ALTER TABLE `action_two` ENABLE KEYS */;


-- Dumping structure for table dtx.order_tbl
CREATE TABLE IF NOT EXISTS `order_tbl` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` varchar(255) DEFAULT NULL,
  `commodity_code` varchar(255) DEFAULT NULL,
  `count` int(11) DEFAULT '0',
  `money` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table dtx.order_tbl: ~0 rows (approximately)
DELETE FROM `order_tbl`;
/*!40000 ALTER TABLE `order_tbl` DISABLE KEYS */;
/*!40000 ALTER TABLE `order_tbl` ENABLE KEYS */;


-- Dumping structure for table dtx.storage_tbl
CREATE TABLE IF NOT EXISTS `storage_tbl` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `commodity_code` varchar(255) DEFAULT NULL,
  `count` int(11) DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `commodity_code` (`commodity_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table dtx.storage_tbl: ~0 rows (approximately)
DELETE FROM `storage_tbl`;
/*!40000 ALTER TABLE `storage_tbl` DISABLE KEYS */;
/*!40000 ALTER TABLE `storage_tbl` ENABLE KEYS */;


-- Dumping structure for table dtx.undo_log
CREATE TABLE IF NOT EXISTS `undo_log` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `branch_id` bigint(20) NOT NULL,
  `xid` varchar(100) NOT NULL,
  `context` varchar(128) NOT NULL,
  `rollback_info` longblob NOT NULL,
  `log_status` int(11) NOT NULL,
  `log_created` datetime NOT NULL,
  `log_modified` datetime NOT NULL,
  `ext` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ux_undo_log` (`xid`,`branch_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table dtx.undo_log: ~0 rows (approximately)
DELETE FROM `undo_log`;
/*!40000 ALTER TABLE `undo_log` DISABLE KEYS */;
/*!40000 ALTER TABLE `undo_log` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
