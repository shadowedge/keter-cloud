package com.keter.dtx;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author jimin.jm@alibaba-inc.com
 * @date 2019/06/14
 */
@SpringBootApplication
public class DtxServiceStart {

    public static void main(String[] args) {
        SpringApplication.run(DtxServiceStart.class, args);
    }

}
