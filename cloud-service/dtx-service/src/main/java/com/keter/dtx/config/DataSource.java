package com.keter.dtx.config;

import com.jfinal.plugin.druid.DruidPlugin;
import com.keter.dtx.plugin.SeataDruidPlugin;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class DataSource {

	@Value("${db.url}")
	protected String dbUrl;

	@Value("${db.username:any}")
	protected String username;

	@Value("${db.password:any}")
	protected String password;

	/**
	 * 使用Seata提供的Druid代理初始化数据源
	 * @return 
	 * @author gulixing@msn.com
	 * @date   2017年1月8日
	 */
	@Bean
	public void initDB(){
		System.out.println("初始化数据源....");
		DruidPlugin druid = new DruidPlugin(dbUrl, username, password);
		druid = new SeataDruidPlugin(dbUrl, username, password);
		druid.start();
		System.out.println("数据源初始化完成！");
	}

}