package com.keter.dtx.config;

import com.jfinal.plugin.activerecord.Db;
import org.springframework.context.annotation.Configuration;

import javax.annotation.PostConstruct;

/**
 * @author jimin.jm@alibaba-inc.com
 * @date 2019/06/14
 */
@Configuration
public class InitData {

    /**
     * 初始化数据
     */
    @PostConstruct
    public void initData() {
        Db.update("delete from account_tbl");
        Db.update("delete from order_tbl");
        Db.update("delete from storage_tbl");
        Db.update("insert into account_tbl(user_id,money) values('U100000','10000') ");
        Db.update("insert into storage_tbl(commodity_code,count) values('C100000','200') ");
    }
}
