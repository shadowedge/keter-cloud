package com.keter.dtx.controller;

import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Record;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author jimin.jm@alibaba-inc.com
 * @date 2019/06/14
 */
@RestController
@RequestMapping("/exp")
public class ExpController {

    /**
     * 尝试处理反复执行测试占用连接数问题
     * 观察mysql参数：
     * show global variables like 'max_connections';
     * show global variables like 'wait_timeout';
     * show global variables like 'interactive_timeout';
     * 观察运行时参数：
     * show status like '%connections%';
     * show status like '%Aborted%';
     * 会出现connections > max_connections的情况，主要因为有Aborted存在。
     * 据测试不会直接出现too many connections问题，因为据说Aborted_clients不会占用实际可用连接资源
     * @return
     */
    @RequestMapping(value = "/datasource")
    public List<Record> datasource() {
        List<Record> list = Db.find("select * from action_two");
        return list;
    }

}
