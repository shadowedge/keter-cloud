package com.keter.dtx.service;

import com.jfinal.plugin.activerecord.Db;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

/**
 * @author jimin.jm@alibaba-inc.com
 * @date 2019/06/14
 */
@Service
public class AccountService {

    private static final Logger logger = LoggerFactory.getLogger(AccountService.class);
    public void reduce(String userId, int money) {
        logger.info("userid:{},money:{}",userId,money);
        Db.update("update account_tbl set money = money - ? where user_id = ?",money,userId);
    }
}
