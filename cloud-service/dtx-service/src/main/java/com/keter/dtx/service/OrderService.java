package com.keter.dtx.service;

import com.jfinal.plugin.activerecord.Db;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author jimin.jm@alibaba-inc.com
 * @date 2019/06/14
 */
@Service
public class OrderService {

    @Autowired
    private AccountService accountService;

    public void create(String userId, String commodityCode, Integer count) {
        int orderMoney = count * 100;
        Db.update("insert order_tbl(user_id,commodity_code,count,money) values(?,?,?,?)", userId, commodityCode, count, orderMoney);
        accountService.reduce(userId, orderMoney);
    }
}
