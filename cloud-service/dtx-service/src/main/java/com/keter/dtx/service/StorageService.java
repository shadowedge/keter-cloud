package com.keter.dtx.service;

import com.jfinal.plugin.activerecord.Db;
import org.springframework.stereotype.Service;

/**
 * @author jimin.jm@alibaba-inc.com
 * @date 2019/06/14
 */
@Service
public class StorageService {

    public void deduct(String commodityCode, int count) {
        Db.update("update storage_tbl set count = count - ? where commodity_code = ?",
            count, commodityCode);
    }
}
