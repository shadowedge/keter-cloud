package com.keter.dtx.service.tcc;

import io.seata.rm.tcc.api.BusinessActionContext;
import io.seata.rm.tcc.api.BusinessActionContextParameter;
import io.seata.rm.tcc.api.LocalTCC;
import io.seata.rm.tcc.api.TwoPhaseBusinessAction;

@LocalTCC
public interface TccActionOne {

    @TwoPhaseBusinessAction(name = "TccActionOne")
    boolean prepare(
            BusinessActionContext actionContext,
            @BusinessActionContextParameter(paramName = "id") int id,
            @BusinessActionContextParameter(paramName = "name") String name);

    boolean commit(BusinessActionContext actionContext);

    boolean rollback(BusinessActionContext actionContext);
}
