package com.keter.dtx.service.tcc;

import io.seata.rm.tcc.api.BusinessActionContext;
import io.seata.rm.tcc.api.BusinessActionContextParameter;
import io.seata.rm.tcc.api.LocalTCC;
import io.seata.rm.tcc.api.TwoPhaseBusinessAction;

/**
 * The interface Tcc action two.
 *
 * @author zhangsen
 */
@LocalTCC
public interface TccActionTwo {

    /**
     * Prepare boolean.
     *
     * @param actionContext the action context
     * @param name             the b
     * @return the boolean
     */
    @TwoPhaseBusinessAction(
            name = "TccActionTwo",
            commitMethod = "commit",
            rollbackMethod = "rollback")
     boolean prepare(BusinessActionContext actionContext,
                     @BusinessActionContextParameter(paramName = "name") String name);

    /**
     * Commit boolean.
     *
     * @param actionContext the action context
     * @return the boolean
     */
     boolean commit(BusinessActionContext actionContext);

    /**
     * Rollback boolean.
     *
     * @param actionContext the action context
     * @return the boolean
     */
     boolean rollback(BusinessActionContext actionContext);

}
