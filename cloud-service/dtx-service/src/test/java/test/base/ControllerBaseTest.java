package test.base;

import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.test.context.junit4.SpringRunner;

/**
 *  Controller测试基类:适用于Web环境的Controller测试
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = ControllerBaseTest.class,webEnvironment = SpringBootTest.WebEnvironment.NONE)
@ComponentScan(basePackages = {"com.keter.dtx"})
public  class ControllerBaseTest extends ServiceBaseTest {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = LoggerFactory.getLogger(ControllerBaseTest.class);

}
