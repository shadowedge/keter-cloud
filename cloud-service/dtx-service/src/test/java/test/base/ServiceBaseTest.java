package test.base;

import com.keter.framework.core.config.AvoidScan;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.FilterType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * Created by gulixing@msn.com on 2018/11/27.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = ServiceBaseTest.class, webEnvironment = SpringBootTest.WebEnvironment.NONE)
@ComponentScan(basePackages = {
    "com.keter.dtx.config",
    "com.keter.dtx.service"},
    //排除swagger
    excludeFilters = {@ComponentScan.Filter(type = FilterType.ANNOTATION, classes = AvoidScan.class)})
public class ServiceBaseTest{

}
