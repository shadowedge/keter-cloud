package test.exp;

import com.jfinal.plugin.activerecord.Db;
import com.keter.dtx.controller.TCCController;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.MockitoAnnotations;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import test.base.ControllerBaseTest;

public class TCCControllerTest extends ControllerBaseTest {
	/**
	* Logger for this class
	*/
	private static final Logger logger = LoggerFactory.getLogger(TCCControllerTest.class);

	@Autowired
	TCCController controller;

	@Before
	public  void mock(){
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void success(){
		String name = RandomStringUtils.randomAlphabetic(5);
		controller.success(name);
		logger.info("验证数据是否提成功");
		Assert.assertEquals(name, findNameOfActionOne());
	}

	@Test
	public void rollback(){
		String oldName = findNameOfActionOne();
		String name = RandomStringUtils.randomAlphabetic(5);
		try {
			controller.rollback(name);
		} catch (Exception e) {
			logger.error("error:{}", e.getMessage());
		}
		logger.info("验证数据是否正确回滚");
		//验证action_one的记录是否恢复原始内容
		Assert.assertEquals(oldName, findNameOfActionOne());
		//验证action_two的新增记录是否被删除
		Assert.assertNull(Db.findFirst("select name from action_two where name=?",name));
	}

	private String findNameOfActionOne(){
		return Db.findFirst("select name from action_one where id=1").getStr("name");
	}
}