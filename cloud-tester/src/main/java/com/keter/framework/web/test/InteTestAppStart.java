package com.keter.framework.web.test;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.context.ServletWebServerInitializedEvent;
import org.springframework.context.ApplicationListener;

/**
 * Spring Boot 应用的入口文件
 */
@SpringBootApplication
public class InteTestAppStart implements ApplicationListener<ServletWebServerInitializedEvent> {
    private static final Logger logger = LoggerFactory.getLogger(InteTestAppStart.class);

    public static void main(String[] args) {
        SpringApplication.run(InteTestAppStart.class, args);
        logger.info("carevault test service started!");
    }

    @Override
    public void onApplicationEvent(ServletWebServerInitializedEvent servletWebServerInitializedEvent) {
        System.out.println("server port : ["+servletWebServerInitializedEvent.getWebServer().getPort()+"]");
    }
}
