package test.base;

import com.alibaba.fastjson.JSONObject;
import com.keter.framework.web.result.JSONResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by gulixing@msn.com on 2018/11/27.
 */
public class BaseTest {
    private static final Logger logger = LoggerFactory.getLogger(BaseTest.class);


    /**
     * 将JSONResult生成的String反向解析成JSONResult
     * 通常用于REST测试或集成测试等
     * @param json
     * @return
     */
    protected JSONResult parseJSONResult(String json){
        JSONObject object = JSONObject.parseObject(json);
        JSONResult result = new JSONResult();
        result.put(JSONResult.KEY_STATUS, object.get(JSONResult.KEY_STATUS));
        result.put(JSONResult.KEY_DATA, object.get(JSONResult.KEY_DATA));
        result.put(JSONResult.KEY_CODE, object.get(JSONResult.KEY_CODE));
        result.put(JSONResult.KEY_ERROR, object.get(JSONResult.KEY_ERROR));
        result.put(JSONResult.KEY_CAUSE, object.get(JSONResult.KEY_CAUSE));
        return result.ensure();
    }
}
