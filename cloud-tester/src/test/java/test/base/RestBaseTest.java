package test.base;

import com.alibaba.fastjson.JSONObject;
import com.google.common.collect.Maps;
import com.google.common.net.MediaType;
import com.jfinal.kit.Base64Kit;
import com.jfinal.kit.PropKit;
import com.keter.framework.web.result.JSONResult;
import org.apache.commons.lang3.Validate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;

import static io.restassured.RestAssured.given;
import static io.restassured.path.json.JsonPath.from;

/**
 * Created by gulixing@msn.com on 2017/6/22.
 */
public class RestBaseTest extends BaseTest {

    private static final Logger logger = LoggerFactory.getLogger(RestBaseTest.class);

    protected static final String Context;

    static {
        PropKit.use("application.properties");
        System.out.println(PropKit.get("app.context"));
        Context = PropKit.get("app.context");
    }

    public Map utf8Headers(){
        Map headers = Maps.newHashMap();
        headers.put("Content-Type", MediaType.JSON_UTF_8.toString());
        return headers;
    }

    public Map jwtHeaders(String username , String password){
        Map headers = utf8Headers();
        headers.put("XAuthorization", "Bearer " + token(username, password));
        return headers;
    }

    public Map jwtHeaders(String token){
        Map headers = utf8Headers();
        headers.put("XAuthorization", "Bearer " + token);//Authorization
        return headers;
    }

    protected String token(String username, String password){
        Map headers = utf8Headers();
        String auth = given().headers(headers).body("{\"username\":\""+username+"\",\"password\":\""+password+"\"}").post(Context+"/authenticate").asString();
//        logger.info("auth:{}",auth);
        String token = from(auth).get("data");
        Validate.notEmpty(token,"认证失败!!！");
        return token;
    }

    /**
     * 结果集正确性校验
     * @param res
     */
    protected JSONResult validate(String res){
        return parseJSONResult(res);
    }

    protected String getFieldFromToken(String token,String filed) {
        String arr[] = token.split(".");
        String sub = Base64Kit.decodeToStr(arr[1]);
        JSONObject obj = JSONObject.parseObject(sub);
        return obj.getJSONObject("sub").getString(filed);
    }

}
