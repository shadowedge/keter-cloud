package test.framework;

import org.hamcrest.CoreMatchers;
import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import test.base.RestBaseTest;

import java.util.Map;

import static io.restassured.RestAssured.given;
import static io.restassured.path.json.JsonPath.from;
import static org.hamcrest.Matchers.equalTo;

public class AuthRestTest extends RestBaseTest {

    /**
     * Logger for this class
     */
    private static final Logger logger = LoggerFactory.getLogger(AuthRestTest.class);

    @Test
    public void meHys() {
        String me = Context + "/auth/me/hys";
        Map userHeaders = jwtHeaders("user", "1");
        String user = given().headers(userHeaders).get(me).asString();
        logger.info("res:{}",user);
        Assert.assertEquals("user", from(user).get("username"));
    }

}
