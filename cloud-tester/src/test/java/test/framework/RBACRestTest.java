package test.framework;

import org.hamcrest.CoreMatchers;
import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import test.base.RestBaseTest;

import java.util.Map;

import static io.restassured.RestAssured.given;
import static io.restassured.path.json.JsonPath.from;
import static org.hamcrest.Matchers.equalTo;

public class RBACRestTest extends RestBaseTest {

    /**
     * Logger for this class
     */
    private static final Logger logger = LoggerFactory.getLogger(RBACRestTest.class);

    @Test
    public void login() {
        Map headers = utf8Headers();
        given().headers(headers).body("{\"username\":\"admin\",\"password\":\"1\"}").
        when().post(Context + "/authenticate").
        then().body("status", equalTo("0"));
    }

    @Test
    public void loginError() {
        Map headers = utf8Headers();
        given().headers(headers).body("{\"username\":\"admin\",\"password\":\"xxx\"}").
        when().post(Context + "/authenticate").
        then().body("status", equalTo("1"));
    }

    @Test
    public void meAdmin() {
        String me = Context + "/auth/me";
        Map adminHeaders = jwtHeaders("admin", "1");
        String admin = given().headers(adminHeaders).get(me).asString();
        logger.info("admin:{}",admin);
        Assert.assertEquals("admin", from(admin).get("data.username"));
    }

    @Test
    public void meUser() {
        String me = Context + "/auth/me";
        Map userHeaders = jwtHeaders("user", "1");
        String user = given().headers(userHeaders).get(me).asString();
        Assert.assertEquals("user", from(user).get("data.username"));
    }

    @Test
    public void rbac() {
        logger.info("=====未登录访问受保护资源测试：======");
        given().
            when().get(Context + "/admin/haha").
            then().statusCode(401);

        logger.info("=====非法token访问受保护资源测试：======");
        given().headers(jwtHeaders("anytoken")).
                when().get(Context + "/admin/haha").
                then().statusCode(401);

        logger.info("=====合法授权访问受保护资源测试：======");
        given().headers(jwtHeaders("admin", "1")).
            when().get(Context + "/admin/haha").
            then().body(CoreMatchers.containsString("ok"));

        given().headers(jwtHeaders("user", "1")).
            when().get(Context + "/user/haha").
            then().body(CoreMatchers.containsString("ok"));

        logger.info("====认证用户越权访问测试：====");
        given().headers(jwtHeaders("user", "1")).
            when().get(Context + "/admin/haha").
            then().statusCode(403);
    }

    @Test
    public void refresh() {
        given().headers(jwtHeaders("admin", "1")).
                when().get(Context + "/tokenrefresh").
                then().statusCode(200);
        given().headers(jwtHeaders("user", "1")).
            when().get(Context + "/tokenrefresh").
            then().statusCode(200);
    }
}
