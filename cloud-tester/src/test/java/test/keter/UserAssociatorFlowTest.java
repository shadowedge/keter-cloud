package test.keter;

import com.alibaba.fastjson.JSONObject;
import com.google.common.collect.Maps;
import com.google.common.net.MediaType;
import com.keter.framework.web.result.JSONResult;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import test.base.RestBaseTest;

import java.util.Map;

import static io.restassured.RestAssured.given;
import static io.restassured.path.json.JsonPath.from;

/**
 * 会员和用户管理核心业务流程测试
 */
public class UserAssociatorFlowTest extends RestBaseTest {
    /**
     * Logger for this class
     */
    private static final Logger logger = LoggerFactory.getLogger(UserAssociatorFlowTest.class);
    String userId;
    //技术会员
    String techOrgId;
    //医疗会员
    String medOrgId;

    /**
     * 用户-会员管理全流程测试
     */
    @Test
    public void userOrgAll() {
        //管理员登录
        login();
        //管理员确认自身身份信息
        me();
        createUser();
        //创建技术会员
        createTechORG();
        //创建医疗会员
        createMedORG();
        //为技术会议添加新创建的用户
        addUserToORG();
        //查看会员名下的用户
        queryORGUsers();
    }

    private void login() {
        String url = Context + "/authenticate";
        logger.info("【登录】测试 - 服务地址：{}",url);
        logger.info("测试用户名：{}，密码：{}","admin",1);
        Map headers = Maps.newHashMap();
//        headers.put("Content-Type", MediaType.APPLICATION_JSON_UTF8_VALUE);
        headers.put("Content-Type", MediaType.JSON_UTF_8);
        given().headers(headers).
                body("{\"username\":\"admin\",\"password\":\"1\"}").
                when().post(url).
                then().statusCode(200);
        logger.info("登录成功！");
    }

    private void me() {
        String url = Context + "/auth/me";
        logger.info("【获取当前登录用户信息】测试 - 服务地址：{}",url);
        Map adminHeaders = jwtHeaders("admin", "1");

        String admin = given().headers(adminHeaders).get(url).asString();
        logger.info("当前登录用户信息:\n{}",admin);
        Assert.assertEquals("admin", from(admin).get("username"));
    }

    @Test
    public void createUser() {
        String url = Context + "/api/v1/admin/users";
        logger.info("【增加用户】测试 - 服务地址：{}",url);
        JSONObject user = new JSONObject();
        String username = "wuyue-"+ RandomStringUtils.randomAlphabetic(10);
        user.put("username",username);
        user.put("name","integration-test-"+RandomStringUtils.randomAlphabetic(5));
        user.put("email","gulixing@msn.com");
        user.put("remark","集成测试");
        user.put("role_id","99");
        user.put("org_id","999");
        logger.info("新用户：username=[{}], \n 完整信息：{}, ",username,user);
        String res = given().headers(jwtHeaders("admin", "1")).body(user).post(url).asString();
        JSONResult result = parseJSONResult(res);
        userId = result.object().getString("id");
        logger.info("用户增加完成，新用户ID={}",result.object().get("id"));
        url = url + "/" +userId;
        logger.info("查询用户: id=[{}] - 服务地址：{}",userId, url);
        res = given().headers(jwtHeaders("admin", "1")).get(url).asString();
        result = parseJSONResult(res);
        logger.info("根据ID查询到的用户，username=[{}]，\n 完整信息:{}\n 注意比对username是否与增加前的随机字符一致！",result.object().getString("username"),res);
        logger.info("=====注意比对查询到的username是否与增加前的随机字符一致！=======");
        Assert.assertEquals(username,  result.object().getString("username"));
    }

    @Test
    public void createTechORG(){
        String url = Context + "/api/v1/admin/orgs";
        logger.info("【技术会员建档】，访问url:{}\n",url);
        String name = "东软集团技术委员会-"+RandomStringUtils.randomAlphabetic(10);
        logger.info("会员名称:{}",name);
        JSONObject org = new JSONObject();
        org.put("name",name);
        org.put("address","机构地址");
        org.put("contact","武跃");
        org.put("contact_phone","0248366666");
        org.put("contact_mobile","13800138000");
        org.put("email","test@test.com");
        org.put("type","T");
        org.put("remark","集成测试");
        String res = given().headers(jwtHeaders("admin", "1")).body(org).post(url).asString();
        JSONResult result = parseJSONResult(res);
        techOrgId = result.object().getString("id");
        logger.info("新技术会员Id={}，会员信息：{}", techOrgId,res);
    }

    @Test
    public void createMedORG(){
        String url = Context + "/api/v1/admin/orgs";
        logger.info("【医疗会员建档】，访问url:{}\n",url);
        String name = "医大一 神内 -"+RandomStringUtils.randomAlphabetic(10);
        logger.info("会员名称:{}",name);
        JSONObject org = new JSONObject();
        org.put("name",name);
        org.put("address","机构地址");
        org.put("contact","李主任");
        org.put("contact_phone","0248366666");
        org.put("contact_mobile","13800138000");
        org.put("email","gulixing@msn.com");
        org.put("type","M");
        org.put("remark","集成测试");
        String res = given().headers(jwtHeaders("admin", "1")).body(org).post(url).asString();
        JSONResult result = parseJSONResult(res);
        medOrgId = result.object().getString("id");
        logger.info("新医疗会员Id={}，会员信息：{}", techOrgId,res);
    }

    @Test
    public void addUserToORG(){
        techOrgId = techOrgId ==null?"999": techOrgId;
        userId = userId==null?"44":userId;
        logger.info("将用户加入技术会员，用户ID:{}，会员ID:{},", techOrgId,userId);
        String url = Context + "/api/v1/admin/orgs/"+ techOrgId +"/users/"+userId;
        logger.info("访问url:{}",url);
        String res = given().headers(jwtHeaders("admin", "1")).get(url).asString();
        logger.info("添加完成:{}",res);
        Assert.assertTrue(parseJSONResult(res).isOK());
    }

    @Test
    public void queryORGUsers(){
        techOrgId = techOrgId ==null?"999": techOrgId;
        String url = Context + "/api/v1/admin/orgs/"+ techOrgId +"/users";
        logger.info("访问url:{}",url);
        String res = given().headers(jwtHeaders("admin", "1")).get(url).asString();
        logger.info("列出会员(ID={})下的全部用户：\n{}", techOrgId,res);
        logger.info("观察是否包含先前创建的用户。");
    }

}

