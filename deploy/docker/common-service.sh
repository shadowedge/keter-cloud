#!/bin/bash
APP_NAME=common-service
PORT=9990
docker stop ${APP_NAME}
docker rm ${APP_NAME}
docker run -d -p ${PORT}:${PORT} -v /sdb/keter/db:/db --name=${APP_NAME} keter/${APP_NAME} --server.port=${PORT} --db.url=jdbc:sqlite:/db/keter-app.db
docker ps|grep ${APP_NAME}