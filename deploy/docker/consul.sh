#!/bin/bash
echo  "Stopping Consul..."
docker stop consul 
#sleep 1s
echo "Removing Consul..."
docker rm consul
#sleep 1s
echo "Running Consul..."
docker run  --name consul  -d -p 8500:8500 -v /data/consul:/consul/data -e CONSUL_BIND_INTERFACE='eth0'  consul agent -server -bootstrap -ui -node=1 -client='0.0.0.0'
