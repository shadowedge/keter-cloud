#!/usr/bin/perl
use strict;
use warnings;
use 5.010;
$, = "\t";
$\ = "\n";

# my $dir = getcwd;
my $dir = $ENV{'PWD'};
# 定义制品名称
my @apps = qw(
    common-service
    asset-service
    device-service
    project-service
    statistic-service
    carevault-portal
);

# 定义制品版本
my $version = "latest";

# 无参数执行时，部署全部app的最新版本
if (int(@ARGV) == 0) {
    print "待部署程序：";
    foreach (@apps) {
        my $app = $_ . "-" . $version . ".jar";
        print $app;
    }
    print "确定部署上述程序？(回车键确认)";
    while (<STDIN>) {
        if (length($_) == 1 and chomp($_)) {
            foreach (@apps) {
                my $app = $_ . "-" . $version . ".jar";
                print "部署: " . $dir ."/". $app;
                qx/scp -r $dir\/$app root\@cv-prod:\/deploy\//;
            }
        }
        else {
            print "取消部署！";
        }
        last;
    }
}

# 单参数：部署特定的程序
if (int(@ARGV) == 1) {
    print "确定部署程序:" . $ARGV[0] . "?(回车键确认)";
    while (<STDIN>) {
        if (length($_) == 1 and chomp($_)) {
            my $app = $ARGV[0];
            print "部署: " . $dir ."/". $app;
            qx/scp -r  $dir\/$app root\@cv-prod:\/deploy\//;
        }
        else {
            print "取消部署！";
        }
        last;
    }
}
