#! /bin/sh
# 系统IP地址
hostIp(){
	echo $(ip addr | grep 'state UP' -A2 | tail -n1 | awk '{print $2}' | cut -f1 -d '/')
}
# 服务标识
appId(){
	echo $(cat /proc/sys/kernel/random/uuid| cut -f1 -d '-')
}
# 主机名
hostname(){
	echo $(cat /etc/hostname)
}

pkill(){
    echo "磨刀霍霍向猪羊~~"
	if [ ! -n "$1" ];then
		echo "请通过参数传递进程名称!"
	fi
	pid=`ps -ef |grep 'java -jar'|grep $1|awk '{print $2}'`
	if [ -n "$pid" ];then
		echo "----kill " $1 ",process: "$pid
		kill -9 $pid
	fi
}

echo "Host IP:" $(hostIp)
echo "Host Name:" $(hostname)
echo "APP ID (仅用于测试):" $(appId)
# pkill
