#! /bin/sh
SCRIPT_DIR=$(cd `dirname $0`; pwd)
mkdir -p ${SCRIPT_DIR}/bin
# 配置模块名称
COMMON_SERVICE_PATH=${SCRIPT_DIR}/../cloud-service/common-service
PORTAL_PATH=${SCRIPT_DIR}/../cloud-aggregate/portal

build(){
	  # 每个模块运行build.sh后会保证至少出现xxx-latest.jar和xxx-version.jar两个二进制文件
    # 提取最新版和前一版的二进制文件
    bash $1/build/build.sh
    cp `ls $1/build/*.jar|sort -r|head -n 2`  ${SCRIPT_DIR}/bin
}

# 单独构建
if [ -n "$1" ];then
    echo "构建模块" $1
    build ${SCRIPT_DIR}/../cloud-service/$1
    exit 0
fi

# 并行构建
echo "构建全部模块..."
build ${COMMON_SERVICE_PATH}&
build ${PORTAL_PATH}&

