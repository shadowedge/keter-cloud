#! /bin/sh
# 应用名称
APP_NAME=common-service
# 配置中心
CONFIG_SERVER=http://192.168.1.221:9100
# 配置描述
PROFILE=prod
# 运行参数
JAVA_ARGS='-Xms256m -Xmx512m'

# 环境初始化
SCRIPT_DIR=$(cd `dirname $0`; pwd)
. ${SCRIPT_DIR}/_env.sh

# 重启服务
pkill ${APP_NAME}

# 运行服务:多副本
if [ -n "$1" ];then
	echo "--- 服务运行副本数: " $1
	for (( i=0; i<$1; i++)){
		id=$(appId)
		echo "启动服务:"  ${APP_NAME}:${i}-${id}
		nohup java -jar ${JAVA_ARGS} ${SCRIPT_DIR}/bin/${APP_NAME}-latest.jar\
		--spring.application.name=${APP_NAME}\
		--spring.profiles.active=${PROFILE}\
		--config.enabled=true\
		--config.hostname=$(hostIp)\
		--config.appid=${id}\
		--logging.name=${APP_NAME}-${id}\
		--config.uri=${CONFIG_SERVER}\
	    > ${SCRIPT_DIR}/${APP_NAME}-$i.log 2>&1 &
	}
    exit 0
fi

# 运行服务
id=$(appId)
echo "启动服务:" ${APP_NAME}-${id}
nohup java -jar  ${JAVA_ARGS}  ${SCRIPT_DIR}/bin/${APP_NAME}-latest.jar\
 --spring.profiles.active=ci\
 --spring.application.name=${APP_NAME}\
 --config.enabled=true\
 --config.hostname=$(hostIp)\
 --config.appid=${id}\
 --config.uri=${CONFIG_SERVER}\
 > ${SCRIPT_DIR}/${APP_NAME}.log 2>&1 &