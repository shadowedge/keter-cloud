#! /bin/sh
SCRIPT_DIR=$(cd `dirname $0`; pwd)

if [ -z "$1" ] ;then
    perl ${SCRIPT_DIR}/_deploy.pl
else
    perl ${SCRIPT_DIR}/_deploy.pl "$1"
fi
