#! /bin/sh
# 配置变量
APP_NAME=portal
CONFIG_SERVER=http://192.168.1.221:9100
PROFILE=prod
JAVA_ARGS='-Xms256m -Xmx512m'

# 环境初始化
SCRIPT_DIR=$(cd `dirname $0`; pwd)
. ${SCRIPT_DIR}/_env.sh

# 重启服务
pkill ${APP_NAME}

# 运行服务
if [ -n "$1" ];then
	echo "--- 服务运行副本数: " $1
	for (( i=0; i<$1; i++)){
		id=$(appId)
		echo "启动服务:"  ${APP_NAME}:${i}-${id}
		nohup java -jar  ${JAVA_ARGS}  ${SCRIPT_DIR}/bin/${APP_NAME}-latest.jar  --spring.profiles.active=${PROFILE} --config.enabled=true --config.hostname=$(hostIp) --config.appid=${id}  --config.uri=${CONFIG_SERVER}  --logging.name=${APP_NAME}-${id} >  ${SCRIPT_DIR}/${APP_NAME}-$i.log 2>&1 &
	}
    exit 0
fi

# 运行服务
id=$(appId)
echo "启动服务:" ${APP_NAME}-${id}
nohup java -jar ${JAVA_ARGS} ${SCRIPT_DIR}/bin/${APP_NAME}-latest.jar\
   --spring.application.name=${APP_NAME}\
   --spring.profiles.active=ci\
   --config.enabled=true\
   --config.hostname=$(hostIp)\
   --config.appid=${id}\
   --config.uri=${CONFIG_SERVER}>  ${SCRIPT_DIR}/${APP_NAME}.log 2>&1 &